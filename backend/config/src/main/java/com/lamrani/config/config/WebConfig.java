package com.lamrani.config.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    /*
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:"+ File.separator+"public"+File.separator };

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
    }
*/
    /*
     * Configure ContentNegotiationManager
     */
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.ignoreAcceptHeader(true).defaultContentType(
                MediaType.APPLICATION_JSON);
    }

    /*
     * Configure ContentNegotiatingViewResolver
     *
    @Bean
    public ViewResolver contentNegotiatingViewResolver(ContentNegotiationManager manager) {
        ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
        resolver.setContentNegotiationManager(manager);

        // Define all possible view resolvers
        List<ViewResolver> resolvers = new ArrayList<>();
        resolvers.add(csvViewResolver());
        resolvers.add(pdfViewResolver());
        resolvers.add(excelViewResolver());

        resolver.setViewResolvers(resolvers);
        return resolver;
    }
    */

    /*
     * Configure View resolver to provide XLS output using Apache POI library to
     * generate XLS output for an object content
     *
    @Bean
    public ViewResolver excelViewResolver() {
        return new ExcelViewResolver();
    }
    */

    /*
     * Configure View resolver to provide Csv output using Super Csv library to
     * generate Csv output for an object content
     *
    @Bean
    public ViewResolver csvViewResolver() {
        return new CsvViewResolver();
    }
    */

    /*
     * Configure View resolver to provide Pdf output using iText library to
     * generate pdf output for an object content
     *
    @Bean
    public ViewResolver pdfViewResolver() {
        return new PdfViewResolver();
    }
     */


}
