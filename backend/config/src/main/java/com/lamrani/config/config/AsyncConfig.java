package com.lamrani.config.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableAsync
public class AsyncConfig extends AsyncConfigurerSupport {

    @Value("${executor.pool-size.core}")
    private Integer corePoolSize;

    @Value("${executor.pool-size.max}")
    private Integer maxPoolSize;

    @Value("${executor.queue-capacity}")
    private Integer queueCapacity;

    @Value("${executor.thead-name.prefix}")
    private String threadNamePrefix;


    @Override
    public Executor getAsyncExecutor() {
        /**
         * Please read doc for what thos conf means:
         * http://docs.spring.io/spring-framework/docs/4.3.6.RELEASE/javadoc-api/org/springframework/scheduling/concurrent/ThreadPoolTaskExecutor.html
         */
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setThreadNamePrefix(threadNamePrefix);
        executor.initialize();
        return executor;
    }
}
