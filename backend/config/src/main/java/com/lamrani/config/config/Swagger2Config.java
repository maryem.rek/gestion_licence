package com.lamrani.config.config;

import com.google.common.base.Predicate;
import com.lamrani.auth.config.JwtAuthenticationRequest;
import com.lamrani.auth.repositories.UserRepository;
import com.lamrani.auth.rest.AuthenticationRestController;
import com.lamrani.auth.service.JwtAuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * SwaggerConfig Swagger2 UI configuration
 *
 * @author Peaqock financials
 * @version 1.0
 * @since 2018-04-03
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Autowired
    private AuthenticationRestController authenticationRestController;

    @Autowired
    private UserRepository userRepository;

    /**
     *  u must add groupName(string) and apis(Predicate<RequestHandler> selector )
     *
     * @return
     */
    @Bean
    public Docket api() {
        //Adding Header
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder.name("Authorization")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .defaultValue(getToken())
                .build();
        List<Parameter> aParameters = new ArrayList<Parameter>();
        aParameters.add(aParameterBuilder.build());


        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                // put header into all requests
                .build().globalOperationParameters(aParameters)
                .apiInfo(metaData());
    }



    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "Bill Authentication Rest Project ",
                "Spring Boot REST API for BILL PROJECT - PEAQOCK FINANCIALS",
                "1.0",
                "Terms of service",

                new Contact("PEAQOCK FINANCIALS", "http://www.peaqock.com", "info@peaqock.com"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0");
        return apiInfo;
    }

    private static Predicate<RequestHandler> exactPackage(final String pkg) {
        return input ->input.getHandlerMethod().getBeanType().getName().contains(pkg);
    }

    private String getToken(){
        if ((userRepository.findByEmail("admin@admin.com")!=null)) {
            JwtAuthenticationRequest request = new JwtAuthenticationRequest("admin@admin.com","admin1");
            ResponseEntity<?> tokenResponse = authenticationRestController.createAuthenticationToken(request);
            return ((JwtAuthenticationResponse)tokenResponse.getBody()).getToken();

        }
        return "";
    }
}

