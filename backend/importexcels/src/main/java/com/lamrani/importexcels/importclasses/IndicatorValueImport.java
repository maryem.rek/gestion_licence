package com.lamrani.importexcels.importclasses;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gestion.domaine.model.Indicator;
import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.model.Subsidiary;
import com.gestion.domaine.services.IndicatorService;
import com.gestion.domaine.services.IndicatorValueService;
import com.gestion.domaine.services.SubsidiaryService;



@Component
public class IndicatorValueImport {

	@Autowired
	private SubsidiaryService subsidiaryService;

	@Autowired
	private IndicatorService indicatorService;

	@Autowired
	private IndicatorValueService indicatorValueService;

	public void  readExcel(File fileToRead) {

		try {
			Workbook workbook = WorkbookFactory.create(fileToRead);
			Iterator<Sheet> sheetIterator = workbook.sheetIterator();
			// Sheet = country
	        while (sheetIterator.hasNext()) {
	        	Sheet datatypeSheet = sheetIterator.next();
				Iterator<Row> iterator = datatypeSheet.iterator();
				String country = datatypeSheet.getSheetName();

				//headers
				List<Date> dates = getdates(iterator.next());

				// values
				Subsidiary sb= null;
				String sbName= null;
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					Cell subsidiaryCell = currentRow.getCell(0);
					Cell indicatorCell = currentRow.getCell(1);

					if(indicatorCell!=null && !indicatorCell.toString().equals("") && subsidiaryCell!=null && !subsidiaryCell.toString().equals("")){
						subsidiaryCell.setCellType(Cell.CELL_TYPE_STRING);
						indicatorCell.setCellType(Cell.CELL_TYPE_STRING);
						if(!subsidiaryCell.toString().equals(sbName)) {
							sbName= subsidiaryCell.toString();
							sb= subsidiaryService.findByBank(subsidiaryCell.toString());
						}
						Indicator indice= indicatorService.findByName(indicatorCell.toString());

						if(sb!=null && indice!=null) {
							List<IndicatorValue> indicatorValues= this.indicatorValueService.findByIndicatorIdAndSubsidiaryId(indice.getId(), sb.getId());


							for (int ii =2;ii< dates.size()+2;ii++){
								Cell indicatorValueCell = currentRow.getCell(ii);
								if(indicatorValueCell!=null) {
									indicatorValueCell.setCellType(Cell.CELL_TYPE_STRING);
									IndicatorValue indicatorValue = new IndicatorValue ();
									if(indicatorValueCell!=null ) {
										DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
										Date date = null;
										indicatorValue.setSubsidiary(sb);
										indicatorValue.setIndicator(indice);
										Calendar cal = Calendar.getInstance();
										cal.setTime(dates.get(ii-2));
										date = format.parse(cal.get(Calendar.DAY_OF_MONTH)+"-"+cal.get(Calendar.MONTH)+'-'+cal.get(Calendar.YEAR));
										indicatorValue.setYear(cal.get(Calendar.YEAR));
										indicatorValue.setMonth(cal.get(Calendar.MONTH)+1);

										if(indicatorValueCell.toString()!="") {
											try{
												BigDecimal value = BigDecimal.valueOf(Double.parseDouble(indicatorValueCell.toString()));
												indicatorValue.setValue(value);
											}catch(NumberFormatException e){
												System.out.print(e);
											}
										}
										IndicatorValue indVal= this.getTheselectedOne(indicatorValues, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1);
										if(indVal==null) {
											indicatorValueService.create(indicatorValue);
										}else {
											indicatorValue.setId(indVal.getId());
											indicatorValueService.update(indicatorValue);
										}
									}
								}
							}
						}
					}
				}
				workbook.close();
			}
		} catch (IOException | EncryptedDocumentException | InvalidFormatException |ParseException e) {
			System.out.println(e);
		}
	}


	private List<Date> getdates(Row headerRow) {
		List<Date> dates = new ArrayList<>();
		if(headerRow!=null) {
			for (int ii =2;ii< headerRow.getLastCellNum();ii++){
				Cell currencyCell = headerRow.getCell(ii);
				if(currencyCell!=null && currencyCell.toString()!="" ) {
					dates.add(currencyCell.getDateCellValue());
				}
			}
		}
		return  dates;
	}

	private int getMonthIndex(String name){
		List<String> monthS = Arrays.asList("Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
				"Juillet", "Août", "Septembre", "Octobre", "Novembre","Décembre");
		List<String> months =
				Arrays.asList("janv", "févr", "mars", "avr", "mai", "juin", "juil", "août"
						, "sept", "oct", "nov","déc");
		for (String s:months){
			if(name.contains(s)){
				return months.indexOf(s)+1;
			}
		}
		for (String s:monthS){
			if(name.contains(s)){
				return monthS.indexOf(s)+1;
			}
		}
		return 0;
	}

	private int getYear(String name){
		if (name!=null && !"".equals(name)){
			String numberOnly = name.replaceAll("[^0-9]", "");
			numberOnly = numberOnly.length()==2?  "20".concat(numberOnly):numberOnly;
			numberOnly = numberOnly.length()==3?  "2".concat(numberOnly):numberOnly;
			return Integer.parseInt(numberOnly)<9999 && Integer.parseInt(numberOnly)>1969?
					Integer.parseInt(numberOnly):1970;
		}
		return 1970;
	}

	private IndicatorValue getTheselectedOne(List<IndicatorValue> indicatorValues, int year, int month) {
		IndicatorValue indice= null;

		for (IndicatorValue item : indicatorValues) {
			if(item.getMonth()==month && item.getYear()==year) {
				indice= item;
			}
		}

		return indice;
	}
}
