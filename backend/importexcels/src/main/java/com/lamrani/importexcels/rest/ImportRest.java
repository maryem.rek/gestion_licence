package com.lamrani.importexcels.rest;

import com.lamrani.importexcels.importclasses.CurrencyImport;
import com.lamrani.importexcels.importclasses.IndicatorImport;
import com.lamrani.importexcels.importclasses.IndicatorValueImport;
import com.lamrani.importexcels.importclasses.SubsidiaryImport;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
@RequestMapping(value = "/import")
@CrossOrigin("*")
public class ImportRest {

    @Autowired
    private CurrencyImport currencyImport;

    @Autowired
    private SubsidiaryImport subsidiaryImport;

    @Autowired
    private IndicatorImport indicatorImport;

    @Autowired
    private IndicatorValueImport indicatorValueImport;

    @RequestMapping(value="/currency" ,method = RequestMethod.POST)
    @ApiOperation(value = "import excel file")
    public ResponseEntity<?> importCurrency(@RequestParam("file") MultipartFile file) {
        File fileToRead = convert(file);
        if (fileToRead!=null){
            currencyImport.readCurrencyExcel(fileToRead);
            fileToRead.delete();
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @RequestMapping(value="/subsidiary" ,method = RequestMethod.POST)
    @ApiOperation(value = "import excel file")
    public ResponseEntity<?> importSubsidiary(@RequestParam("file") MultipartFile file) {
        File fileToRead = convert(file);
        if (fileToRead!=null){
            subsidiaryImport.readExcel(fileToRead);
            fileToRead.delete();
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @RequestMapping(value="/indicators" ,method = RequestMethod.POST)
    @ApiOperation(value = "import excel file")
    public ResponseEntity<?> importIndicator(@RequestParam("file") MultipartFile file) {
        File fileToRead = convert(file);
        if (fileToRead!=null){
            indicatorImport.readExcel(fileToRead);
            fileToRead.delete();
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @RequestMapping(value="/indicatorvalues" ,method = RequestMethod.POST)
    @ApiOperation(value = "import excel file")
    public ResponseEntity<?> importIndicatorValues(@RequestParam("file") MultipartFile file) {
        File fileToRead = convert(file);
        if (fileToRead!=null){
            indicatorValueImport.readExcel(fileToRead);
            fileToRead.delete();
        }
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    private File convert(MultipartFile file)
    {
        try {
            File convFile = new File(file.getOriginalFilename());
            convFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
            return convFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
