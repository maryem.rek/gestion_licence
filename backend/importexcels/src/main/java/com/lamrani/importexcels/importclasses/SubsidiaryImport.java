package com.lamrani.importexcels.importclasses;

import com.gestion.domaine.model.Country;
import com.gestion.domaine.model.Currency;
import com.gestion.domaine.model.Subsidiary;
import com.gestion.domaine.model.Zone;
import com.gestion.domaine.services.CountryService;
import com.gestion.domaine.services.CurrencyService;
import com.gestion.domaine.services.SubsidiaryService;
import com.gestion.domaine.services.ZoneService;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

@Component
public class SubsidiaryImport {

	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private ZoneService zoneService;

	@Autowired
	private CountryService countryService;

	@Autowired
	private SubsidiaryService subsidiaryService;

	public void  readExcel(File fileToRead) {

		try {
			Workbook workbook = WorkbookFactory.create(fileToRead);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();

			//header
			iterator.next();
			// values
			while (iterator.hasNext()) {
				createZonesAndCountriesAndSubsidiaries(iterator.next());
			}
			workbook.close();

		} catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
			System.out.println(e);
		}
	}


	private void createZonesAndCountriesAndSubsidiaries(Row row)  {

		if(row!=null) {
			Cell zoneCell = row.getCell(0);
			if(zoneCell!=null){
				Zone zone =  new Zone();
				zone.setName(zoneCell.toString());
				zoneService.create(zone);
			}

			Cell currencyCell = row.getCell(2);
			if(currencyCell!=null){
				Currency currency = new Currency();
				currency.setName(currencyCell.toString());
				currency.setSymbole(currencyCell.toString());
				currencyService.create(currency);
			}

			Cell countryCell = row.getCell(1);
			if(countryCell!=null && zoneCell!=null && currencyCell!=null){
				Country country = new Country();
				country.setName(countryCell.toString());
				country.setZone(zoneService.findByName(zoneCell.toString()));
				country.setCurrency(currencyService.findByName(currencyCell.toString()));
				countryService.create(country);
			}

			Cell nameCell = row.getCell(3);
			Cell detailedCompanyNameCell = row.getCell(4);
			Cell createdAtCell = row.getCell(5);
			Cell shareholdersCell = row.getCell(6);
			Cell agenciesNumberCell = row.getCell(7);
			Cell PdmCreditsCell = row.getCell(8);
			Cell PDMDepotsCell = row.getCell(9);

			if(countryCell!=null){
				Subsidiary subsidiary = new Subsidiary();
				subsidiary.setBank(nameCell.toString());

				if(detailedCompanyNameCell!=null) {
					subsidiary.setDetailedCompanyName(detailedCompanyNameCell.toString());
				}

				//dates
				if(createdAtCell!=null) {
					if(!createdAtCell.toString().equals("")) {
						if(createdAtCell.toString().matches("[0-9]*/[0-9]*/[0-9]*")) {
							DateFormat format = new SimpleDateFormat("dd/MMM/yyyy", Locale.FRANCE);
							Date date = null;
							try {
								date = format.parse(createdAtCell.toString());
							} catch (ParseException e) {
								//							e.printStackTrace();
							}
							subsidiary.setCreatedAt(date);
						}else if(createdAtCell.toString().matches("[0-9]*")) {
							DateFormat format = new SimpleDateFormat("dd/mm/yyyy", Locale.FRANCE);
							Date date = null;
							try {
								date = format.parse("01/01/"+createdAtCell.toString());
							} catch (ParseException e) {
								//							e.printStackTrace();
							}
							subsidiary.setCreatedAt(date);
						}else if(createdAtCell.toString().contains(".")) {
							if(createdAtCell.toString().split("\\.").length>0) {
								String res= createdAtCell.toString().split("\\.")[0];
								if((!res.equals("")) && res.matches("[0-9]*")) {
									DateFormat format = new SimpleDateFormat("dd/mm/yyyy", Locale.FRANCE);
									Date date = null;
									try {
										date = format.parse("01/01/"+res);
									} catch (ParseException e) {
										//							e.printStackTrace();
									}
									subsidiary.setCreatedAt(date);
								}
							}
						}
					}
				}
				if(shareholdersCell!=null) {
					subsidiary.setShareholders(shareholdersCell.toString());
				}
				if(PdmCreditsCell!=null) {
					subsidiary.setPDMCredits(PdmCreditsCell.toString().replace("%", ""));
				}
				if(PDMDepotsCell!=null) {
					subsidiary.setPDMDepots(PDMDepotsCell.toString().replace("%", ""));
				}

				//number of agencies
				subsidiary.setNumberOfAgencies( 0L );
				if(agenciesNumberCell!=null) {
					try{
						if(agenciesNumberCell.toString().split("\\.").length>0) {
							String res= agenciesNumberCell.toString().split("\\.")[0];
							subsidiary.setNumberOfAgencies( Long.parseLong(res));
						}else {
							subsidiary.setNumberOfAgencies( Long.parseLong(agenciesNumberCell.toString()));
						}
					}catch(NumberFormatException e){
						//						e.printStackTrace();
					}
				}
				subsidiary.setCountry(countryService.findByName(countryCell.toString()));
				subsidiaryService.saveOrUpdate(subsidiary);
			}

		}
	}

}
