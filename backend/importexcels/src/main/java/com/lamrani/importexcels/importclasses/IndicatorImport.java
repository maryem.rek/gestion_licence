package com.lamrani.importexcels.importclasses;

import com.gestion.domaine.model.Family;
import com.gestion.domaine.model.Indicator;
import com.gestion.domaine.model.Topic;
import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.services.FamilyService;
import com.gestion.domaine.services.IndicatorService;
import com.gestion.domaine.services.TopicService;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

@Component
public class IndicatorImport {


    @Autowired
    private TopicService topicService;

    @Autowired
    private IndicatorService indicatorService;

    @Autowired
    private FamilyService familyService;

    public void  readExcel(File fileToRead ) {

        try {
            Workbook workbook = WorkbookFactory.create(fileToRead);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            int i= 0;

            //header
            iterator.next();
            // values
            while (iterator.hasNext()) {
            	i++;
                createTopicAndFamilyAndIndicator(iterator.next());
            }
            workbook.close();

        } catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
            System.out.println(e);
        }
    }


    private void createTopicAndFamilyAndIndicator(Row row)  {

        if(row!=null) {
            Cell topicCell = row.getCell(0);
            if(topicCell!=null){
                Topic topic =  new Topic();
                topic.setName(topicCell.toString());
                topicService.create(topic);
            }

            Cell familyCell = row.getCell(1);
            if(familyCell!=null && topicCell!=null){
                Family family = new Family();
                family.setName(familyCell.toString());
                family.setTopic(topicService.findByName(topicCell.toString()));
                familyService.create(family);
            }

            Cell nameCell = row.getCell(2);
            Cell calculableCell = row.getCell(3);
            Cell formulaCell = row.getCell(4);
            Cell convertibleCell = row.getCell(5);
            Cell currencyRateCell = row.getCell(6);
            Cell varianceCell = row.getCell(7);
            Cell displayedOnAdHocFileCell = row.getCell(8);
            Cell parentCell = row.getCell(9);
            
            if(nameCell!=null && calculableCell!=null&& familyCell!=null &&
                    convertibleCell!=null &&
                    varianceCell!=null && displayedOnAdHocFileCell!=null){

                Indicator indicator = new Indicator();
                indicator.setName(nameCell.toString());
                indicator.setCalculable(!"Déclaratif".toUpperCase().equals(calculableCell.toString().toUpperCase()));
                indicator.setFamily(familyService.findByName(familyCell.toString()));
                if(formulaCell!=null) {
                	indicator.setFormula(formulaCell.toString());
                }
                indicator.setConvertible("OUI".equals(convertibleCell.toString().toUpperCase()));
                if(currencyRateCell!=null) {
                	indicator.setCurrencyRate("cours moyen".equals(currencyRateCell.toString().toLowerCase())
                        ? CurrencyRate.AVERAGE_PRICE :"cours spot".equals(currencyRateCell.toString().toLowerCase())?
                        CurrencyRate.AVERAGE_SPOT : null );
                }
                indicator.setVariance(varianceCell.toString());
                if(parentCell!=null){
                    Indicator tp =indicatorService.findByName(parentCell.toString());
                    indicator.setParent(tp);
                }
                indicator.setDisplayedOnAdHocFile("OUI".equals(displayedOnAdHocFileCell.toString().toUpperCase()));

                indicatorService.create(indicator);
            }

        }
    }

}
