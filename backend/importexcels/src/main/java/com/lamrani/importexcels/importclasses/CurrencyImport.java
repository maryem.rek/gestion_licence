package com.lamrani.importexcels.importclasses;

import com.gestion.domaine.model.Currency;
import com.gestion.domaine.model.CurrencyValue;
import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.services.CurrencyService;
import com.gestion.domaine.services.CurrencyValueService;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Component
public class CurrencyImport {

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private CurrencyValueService currencyValueService;

    private Double readBourse(String name, int year, int month) {
        Double result= 0D;
        String path= "path here!" ; //HelpersUtils.getUrl()+name;
        File fileToRead = new File(path);

        try {
            Workbook workbook = WorkbookFactory.create(fileToRead);

            Sheet datatypeSheet = workbook.getSheetAt(0);

            Iterator<Row> iterator = datatypeSheet.iterator();
            int i= 0;
            if(iterator.hasNext()) {iterator.next();}
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                if(currentRow!=null) {
                    Cell cellSourceDate= currentRow.getCell(22);
                    if(cellSourceDate!=null ) {

                        cellSourceDate.setCellType(Cell.CELL_TYPE_STRING);
                        String yearVal=null;
                        yearVal= cellSourceDate.toString().substring(0,4);
                    }
                }
            }
            workbook.close();
        } catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
            System.out.println(e);
            result= 0D;
        }

        return result;
    }


    public void  readCurrencyExcel(File fileToRead ) {
        try {
            Workbook workbook = WorkbookFactory.create(fileToRead);
            // Sheet = year
            for(int j=0;j<workbook.getNumberOfSheets();j++){
                Sheet datatypeSheet = workbook.getSheetAt(j);
                Iterator<Row> iterator = datatypeSheet.iterator();
                int curencyYear = Integer.parseInt(datatypeSheet.getSheetName());
//                if(iterator.hasNext()) {iterator.next();}

                //headers
                List<String> currenciesName = createCurrenciesAndGetTheirNames(iterator.next());

                // values
                while (iterator.hasNext()) {
                    Row currentRow = iterator.next();

                    Cell monthCell = currentRow.getCell(0);

                    for (int ii =1;ii< currenciesName.size()+1;ii++){
                        Cell currencyCell = currentRow.getCell(ii);
                        CurrencyValue currencyValue = new CurrencyValue();
                        if(currencyCell!=null && monthCell!=null ) {
                            int month = getMonthIndex(monthCell.toString());
                            currencyValue.setYear(curencyYear);
                            currencyValue.setMonth(month);
                            try{
                                BigDecimal value = BigDecimal.valueOf(Double.parseDouble(currencyCell.toString()));
                                currencyValue.setValue(value);
                            }catch(NumberFormatException e){
                                System.out.println(e);
                            }
                            if (month==0) currencyValue.setCurrencyRate(CurrencyRate.AVERAGE_SPOT);
                            else currencyValue.setCurrencyRate(CurrencyRate.AVERAGE_PRICE);
                            currencyValue.setCurrency(currencyService.findByName(currenciesName.get(ii-1)));
                            currencyValueService.create(currencyValue);
                        }
                    }
                }
                workbook.close();
            }
        } catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
            System.out.println(e);
        }
    }

    private List<String> createCurrenciesAndGetTheirNames(Row headerRow) {
        List<String> currencies = new ArrayList<>();
        if(headerRow!=null) {
            for (int ii =1;ii< headerRow.getLastCellNum();ii++){
                Cell currencyCell = headerRow.getCell(ii);
                Currency currency = new Currency();
                if(currencyCell!=null && currencyCell.toString()!="" ) {
                    currency.setName(currencyCell.toString());
                    currency.setSymbole(currencyCell.toString());
                    currencies.add(currencyCell.toString());
                    currencyService.create(currency);
                    //currency.setCellType(CellType.STRING);
                 }
            }
        }
        return  currencies;
    }


    private int getMonthIndex(String name){
        List<String> months = Arrays.asList("Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
                "Juillet", "Août", "Septembre", "Octobre", "Novembre","Décembre");
        return months.contains(name)? months.indexOf(name)+1:0;
    }
}
