package com.gestion.domaine.rest;

import com.gestion.domaine.services.DomainUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class DomainUserRest {

    @Autowired
    private DomainUserService domainUserService;

    @GetMapping()
    @ApiOperation(value = "Get List users")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                domainUserService.findAll(),
                HttpStatus.OK);
    }

}
