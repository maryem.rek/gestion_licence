package com.gestion.domaine.services;

import com.gestion.domaine.model.*;
import com.gestion.domaine.repositories.IndicatorValueRepository;
import com.lamrani.domaine.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class IndicatorValueService {

    @Autowired
    private IndicatorValueRepository indicatorValueRepository;

    @Autowired
    private CurrencyValueService currencyValueService;

    /**
     * Create IndicatorValue
     * @param indicatorValue
     * @return IndicatorValue | null
     */
    public IndicatorValue create(IndicatorValue indicatorValue){
        indicatorValue.setDateValue(new GregorianCalendar(indicatorValue.getYear(),indicatorValue.getMonth()-1,1).getTime());
        return indicatorValueRepository.save(indicatorValue);
    }

    /**
     * Update IndicatorValue : Id should be present
     * @param indicatorValue
     * @return IndicatorValue if updated else null
     */
    public IndicatorValue update(IndicatorValue indicatorValue){
        indicatorValue.setDateValue(new GregorianCalendar(indicatorValue.getYear(),indicatorValue.getMonth()-1,1).getTime());
        return (indicatorValue.getId()!=null && indicatorValueRepository.findById(indicatorValue.getId()).isPresent())?
            indicatorValueRepository.saveAndFlush(indicatorValue):null;
    }

    /**
     * Get IndicatorValue by id
     * @param id
     * @return IndicatorValue if updated else null
     */
    public IndicatorValue findById(Long id){
        Optional<IndicatorValue> indicatorValue1 = indicatorValueRepository.findById(id);
        return indicatorValue1.orElse(null);
    }

    /**
     * Get all indicatorValues
     * @return List<IndicatorValue>
     */
    public List<IndicatorValue> findAll(){
        return indicatorValueRepository.findAll();
    }

    /**
     * Get Page of indicatorValues
     * @param page
     * @param size
     * @return Page<IndicatorValue>
     */
    public Page<IndicatorValue> findPages(int page, int size)
    {
        return indicatorValueRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete IndicatorValue by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            indicatorValueRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !indicatorValueRepository.findById(id).isPresent();
    }

    /**
     * Delete IndicatorValue
     * @param indicatorValue
     * @return isDeleted
     */
    public Boolean delete(IndicatorValue indicatorValue){
        try{
            if (indicatorValue.getId()!=null){
                indicatorValueRepository.delete(indicatorValue);
                return !indicatorValueRepository.findById(indicatorValue.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }

    /**
     * get IndicatorValue by these params
     * @param indicatorId
     * @param startedDate
     * @param endDate
     * @return
     */
    public List<IndicatorValue> findByIndicatorAndYearAndMonth(long indicatorId, String startedDate, String endDate){
        return  indicatorValueRepository.findByIndicatorAndStartDateAndEndDate(indicatorId,startedDate,endDate);
    }

    /**
     * get IndicatorValue by these params
     * @param indicatorId
     * @param subsidiaryId
     * @param startedDate
     * @param endDate
     * @return
     */
    public List<IndicatorValue> findByIndicatorAndSubsidiaryAndStartDateAndEndDate(long indicatorId,long subsidiaryId, String startedDate, String endDate){
        return  indicatorValueRepository.findByIndicatorAndSubsidiaryAndStartDateAndEndDate(indicatorId,subsidiaryId,startedDate,endDate);
    }


    public List<IndicatorValue> executeQuery(QueryEntity queryEntity) {
        //TODO  calculate Indicators and ratios
        CurrencyValue currencyValue = currencyValueService.findByYearAndCurrencyRateAndCurrency
                (queryEntity.getCurrencyMonth(),queryEntity.getCurrencyYear(),queryEntity.getCurrencyRate(),queryEntity.getCurrency());

        List<Long> indicatorsIds = queryEntity.getIndicators()!=null? queryEntity.getIndicators().stream().map(indicator -> indicator.getId()).collect(Collectors.toList()):new ArrayList<>();
        List<Long> subsidiariesIds = queryEntity.getSubsidiaries()!=null? queryEntity.getSubsidiaries().stream().map(subsidiary -> subsidiary.getId()).collect(Collectors.toList()):new ArrayList<>();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String fromDate = df.format(queryEntity.getFromDate());
        String toDate = df.format(queryEntity.getToDate());
        List<IndicatorValue> indicatorValues = indicatorValueRepository.executeQuery(indicatorsIds, subsidiariesIds, fromDate, toDate);
        return indicatorValues.stream().map(indicatorValue -> {
            if(indicatorValue.getIndicator().getConvertible() && currencyValue!=null)
                indicatorValue.setValue(indicatorValue.getValue().multiply(currencyValue.getValue()));
            return indicatorValue;
        }).collect(Collectors.toList());
    }

    /**
     * get Indicator Values based on query params
     * @param nonCalculableIndicators
     * @param queryEntity
     * @return
     */
    public List<IndicatorValue> findByIndicatorsAndSubsidiaryAndDates(List<Indicator> nonCalculableIndicators, QueryEntity queryEntity) {
        if (nonCalculableIndicators.size()!=0){
        List<Long> indicatorsIds = nonCalculableIndicators.stream().map(indicator -> indicator.getId()).collect(Collectors.toList());
        List<Long> subsidiariesIds = queryEntity.getSubsidiaries()!=null?
                queryEntity.getSubsidiaries().stream().map(subsidiary -> subsidiary.getId()).collect(Collectors.toList()):new ArrayList<>();

        if(!queryEntity.isDiscreet())
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String fromDate = df.format(queryEntity.getFromDate());
            String toDate = df.format(queryEntity.getToDate());
            return indicatorValueRepository.executeQueryfromTo(indicatorsIds,subsidiariesIds,fromDate,toDate);
        }
       List<String> stringDates =  queryEntity.getDates().stream().map(date ->{
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            return  df.format(date);
        }).collect(Collectors.toList());
        return indicatorValueRepository.executeQueryInTheseDates(indicatorsIds,subsidiariesIds,stringDates);
        }
        return new ArrayList<>();
    }

    public List<IndicatorValue> findByIndicatorAndMonthAndYearAndSubsidiary(Indicator indicator, int month, int year, Subsidiary subsidiary) {
        return indicatorValueRepository.findByIndicatorAndMonthAndYearAndSubsidiary( indicator,  month,  year, subsidiary);
    }

    public List<Date> findFirstDateAndLastDateEver() {
        List<Date> dates =  new ArrayList<>();
        dates.add(indicatorValueRepository.findFirstDateEver());
        dates.add(indicatorValueRepository.findLastDateEver());
        return dates;
    }

    public List<IndicatorValue> findByIndicatorIdAndSubsidiaryId(Long indicatorId, Long subsidiaryId) {
        return indicatorValueRepository.findByIndicatorIdAndSubsidiaryId(indicatorId, subsidiaryId);
    }

    public IndicatorValue findTopByIndicatorAndSubsidiaryAndYearAndMonth(Indicator indicator, Subsidiary subsidiary, int year, int month) {
        return indicatorValueRepository.findTopByIndicatorAndSubsidiaryAndYearAndMonth(indicator,subsidiary,year,month);
    }
}
