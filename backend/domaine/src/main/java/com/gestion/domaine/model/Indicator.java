package com.gestion.domaine.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.gestion.domaine.model.enums.CurrencyRate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Indicator implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String variance;
    private String formula;
    private Boolean calculable;
    private Boolean convertible;
    @Enumerated(EnumType.STRING)
    private CurrencyRate currencyRate;
    private Boolean displayedOnAdHocFile;

    @ManyToOne(fetch = FetchType.LAZY)
    private Family family;

    @OneToMany(mappedBy = "indicator",cascade=CascadeType.ALL)//, orphanRemoval=true)
    private List<CorrespondingIndicator> correspondingIndicators;

    @OneToMany(mappedBy = "indicator",cascade=CascadeType.ALL)//, orphanRemoval=true)
    private List<IndicatorValue> indicatorValues;

    @ManyToMany(mappedBy = "indicators",cascade=CascadeType.ALL)
        private List<QueryEntity> queryEntities;

    @ManyToOne(fetch = FetchType.EAGER)
    private Indicator parent;

    public Indicator() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVariance() {
        return variance;
    }

    public void setVariance(String variance) {
        this.variance = variance;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Boolean getCalculable() {
        return calculable;
    }

    public void setCalculable(Boolean calculable) {
        this.calculable = calculable;
    }

    public Boolean getConvertible() {
        return convertible;
    }

    public void setConvertible(Boolean convertible) {
        this.convertible = convertible;
    }

    public CurrencyRate getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(CurrencyRate currencyRate) {
        this.currencyRate = currencyRate;
    }

    public Boolean getDisplayedOnAdHocFile() {
        return displayedOnAdHocFile;
    }

    public void setDisplayedOnAdHocFile(Boolean displayedOnAdHocFile) {
        this.displayedOnAdHocFile = displayedOnAdHocFile;
    }

    
    public Indicator getParent() {
        return parent;
    }

    @JsonSetter
    public void setParent(Indicator parent) {
        this.parent = parent;
    }

    @JsonIgnore
    public List<CorrespondingIndicator> getCorrespondingIndicators() {
        return correspondingIndicators;
    }

    @JsonSetter
    public void setCorrespondingIndicators(List<CorrespondingIndicator> correspondingIndicators) {
        this.correspondingIndicators = correspondingIndicators;
    }

    @JsonIgnore
    public List<IndicatorValue> getIndicatorValues() {
        return indicatorValues;
    }

    @JsonSetter
    public void setIndicatorValues(List<IndicatorValue> indicatorValues) {
        this.indicatorValues = indicatorValues;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @JsonIgnore
    public List<QueryEntity> getQueryEntities() {
        return queryEntities;
    }

    @JsonSetter
    public void setQueryEntities(List<QueryEntity> queryEntities) {
        this.queryEntities = queryEntities;
    }


}
