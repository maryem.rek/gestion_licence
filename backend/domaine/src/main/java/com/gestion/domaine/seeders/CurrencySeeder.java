package com.gestion.domaine.seeders;

import com.gestion.domaine.model.Currency;
import com.gestion.domaine.repositories.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CurrencySeeder {

    @Autowired
    CurrencyRepository currencyRepository;

    public void run(){
        createEntity("TND","TND");
        createEntity("FCFA","FCFA");
        createEntity("XOF (FCFA)","XOF (FCFA)"); //TODO keep it or lose it
        createEntity("XAF (FCFA)","XAF (FCFA)"); //TODO keep it or lose it
        createEntity("MRO","MRO");
        createEntity("EGP","EGP");
        createEntity("EAD","EAD");
        createEntity("USD/EAD","USD/EAD");

        createEntity("EUR","€");
        createEntity("MAD","MAD");
        createEntity("USD","$");
    }

    private void createEntity(String name, String symbol) {
        Optional<Currency> currency1 = currencyRepository.findByName(name);
        if (currency1.isPresent()) {
            currencyRepository.saveAndFlush(getEntity(currency1.get().getId(),name,symbol));
        }else
            currencyRepository.save(getEntity(-1L,name,symbol));
    }

    private Currency  getEntity(Long id, String name, String symbol){
        Currency currency =new Currency();
        currency.setName(name);
        currency.setSymbole(symbol);
        currency.setId(id);
        return currency;
    }


}
