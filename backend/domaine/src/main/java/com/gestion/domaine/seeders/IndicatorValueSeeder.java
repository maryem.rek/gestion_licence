package com.gestion.domaine.seeders;

import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.repositories.IndicatorRepository;
import com.gestion.domaine.repositories.IndicatorValueRepository;
import com.gestion.domaine.repositories.SubsidiaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class IndicatorValueSeeder {

    @Autowired
    private IndicatorRepository indicatorRepository;

    @Autowired
    private IndicatorValueRepository indicatorValueRepository;

    @Autowired
    private SubsidiaryRepository subsidiaryRepository;

    private void createEntitiesByYearAndCurrency(int year, int month, String subsidiary, String[] indicators ,double[] values){
            for (int i =0;i<values.length;i++){
                createEntity(BigDecimal.valueOf( values[i]),month,year, subsidiary,indicators[i]);
            }
    }

    private void createEntitiesByYearAndSubsidiaries(int year, int month, String[] subsidiaries, String indicator ,double[] values){
            for (int i =0;i<values.length;i++){
                createEntity(BigDecimal.valueOf( values[i]),month,year, subsidiaries[i],indicator);
            }

    }

    public void run(){

        String[] indicators={"RAO : Résultat net / total bilan","PNB/ Total bilan","MNI/ Total bilan", "marge sur commissions / Total Bilan",
                "Résultats marché / Total bilan", "Autres PNB/ Total bilan"
                , "Autres pdts bancaires / Total bilan", "Charges / Total bilan", "Autres pdts non bancaires / Total bilan", "Coût du risque / Total bilan", "Résultat non courant/Total bilan", "IS / Total bilan"};

        double[] values3_2015 ={2.38,6.72,2.94, 2.47, 1.32,  0
                , 0.01, -3.15, 0.00, -0.07, -0.12, -1.02};
        createEntitiesByYearAndCurrency(2015,3,"CDC", indicators,values3_2015);

        double[] values6_2015 ={2.07,7.48,3.41, 3.00, 1.08,  0
                , 0.01, -3.36, 0.00, -1.05, -0.13, -0.89};
        createEntitiesByYearAndCurrency(2015,6,"CDC", indicators,values6_2015);

        double[] values9_2015 ={2.28,7.44,3.05, 2.95, 1.45,  0.00
                , 0.01, -3.35, 0.00, -0.72, -0.12, -0.98};
        createEntitiesByYearAndCurrency(2015,9,"CDC", indicators,values9_2015);

        double[] values12_2015 ={2.27,7.61,3.53, 2.97, 1.12,  0.00
                , 0.01, -3.50, 0.00, -0.66, -0.20, -0.99};
        createEntitiesByYearAndCurrency(2015,12,"CDC", indicators,values12_2015);



        double[] values3_2016 ={2.26,7.17,3.01, 2.82, 1.33,  0.00
                , 0.00, -3.48, 0.00, -0.29, -0.17, -0.97};
        createEntitiesByYearAndCurrency(2016,3,"CDC", indicators,values3_2016);

        double[] values6_2016 ={2.10,7.70,3.31, 2.84, 1.54,  0.00
                , 0.01, -3.59, 0.00, -0.86, -0.27, -0.90};
        createEntitiesByYearAndCurrency(2016,6,"CDC", indicators,values6_2016);

        double[] values9_2016 ={2.35,7.91,3.45, 2.89, 1.57,  0.00
                , 0.01, -3.68, 0.00, -0.70, -0.18, -1.01};
        createEntitiesByYearAndCurrency(2016,9,"CDC", indicators,values9_2016);

        double[] values12_2016 ={2.28,7.64,3.46, 2.66, 1.52,  0.00
                , 0.01, -3.68, 0.00, -0.78, -0.12, -0.78};
        createEntitiesByYearAndCurrency(2016,12,"CDC", indicators,values12_2016);



        double[] values3_2017 ={2.44,7.46,3.47, 2.66, 1.30,  0.00
                , 0.01, -3.87, 0.00, -0.10, -0.02, -1.04};
        createEntitiesByYearAndCurrency(2017,3,"CDC", indicators,values3_2017);

        double[] values6_2017 ={2.51,8.15,3.86, 2.80, 1.48,  0.00
                , 0.01, -3.96, 0.00, -0.58, -0.05, -1.07};
        createEntitiesByYearAndCurrency(2017,6,"CDC", indicators,values6_2017);

        double[] values9_2017 ={3.28,9.98,4.68, 3.32, 1.97,  0.00
                , 0.01, -4.64, 0.00, -0.64, -0.03, -1.41};
        createEntitiesByYearAndCurrency(2017,9,"CDC", indicators,values9_2017);

        double[] values12_2017 ={3.41,10.06,4.45, 3.35, 2.26,  0.00
                , 0.01, -4.56, 0.00, -0.69, -0.21, -1.20};
        createEntitiesByYearAndCurrency(2017,12,"CDC", indicators,values12_2017);




        double[] values1_3_2015 ={2.37,8.58,4.44, 2.78, 1.36,  0.00
                , 0.00, -4.98, 0.00, -0.44, 0.00, -0.78};
        createEntitiesByYearAndCurrency(2015,3,"UGB", indicators,values1_3_2015);

        double[] values1_6_2015 ={1.41,8.08,4.28, 2.69, 1.11,  0.00
                , 0.04, -4.83, 0.00, -1.08, -0.33, -0.46};
        createEntitiesByYearAndCurrency(2015,6,"UGB", indicators,values1_6_2015);

        double[] values1_9_2015 ={1.77,6.74,3.51, 2.22, 1.02,  0.00
                , 0.02, -3.76, 0.00, -0.66, -0.24, -0.33};
        createEntitiesByYearAndCurrency(2015,9,"UGB", indicators,values1_9_2015);

        double[] values1_12_2015 ={1.86,8.07,4.30, 2.56, 1.19,  0.00
                , 0.02, -4.55, 0.00, -0.75, -0.28, -0.65};
        createEntitiesByYearAndCurrency(2015,12,"UGB", indicators,values1_12_2015);


        double[] values1_3_2016 ={2.52,8.71,4.70, 2.48, 1.27,  0.00
                , 0.00, -4.73, 0.00, -0.64, 0.07, -0.88};
        createEntitiesByYearAndCurrency(2016,3,"UGB", indicators,values1_3_2016);

        double[] values1_6_2016 ={2.59,8.65,4.68, 2.37, 1.47,  0.00
                , 0.00, -4.62, 0.00, -0.52, -0.09, -0.83};
        createEntitiesByYearAndCurrency(2016,6,"UGB", indicators,values1_6_2016);

        double[] values1_9_2016 ={2.36,8.56,4.77, 2.34, 1.37,  0.00
                , 0.00, -4.85, 0.00, -0.56, 0.04, -0.83};
        createEntitiesByYearAndCurrency(2016,9,"UGB", indicators,values1_9_2016);

        double[] values1_12_2016 ={2.37,8.27,4.19, 2.43, 1.62,  0.00
                , 0.00, -4.71, 0.00, -0.49, 0.01, -0.71};
        createEntitiesByYearAndCurrency(2016,12,"UGB", indicators,values1_12_2016);


        double[] values1_3_2017 ={2.31,9.06,5.41, 2.64, 1.18,  0.00
                , 0.00, -4.62, 0.00, -1.43, -0.02, -0.69};
        createEntitiesByYearAndCurrency(2017,3,"UGB", indicators,values1_3_2017);

        double[] values1_6_2017 ={2.54,9.64,5.36, 2.78, 1.45,  0.00
                , 0.00, -4.99, 0.00, -1.34, -0.05, -0.73};
        createEntitiesByYearAndCurrency(2017,6,"UGB", indicators,values1_6_2017);

        double[] values1_9_2017 ={2.66,10.31,5.22, 2.94, 1.60,  0.00
                , 0.00, -5.46, 0.00, -1.35, -0.03, -0.79};
        createEntitiesByYearAndCurrency(2017,9,"UGB", indicators,values1_9_2017);

        double[] values1_12_2017 ={3.19,10.74,5.54, 3.20, 1.93,  0.00
                , 0.00, -5.38, 0.00, -1.20, -0.01, -0.96};
        createEntitiesByYearAndCurrency(2017,12,"UGB", indicators,values1_12_2017);






        double[] values2_3_2015 ={1.07,5.42,2.44, 1.48, 1.50,  0.00
                , 0.00, -3.63, 0.00, -0.59, 0.07, -0.20};
        createEntitiesByYearAndCurrency(2015,3,"Bim", indicators,values2_3_2015);

        double[] values2_6_2015 ={0.76,5.14,1.99, 1.48, 1.67,  0.00
                , 0.00, -3.51, 0.00, -0.77, 0.03, -0.14};
        createEntitiesByYearAndCurrency(2015,6,"Bim", indicators,values2_6_2015);

        double[] values2_9_2015 ={0.55,4.94,1.88, 1.49, 1.57,  0.00
                , 0.00, -3.67, 0.00, -0.79, 0.17, -0.10};
        createEntitiesByYearAndCurrency(2015,9,"Bim", indicators,values2_9_2015);

        double[] values2_12_2015 ={0.10,4.98,2.06, 1.42, 1.51,  0.00
                , 0.00, -3.71, 0.00, -0.77, -0.29, -0.12};
        createEntitiesByYearAndCurrency(2015,12,"Bim", indicators,values2_12_2015);


        double[] values2_3_2016 ={1.09,5.75,2.35, 1.76, 1.64,  0.00
                , 0.00, -3.99, 0.00, -0.32, -0.15, -0.21};
        createEntitiesByYearAndCurrency(2016,3,"Bim", indicators,values2_3_2016);

        double[] values2_6_2016 ={0.45,5.08,2.01, 1.67, 1.40,  0.00
                , 0.00, -3.45, 0.00, -1.03, -0.01, -0.14};
        createEntitiesByYearAndCurrency(2016,6,"Bim", indicators,values2_6_2016);

        double[] values2_9_2016 ={0.81,4.96,1.94, 1.58, 1.43,  0.00
                , 0.00, -3.35, 0.00, -0.42, -0.05, -0.33};
        createEntitiesByYearAndCurrency(2016,9,"Bim", indicators,values2_9_2016);

        double[] values2_12_2016 ={0.03,4.57,1.61, 1.44, 1.52,  0.00
                , 0.00, -3.38, 0.00, -0.49, -0.50, -0.18};
        createEntitiesByYearAndCurrency(2016,12,"Bim", indicators,values2_12_2016);


        double[] values2_3_2017 ={0.74,4.91,2.21, 1.41, 1.30,  0.00
                , 0.00, -3.61, 0.00, -0.38, -0.01, -0.18};
        createEntitiesByYearAndCurrency(2017,3,"Bim", indicators,values2_3_2017);

        double[] values2_6_2017 ={0.84,5.22,2.63, 1.41, 1.18,  0.00
                , 0.00, -3.38, 0.00, -0.83, 0.06, -0.23};
        createEntitiesByYearAndCurrency(2017,6,"Bim", indicators,values2_6_2017);

        double[] values2_9_2017 ={0.55,5.60,2.74, 1.57, 1.30,  0.00
                , 0.00, -3.78, 0.00, -1.23, 0.08, -0.13};
        createEntitiesByYearAndCurrency(2017,9,"Bim", indicators,values2_9_2017);

        double[] values2_12_2017 ={0.02,5.56,2.64, 1.51, 1.42,  0.00
                , 0.00, -3.76, 0.00, -1.91, 0.20, -0.08};
        createEntitiesByYearAndCurrency(2017,12,"Bim", indicators,values2_12_2017);





        double[] values3_3_2015 ={1.43,6.06,3.81, 1.50, 0.75,  0.00
                , 0.00, -4.21, 0.00, 0.23, -0.09, -0.56};
        createEntitiesByYearAndCurrency(2015,3,"CDS", indicators,values3_3_2015);

        double[] values3_6_2015 ={1.65,6.40,3.63, 1.66, 1.10,  0.00
                , 0.00, -4.11, 0.00, 0.08, -0.11, -0.61};
        createEntitiesByYearAndCurrency(2015,6,"CDS", indicators,values3_6_2015);

        double[] values3_9_2015 ={1.64,6.22,3.46, 1.58, 1.18,  0.00
                , 0.00, -3.97, 0.00, 0.05, -0.07, -0.58};
        createEntitiesByYearAndCurrency(2015,9,"CDS", indicators,values3_9_2015);

        double[] values3_12_2015 ={1.51,5.58,3.06, 1.43, 1.09,  0.00
                , 0.00, -3.46, 0.00, -0.07, -0.01, -0.52};
        createEntitiesByYearAndCurrency(2015,12,"CDS", indicators,values3_12_2015);


        double[] values3_3_2016 ={2.52,5.96,3.12, 1.36, 1.49,  0.00
                , 0.00, -3.60, 0.00, -0.04, 1.22, -1.02};
        createEntitiesByYearAndCurrency(2016,3,"CDS", indicators,values3_3_2016);

        double[] values3_6_2016 ={1.87,5.27,2.78, 1.23, 1.27,  0.00
                , 0.00, -3.20, 0.00, 0.28, 0.24, -0.71};
        createEntitiesByYearAndCurrency(2016,6,"CDS", indicators,values3_6_2016);

        double[] values3_9_2016 ={1.89,5.42,2.77, 1.36, 1.29,  0.00
                , 0.00, -3.13, 0.00, 0.13, 0.15, -0.68};
        createEntitiesByYearAndCurrency(2016,9,"CDS", indicators,values3_9_2016);

        double[] values3_12_2016 ={1.69,5.51,2.75, 1.40, 1.36,  0.00
                , 0.00, -3.10, 0.00, -0.30, 0.15, -0.57};
        createEntitiesByYearAndCurrency(2016,12,"CDS", indicators,values3_12_2016);


        double[] values3_3_2017 ={1.90,6.01,2.79, 1.65, 1.57,  0.00
                , 0.00, -3.48, 0.00, -0.03, -0.02, -0.58};
        createEntitiesByYearAndCurrency(2017,3,"CDS", indicators,values3_3_2017);

        double[] values3_6_2017 ={2.06,6.09,2.93, 1.64, 1.52,  0.00
                , 0.00, -3.57, 0.00, 0.03, 0.14, -0.65};
        createEntitiesByYearAndCurrency(2017,6,"CDS", indicators,values3_6_2017);

        double[] values3_9_2017 ={2.22,6.54,3.16, 1.73, 1.64,  0.00
                , 0.00, -3.81, 0.00, 0.02, 0.13, -0.66};
        createEntitiesByYearAndCurrency(2017,9,"CDS", indicators,values3_9_2017);

        double[] values3_12_2017 ={2.07,6.75,3.46, 1.66, 1.63,  0.00
                , 0.00, -3.80, 0.00, -0.42, 0.07, -0.54};
        createEntitiesByYearAndCurrency(2017,12,"CDS", indicators,values3_12_2017);





        double[] values4_3_2015 ={1.45,4.94,2.52, 1.28, 1.14,  0.00
                , 0.00, -2.68, 0.11, -0.14, 0.00, -0.78};
        createEntitiesByYearAndCurrency(2015,3,"ABT", indicators,values4_3_2015);

        double[] values4_6_2015 ={1.58,4.93,2.47, 1.27, 1.19,  0.00
                , 0.00, -2.51, 0.11, -0.12, 0.01, -0.85};
        createEntitiesByYearAndCurrency(2015,6,"ABT", indicators,values4_6_2015);

        double[] values4_9_2015 ={1.49,4.78,2.37, 1.25, 1.16,  0.00
                , 0.00, -2.42, 0.10, -0.18, 0.01, -0.80};
        createEntitiesByYearAndCurrency(2015,9,"ABT", indicators,values4_9_2015);

        double[] values4_12_2015 ={1.51,4.54,2.19, 1.23, 1.12,  0.00
                , 0.00, -2.45, 0.10, 0.25, -0.17, -0.75};
        createEntitiesByYearAndCurrency(2015,12,"ABT", indicators,values4_12_2015);


        double[] values4_3_2016 ={1.39,4.57,1.96, 1.31, 1.29,  0.00
                , 0.00, -2.40, 0.10, -0.11, -0.01, -0.75};
        createEntitiesByYearAndCurrency(2016,3,"ABT", indicators,values4_3_2016);

        double[] values4_6_2016 ={1.67,4.66,1.95, 1.39, 1.32,  0.00
                , 0.00, -2.34, 0.10, 0.10, -0.01, -0.85};
        createEntitiesByYearAndCurrency(2016,6,"ABT", indicators,values4_6_2016);

        double[] values4_9_2016 ={1.55,4.59,1.95, 1.36, 1.27,  0.00
                , 0.00, -2.29, 0.09, 0.01, -0.02, -0.84};
        createEntitiesByYearAndCurrency(2016,9,"ABT", indicators,values4_9_2016);

        double[] values4_12_2016 ={1.56,4.61,2.00, 1.34, 1.27,  0.00
                , 0.00, -2.31, 0.09, 0.00, 0.04, -0.88};
        createEntitiesByYearAndCurrency(2016,12,"ABT", indicators,values4_12_2016);


        double[] values4_3_2017 ={1.29,4.72,2.05, 1.37, 1.29,  0.00
                , 0.00, -2.65, 0.11, -0.19, 0.00, -0.70};
        createEntitiesByYearAndCurrency(2017,3,"ABT", indicators,values4_3_2017);

        double[] values4_6_2017 ={1.33,4.51,1.99, 1.13, 1.38,  0.00
                , 0.00, -2.35, 0.10, -0.19, 0.00, -0.73};
        createEntitiesByYearAndCurrency(2017,6,"ABT", indicators,values4_6_2017);

        double[] values4_9_2017 ={1.52,4.75,2.05, 1.33, 1.36,  0.00
                , 0.00, -2.26, 0.10, -0.22, 0.00, -0.83};
        createEntitiesByYearAndCurrency(2017,9,"ABT", indicators,values4_9_2017);

        double[] values4_12_2017 ={1.66,4.63,2.06, 1.28, 1.29,  0.00
                , 0.00, -2.20, 0.09, -0.17, 0.01, -0.70};
        createEntitiesByYearAndCurrency(2017,12,"ABT", indicators,values4_12_2017);





        double[] values5_3_2015 ={2.19,6.70,3.77, 1.65, 1.28,  0.00
                , 0.00, -3.45, 0.00, -0.29, -0.04, -0.73};
        createEntitiesByYearAndCurrency(2015,3,"ABM", indicators,values5_3_2015);

        double[] values5_6_2015 ={2.30,6.45,3.51, 1.86, 1.09,  0.00
                , 0.00, -3.26, 0.00, -0.25, 0.12, -0.77};
        createEntitiesByYearAndCurrency(2015,6,"ABM", indicators,values5_6_2015);

        double[] values5_9_2015 ={2.09,6.69,3.66, 1.86, 1.17,  0.00
                , 0.00, -3.48, 0.00, -0.48, 0.06, -0.70};
        createEntitiesByYearAndCurrency(2015,9,"ABM", indicators,values5_9_2015);

        double[] values5_12_2015 ={2.31,6.71,3.86, 1.71, 1.14,  0.00
                , 0.00, -3.24, 0.00, -0.46, 0.06, -0.77};
        createEntitiesByYearAndCurrency(2015,12,"ABM", indicators,values5_12_2015);


        double[] values5_3_2016 ={2.37,7.67,4.04, 2.18, 1.46,  0.00
                , 0.00, -3.90, 0.00, -0.48, -0.13, -0.79};
        createEntitiesByYearAndCurrency(2016,3,"ABM", indicators,values5_3_2016);

        double[] values5_6_2016 ={2.49,7.67,4.62, 1.89, 1.15,  0.00
                , 0.00, -3.68, 0.00, -0.52, -0.14, -0.83};
        createEntitiesByYearAndCurrency(2016,6,"ABM", indicators,values5_6_2016);

        double[] values5_9_2016 ={2.51,7.83,4.90, 1.83, 1.10,  0.00
                , 0.00, -3.71, 0.00, -0.56, -0.21, -0.84};
        createEntitiesByYearAndCurrency(2016,9,"ABM", indicators,values5_9_2016);

        double[] values5_12_2016 ={2.71,7.78,4.86, 1.81, 1.11,  0.00
                , 0.00, -3.58, 0.00, -0.47, -0.11, -0.90};
        createEntitiesByYearAndCurrency(2016,12,"ABM", indicators,values5_12_2016);


        double[] values5_3_2017 ={2.46,7.85,4.71, 2.15, 0.98,  0.00
                , 0.00, -3.68, 0.00, -0.60, -0.29, -0.82};
        createEntitiesByYearAndCurrency(2017,3,"ABM", indicators,values5_3_2017);

        double[] values5_6_2017 ={2.43,7.28,4.46, 1.94, 0.89,  0.00
                , 0.00, -3.36, 0.00, -0.49, -0.20, -0.81};
        createEntitiesByYearAndCurrency(2017,6,"ABM", indicators,values5_6_2017);

        double[] values5_9_2017 ={2.72,7.68,5.01, 1.84, 0.82,  0.00
                , 0.00, -3.36, 0.00, -0.50, -0.20, -0.91};
        createEntitiesByYearAndCurrency(2017,9,"ABM", indicators,values5_9_2017);

        double[] values5_12_2017 ={2.70,7.51,4.87, 1.81, 0.82,  0.00
                , 0.00, -3.23, 0.00, -0.73, 0.04, -0.90};
        createEntitiesByYearAndCurrency(2017,12,"ABM", indicators,values5_12_2017);


       /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       /////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        String[] indicators2 = {"Dépôts clientèle (Spot)","Dépôts rémunérés (Spot)","DAT (Spot)", "Comptes d'épargne (Spot)"
                , "Certificats de dépôt (Spot)", "Dépôts non rémunérés (Spot)"
                , "Dont DAV (Spot)", "poids dépôts non rémunérés(Spot)"};


        double[] values6_1_2015 ={4496 , 2637 , 864 , 1624 , 150 , 1859 , 1707 , 152 };
        createEntitiesByYearAndCurrency(2015,1,"ABT", indicators2,values6_1_2015);

        double[] values6_2_2015 ={4554 , 2659 , 847 , 1632 , 180 , 1895 , 1759 , 136 };
        createEntitiesByYearAndCurrency(2015,2,"ABT", indicators2,values6_2_2015);

        double[] values6_3_2015 ={4605 , 2681 , 868 , 1642 , 172 , 1924 , 1793 , 132 };
        createEntitiesByYearAndCurrency(2015,3,"ABT", indicators2,values6_3_2015);

        double[] values6_4_2015 ={4597 , 2649 , 852 , 1644 , 154 , 1947 , 1815 , 133 };
        createEntitiesByYearAndCurrency(2015,4,"ABT", indicators2,values6_4_2015);

        double[] values6_5_2015 ={4661 , 2682 , 880 , 1625 , 176 , 1979 , 1799 , 181 };
        createEntitiesByYearAndCurrency(2015,5,"ABT", indicators2,values6_5_2015);

        double[] values6_6_2015 ={4738 , 2693 ,884 , 1637 ,172 , 2045 , 1845 ,200 };
        createEntitiesByYearAndCurrency(2015,6,"ABT", indicators2,values6_6_2015);

        double[] values6_7_2015 ={4672 , 2697 ,886 , 1650 ,161 , 1976 , 1811 ,165 };
        createEntitiesByYearAndCurrency(2015,7,"ABT", indicators2,values6_7_2015);

        double[] values6_8_2015 ={4718 , 2703 ,908 , 1640 ,155 , 2016 , 1836 ,179 };
        createEntitiesByYearAndCurrency(2015,8,"ABT", indicators2,values6_8_2015);

        double[] values6_9_2015 ={4784 , 2733 ,911 , 1668 ,154 , 2051 , 1883 ,168 };
        createEntitiesByYearAndCurrency(2015,9,"ABT", indicators2,values6_9_2015);

        double[] values6_10_2015 ={4835 , 2703 ,909 , 1667 ,128 , 2132 , 1966 ,166 };
        createEntitiesByYearAndCurrency(2015,10,"ABT", indicators2,values6_10_2015);

        double[] values6_11_2015 ={4813 , 2726 ,916 , 1667 ,143 , 2086 , 1917 ,170 };
        createEntitiesByYearAndCurrency(2015,11,"ABT", indicators2,values6_11_2015);

        double[] values6_12_2015 ={5460 , 2908 ,998 , 1823 ,87 , 2552 , 2344 ,209 };
        createEntitiesByYearAndCurrency(2015,12,"ABT", indicators2,values6_12_2015);




        double[] values6_1_2016 ={4901 , 2750 ,913 , 1704 ,134 , 2151 , 1994 ,157 };
        createEntitiesByYearAndCurrency(2016,1,"ABT", indicators2,values6_1_2016);

        double[] values6_2_2016 ={4955 , 2741 ,910 , 1709 ,123 , 2214 , 2043 ,171 };
        createEntitiesByYearAndCurrency(2016,2,"ABT", indicators2,values6_2_2016);

        double[] values6_3_2016 ={5079 , 2806 ,942 , 1725 ,138 , 2273 , 2100 ,174 };
        createEntitiesByYearAndCurrency(2016,3,"ABT", indicators2,values6_3_2016);

        double[] values6_4_2016 ={5018 , 2793 ,966 , 1733 ,94 , 2225 , 2061 ,164 };
        createEntitiesByYearAndCurrency(2016,4,"ABT", indicators2,values6_4_2016);

        double[] values6_5_2016 ={5062 , 2787 ,968 , 1736 ,83 , 2275 , 2110 ,165 };
        createEntitiesByYearAndCurrency(2016,5,"ABT", indicators2,values6_5_2016);

        double[] values6_6_2016 ={5222 , 2797 ,968 , 1747 ,83 , 2424 , 2252 ,172 };
        createEntitiesByYearAndCurrency(2016,6,"ABT", indicators2,values6_6_2016);

        double[] values6_7_2016 ={5247 , 2819 ,979 , 1765 ,76 , 2428 , 2252 ,176 };
        createEntitiesByYearAndCurrency(2016,7,"ABT", indicators2,values6_7_2016);

        double[] values6_8_2016 ={5213 , 2845 ,995 , 1766 ,83 , 2368 , 2198 ,170 };
        createEntitiesByYearAndCurrency(2016,8,"ABT", indicators2,values6_8_2016);

        double[] values6_9_2016 ={5352 , 2871 ,994 , 1796 ,81 , 2480 , 2260 ,221 };
        createEntitiesByYearAndCurrency(2016,9,"ABT", indicators2,values6_9_2016);

        double[] values6_10_2016 ={5310 , 2888 ,985 , 1816 ,87 , 2422 , 2245 ,177 };
        createEntitiesByYearAndCurrency(2016,10,"ABT", indicators2,values6_10_2016);

        double[] values6_11_2016 ={5323 , 2939 , 1001 , 1825 ,113 , 2384 , 2192 ,192 };
        createEntitiesByYearAndCurrency(2016,11,"ABT", indicators2,values6_11_2016);

        double[] values6_12_2016 ={5460 , 2908 ,998 , 1823 ,87 , 2552 , 2344 ,209 };
        createEntitiesByYearAndCurrency(2016,12,"ABT", indicators2,values6_12_2016);






        double[] values6_1_2017 ={5415 , 2920 , 1003 , 1843 ,75 , 2494 , 2302 ,192 };
        createEntitiesByYearAndCurrency(2017,1,"ABT", indicators2,values6_1_2017);

        double[] values6_2_2017 ={5435 , 2933 , 1005 , 1854 ,74 , 2503 , 2320 ,183 };
        createEntitiesByYearAndCurrency(2017,2,"ABT", indicators2,values6_2_2017);

        double[] values6_3_2017 ={5654 , 2955 , 1001 , 1870 ,85 , 2698 , 2489 ,209 };
        createEntitiesByYearAndCurrency(2017,3,"ABT", indicators2,values6_3_2017);

        double[] values6_4_2017 ={5583 , 2926 ,992 , 1869 ,65 , 2657 , 2436 ,221 };
        createEntitiesByYearAndCurrency(2017,4,"ABT", indicators2,values6_4_2017);

        double[] values6_5_2017 ={5638 , 2909 ,980 , 1870 ,60 , 2729 , 2516 ,213 };
        createEntitiesByYearAndCurrency(2017,5,"ABT", indicators2,values6_5_2017);

        double[] values6_6_2017 ={5836 , 2945 ,974 , 1895 ,76 , 2891 , 2642 ,250 };
        createEntitiesByYearAndCurrency(2017,6,"ABT", indicators2,values6_6_2017);

        double[] values6_7_2017 ={5739 , 2935 ,950 , 1902 ,83 , 2804 , 2541 ,262 };
        createEntitiesByYearAndCurrency(2017,7,"ABT", indicators2,values6_7_2017);

        double[] values6_8_2017 ={5790 , 2942 ,964 , 1922 ,56 , 2848 , 2573 ,275 };
        createEntitiesByYearAndCurrency(2017,8,"ABT", indicators2,values6_8_2017);

        double[] values6_9_2017 ={5973 , 3002 , 1000 , 1949 ,54 , 2971 , 2693 ,278 };
        createEntitiesByYearAndCurrency(2017,9,"ABT", indicators2,values6_9_2017);

        double[] values6_10_2017 ={5863 , 3012 ,980 , 1967 ,66 , 2851 , 2629 ,222 };
        createEntitiesByYearAndCurrency(2017,10,"ABT", indicators2,values6_10_2017);

        double[] values6_11_2017 ={5868 , 3043 ,973 , 1983 ,87 , 2825 , 2595 ,229 };
        createEntitiesByYearAndCurrency(2017,11,"ABT", indicators2,values6_11_2017);

        double[] values6_12_2017 ={6002 , 3043 ,988 , 1998 ,57 , 2959 , 2692 ,267 };
        createEntitiesByYearAndCurrency(2017,12,"ABT", indicators2,values6_12_2017);




        double[] values7_1_2015 ={36661 , 4916 , 4016 ,900, 0 , 31746 , 24654 , 7092 };
        createEntitiesByYearAndCurrency(2015,1,"ABM", indicators2,values7_1_2015);

        double[] values7_2_2015 ={39579 , 9987 , 4994 , 4994,0 , 29592 , 23249 , 6343 };
        createEntitiesByYearAndCurrency(2015,2,"ABM", indicators2,values7_2_2015);

        double[] values7_3_2015 ={46869 , 12452 , 6226 , 6226,0 , 34417 , 27767 , 6649 };
        createEntitiesByYearAndCurrency(2015,3,"ABM", indicators2,values7_3_2015);

        double[] values7_4_2015 ={40441 , 6927 , 5957 ,970,0 , 33514 , 26187 , 7327 };
        createEntitiesByYearAndCurrency(2015,4,"ABM", indicators2,values7_4_2015);

        double[] values7_5_2015 ={41663 , 7065 , 6124 ,940,0 , 34598 , 27264 , 7335 };
        createEntitiesByYearAndCurrency(2015,5,"ABM", indicators2,values7_5_2015);

        double[] values7_6_2015 ={46212 , 7321 , 6390 ,931,0 , 38892 , 30827 , 8064 };
        createEntitiesByYearAndCurrency(2015,6,"ABM", indicators2,values7_6_2015);

        double[] values7_7_2015 ={40394 , 7369 , 6427 ,942,0 , 33025 , 27335 , 5690 };
        createEntitiesByYearAndCurrency(2015,7,"ABM", indicators2,values7_7_2015);

        double[] values7_8_2015 ={44324 , 7789 , 6828 ,961,0 , 36535 , 30741 , 5794 };
        createEntitiesByYearAndCurrency(2015,8,"ABM", indicators2,values7_8_2015);

        double[] values7_9_2015 ={46061 , 11157 , 10153 , 1004,0 , 34903 , 28905 , 5998 };
        createEntitiesByYearAndCurrency(2015,9,"ABM", indicators2,values7_9_2015);

        double[] values7_10_2015 ={47001 , 10200 , 9163 , 1037,0 , 36801 , 30059 , 6741 };
        createEntitiesByYearAndCurrency(2015,10,"ABM", indicators2,values7_10_2015);

        double[] values7_11_2015 ={47200 , 11061 , 10017 , 1044,0 , 36138 , 28346 , 7792 };
        createEntitiesByYearAndCurrency(2015,11,"ABM", indicators2,values7_11_2015);

        double[] values7_12_2015 ={49665 , 11166 , 10092 , 1074, 0, 38500 , 31696 , 6803 };
        createEntitiesByYearAndCurrency(2015,12,"ABM", indicators2,values7_12_2015);


        double[] values7_1_2016 ={49526 , 10457 , 9378 , 1079,0 , 39068 , 31332 , 7736 };
        createEntitiesByYearAndCurrency(2016,1,"ABM", indicators2,values7_1_2016);

        double[] values7_2_2016 ={50417 , 9890 , 8780 , 1110, 0, 40527 , 32721 , 7806 };
        createEntitiesByYearAndCurrency(2016,2,"ABM", indicators2,values7_2_2016);

        double[] values7_3_2016 ={50881 , 12351 , 11245 , 1106,0 , 38530 , 30920 , 7610 };
        createEntitiesByYearAndCurrency(2016,3,"ABM", indicators2,values7_2_2016);

        double[] values7_4_2016 ={51627 , 13174 , 12041 , 1133,0 , 38453 , 30942 , 7510 };
        createEntitiesByYearAndCurrency(2016,4,"ABM", indicators2,values7_3_2016);

        double[] values7_5_2016 ={48094 , 11424 , 10414 , 1009,0 , 36670 , 28422 , 8249 };
        createEntitiesByYearAndCurrency(2016,5,"ABM", indicators2,values7_4_2016);

        double[] values7_6_2016 ={53663 , 11618 , 10612 , 1007,0 , 42045 , 34390 , 7654 };
        createEntitiesByYearAndCurrency(2016,6,"ABM", indicators2,values7_5_2016);

        double[] values7_7_2016 ={53710 , 14823 , 13821 , 1002 ,0 , 38887 , 31456 , 7430 };
        createEntitiesByYearAndCurrency(2016,7,"ABM", indicators2,values7_6_2016);

        double[] values7_8_2016 ={50459 , 13609 , 12601 , 1008,0 , 36850 , 29974 , 6876 };
        createEntitiesByYearAndCurrency(2016,8,"ABM", indicators2,values7_7_2016);

        double[] values7_9_2016 ={51182 , 14082 , 13052 , 1030,0 , 37099 , 30579 , 6521 };
        createEntitiesByYearAndCurrency(2016,9,"ABM", indicators2,values7_8_2016);

        double[] values7_10_2016 ={50286 , 12365 , 11351 , 1013 ,0 , 37922 , 32376 , 5546 };
        createEntitiesByYearAndCurrency(2016,10,"ABM", indicators2,values7_9_2016);

        double[] values7_11_2016 ={49824 , 12425 , 11402 , 1023,0 , 37399 , 30999 , 6400 };
        createEntitiesByYearAndCurrency(2016,11,"ABM", indicators2,values7_10_2016);

        double[] values7_12_2016 ={53069 , 12894 , 11732 , 1162 , 0, 40176 , 33540 , 6636 };
        createEntitiesByYearAndCurrency(2016,12,"ABM", indicators2,values7_12_2016);






        double[] values7_1_2017 ={49780 , 10293 , 9057 , 1236,0, 39487 , 32768 , 6719 };
        createEntitiesByYearAndCurrency(2017,1,"ABM", indicators2,values7_1_2017);

        double[] values7_2_2017 ={49319 , 12761 , 11514 , 1247,0, 36558 , 31214 , 5344 };
        createEntitiesByYearAndCurrency(2017,2,"ABM", indicators2,values7_2_2017);

        double[] values7_3_2017 ={50124 , 10966 , 9688 , 1279,0, 39157 , 34854 , 4303 };
        createEntitiesByYearAndCurrency(2017,3,"ABM", indicators2,values7_3_2017);

        double[] values7_4_2017 ={54633 , 10922 , 9669 , 1253,0, 43711 , 33489 , 10222 };
        createEntitiesByYearAndCurrency(2017,4,"ABM", indicators2,values7_4_2017);

        double[] values7_5_2017 ={56736 , 11107 , 9886 , 1221,0, 45629 , 36831 , 8799 };
        createEntitiesByYearAndCurrency(2017,5,"ABM", indicators2,values7_5_2017);

        double[] values7_6_2017 ={59050 , 10791 , 9514 , 1277,0, 48259 , 39591 , 8667 };
        createEntitiesByYearAndCurrency(2017,6,"ABM", indicators2,values7_6_2017);

        double[] values7_7_2017 ={57615 , 13076 , 11819 , 1257,0, 44540 , 39821 , 4718 };
        createEntitiesByYearAndCurrency(2017,7,"ABM", indicators2,values7_7_2017);

        double[] values7_8_2017 ={57520 , 13560 , 12247 , 1313,0, 43960 , 39246 , 4714 };
        createEntitiesByYearAndCurrency(2017,8,"ABM", indicators2,values7_8_2017);

        double[] values7_9_2017 ={57307 , 15356 , 14017 , 1339,0, 41951 , 34830 , 7121 };
        createEntitiesByYearAndCurrency(2017,9,"ABM", indicators2,values7_9_2017);

        double[] values7_10_2017 ={55076 , 13439 , 12065 , 1374,0, 41637 , 35900 , 5737 };
        createEntitiesByYearAndCurrency(2017,10,"ABM", indicators2,values7_10_2017);

        double[] values7_11_2017 ={53883 , 13677 , 12237 , 1439,0, 40206 , 33051 , 7155 };
        createEntitiesByYearAndCurrency(2017,11,"ABM", indicators2,values7_11_2017);

        double[] values7_12_2017 ={58413 , 12561 , 11075 , 1487,0, 45852 , 38941 , 6911 };
        createEntitiesByYearAndCurrency(2017,12,"ABM", indicators2,values7_12_2015);




        double[] values8_1_2015 ={556343 , 254553 , 69212 , 179241 , 6100 , 301790 , 268328 , 33462 };
        createEntitiesByYearAndCurrency(2015,1,"CBAO", indicators2,values8_1_2015);

        double[] values8_2_2015 ={556130 , 252716 , 66743 , 179873 , 6100 , 303414 , 271725 , 31689 };
        createEntitiesByYearAndCurrency(2015,2,"CBAO", indicators2,values8_2_2015);

        double[] values8_3_2015 ={582981 , 246288 , 60580 , 179608 , 6100 , 336693 , 303897 , 32796 };
        createEntitiesByYearAndCurrency(2015,3,"CBAO", indicators2,values8_3_2015);

        double[] values8_4_2015 ={568618 , 246936 , 60374 , 180462 , 6100 , 321682 , 288133 , 33548 };
        createEntitiesByYearAndCurrency(2015,4,"CBAO", indicators2,values8_4_2015);

        double[] values8_5_2015 ={576988 , 250092 , 61154 , 182838 , 6100 , 326896 , 294120 , 32776 };
        createEntitiesByYearAndCurrency(2015,5,"CBAO", indicators2,values8_5_2015);

        double[] values8_6_2015 ={573627 , 248928 , 59098 , 183730 , 6100 , 324699 , 289427 , 35272 };
        createEntitiesByYearAndCurrency(2015,6,"CBAO", indicators2,values8_6_2015);

        double[] values8_7_2015 ={582968 , 256422 , 62051 , 188271 , 6100 , 326546 , 293448 , 33098 };
        createEntitiesByYearAndCurrency(2015,7,"CBAO", indicators2,values8_7_2015);

        double[] values8_8_2015 ={607844 , 258081 , 62141 , 189840 , 6100 , 349763 , 313479 , 36283 };
        createEntitiesByYearAndCurrency(2015,8,"CBAO", indicators2,values8_8_2015);

        double[] values8_9_2015 ={601200 , 258071 , 59765 , 192206 , 6100 , 343129 , 307194 , 35935 };
        createEntitiesByYearAndCurrency(2015,9,"CBAO", indicators2,values8_9_2015);

        double[] values8_10_2015 ={620766 , 253964 , 53693 , 194171 , 6100 , 366802 , 332374 , 34428 };
        createEntitiesByYearAndCurrency(2015,10,"CBAO", indicators2,values8_10_2015);

        double[] values8_11_2015 ={610889 , 255142 , 54421 , 194620 , 6100 , 355747 , 321436 , 34311 };
        createEntitiesByYearAndCurrency(2015,11,"CBAO", indicators2,values8_11_2015);

        double[] values8_12_2015 ={631742 , 262379 , 62432 , 193847 , 6100 , 369364 , 329825 , 39538 };
        createEntitiesByYearAndCurrency(2015,12,"CBAO", indicators2,values8_12_2015);



        double[] values8_1_2016 ={620227 , 262033 , 61262 , 194671 , 6100 , 358193 , 317636 , 40558 };
        createEntitiesByYearAndCurrency(2016,1,"CBAO", indicators2,values8_1_2016);

        double[] values8_2_2016 ={596209 , 264172 , 63065 , 195006 , 6100 , 332037 , 292407 , 39630 };
        createEntitiesByYearAndCurrency(2016,2,"CBAO", indicators2,values8_2_2016);

        double[] values8_3_2016 ={602273 , 264731 , 63624 , 195007 , 6100 , 337541 , 294342 , 43199 };
        createEntitiesByYearAndCurrency(2016,3,"CBAO", indicators2,values8_3_2016);

        double[] values8_4_2016 ={618409 , 267917 , 63791 , 198026 , 6100 , 350493 , 309368 , 41125 };
        createEntitiesByYearAndCurrency(2016,4,"CBAO", indicators2,values8_4_2016);

        double[] values8_5_2016 ={617484 , 259080 , 54631 , 198349 , 6100 , 358404 , 318039 , 40365 };
        createEntitiesByYearAndCurrency(2016,5,"CBAO", indicators2,values8_5_2016);

        double[] values8_6_2016 ={640873 , 270667 , 64576 , 199991 , 6100 , 370207 , 327078 , 43129 };
        createEntitiesByYearAndCurrency(2016,6,"CBAO", indicators2,values8_6_2016);

        double[] values8_7_2016 ={632841 , 275568 , 64729 , 204740 , 6100 , 357273 , 315509 , 41764 };
        createEntitiesByYearAndCurrency(2016,7,"CBAO", indicators2,values8_7_2016);

        double[] values8_8_2016 ={630262 , 273570 , 60950 , 206520 , 6100 , 356691 , 315892 , 40800 };
        createEntitiesByYearAndCurrency(2016,8,"CBAO", indicators2,values8_8_2016);

        double[] values8_9_2016 ={601200 , 258071 , 59765 , 192206 , 6100 , 343129 , 307194 , 35935 };
        createEntitiesByYearAndCurrency(2016,9,"CBAO", indicators2,values8_9_2016);

        double[] values8_10_2016 ={620766 , 253964 , 53693 , 194171 , 6100 , 366802 , 332374 , 34428 };
        createEntitiesByYearAndCurrency(2016,10,"CBAO", indicators2,values8_10_2016);

        double[] values8_11_2016 ={610889 , 255142 , 54421 , 194620 , 6100 , 355747 , 321436 , 34311 };
        createEntitiesByYearAndCurrency(2016,11,"CBAO", indicators2,values8_11_2016);

        double[] values8_12_2016 ={662935 , 273645 , 54800 , 212744 , 6100 , 389291 , 341590 , 47701 };
        createEntitiesByYearAndCurrency(2016,12,"CBAO", indicators2,values8_12_2016);





        double[] values8_1_2017 ={647595 , 276111 , 55918 , 214092 , 6100 , 371484 , 333096 , 38388 };
        createEntitiesByYearAndCurrency(2017,1,"CBAO", indicators2,values8_1_2017);

        double[] values8_2_2017 ={649410 , 273430 , 53088 , 214242 , 6100 , 375980 , 338323 , 37657 };
        createEntitiesByYearAndCurrency(2017,2,"CBAO", indicators2,values8_2_2017);

        double[] values8_3_2017 ={637409 , 271821 , 52489 , 213232 , 6100 , 365587 , 327194 , 38393 };
        createEntitiesByYearAndCurrency(2017,3,"CBAO", indicators2,values8_3_2017);

        double[] values8_4_2017 ={649321 , 273145 , 52843 , 214201 , 6100 , 376176 , 337528 , 38648 };
        createEntitiesByYearAndCurrency(2017,4,"CBAO", indicators2,values8_4_2017);

        double[] values8_5_2017 ={667245 , 271760 , 51380 , 214280 , 6100 , 395486 , 356200 , 39285 };
        createEntitiesByYearAndCurrency(2017,5,"CBAO", indicators2,values8_5_2017);

        double[] values8_6_2017 ={674621 , 274244 , 51332 , 216812 , 6100 , 400378 , 361809 , 38569 };
        createEntitiesByYearAndCurrency(2017,6,"CBAO", indicators2,values8_6_2017);

        double[] values8_7_2017 ={679079 , 278132 , 51886 , 220147 , 6100 , 400946 , 362482 , 38464 };
        createEntitiesByYearAndCurrency(2017,7,"CBAO", indicators2,values8_7_2017);

        double[] values8_8_2017 ={676414 , 280242 , 53542 , 220601 , 6100 , 396172 , 350911 , 45261 };
        createEntitiesByYearAndCurrency(2017,8,"CBAO", indicators2,values8_8_2017);

        double[] values8_9_2017 ={684436 , 282968 , 52333 , 224535 , 6100 , 401469 , 356744 , 44725 };
        createEntitiesByYearAndCurrency(2017,9,"CBAO", indicators2,values8_9_2017);

        double[] values8_10_2017 ={698194 , 279406 , 47097 , 226209 , 6100 , 418788 , 362744 , 56044 };
        createEntitiesByYearAndCurrency(2017,10,"CBAO", indicators2,values8_10_2017);

        double[] values8_11_2017 ={698194 , 279406 , 47097 , 226209 , 6100 , 418788 , 368744 , 50044 };
        createEntitiesByYearAndCurrency(2017,11,"CBAO", indicators2,values8_11_2017);

        double[] values8_12_2017 ={711429 , 278875 , 48392 , 224383 , 6100 , 432554 , 374744 , 57810 };
        createEntitiesByYearAndCurrency(2017,12,"CBAO", indicators2,values8_12_2017);





        double[] values9_1_2015 ={114056 , 32328 , 21801 , 10527 ,0 , 81728 , 80003 , 1725 };
        createEntitiesByYearAndCurrency(2015,1,"CDS", indicators2,values9_1_2015);

        double[] values9_2_2015 ={116827 , 32128 , 21662 , 10466 ,0 , 84699 , 82878 , 1821 };
        createEntitiesByYearAndCurrency(2015,2,"CDS", indicators2,values9_2_2015);

        double[] values9_3_2015 ={115933 , 32273 , 21752 , 10521 ,0 , 83660 , 81247 , 2413 };
        createEntitiesByYearAndCurrency(2015,3,"CDS", indicators2,values9_3_2015);

        double[] values9_4_2015 ={111480 , 32179 , 21769 , 10410 ,0 , 79301 , 77076 , 2225 };
        createEntitiesByYearAndCurrency(2015,4,"CDS", indicators2,values9_4_2015);

        double[] values9_5_2015 ={109110 , 32225 , 21644 , 10581 ,0 , 76885 , 74871 , 2014 };
        createEntitiesByYearAndCurrency(2015,5,"CDS", indicators2,values9_5_2015);

        double[] values9_6_2015 ={108008 , 32502 , 21595 , 10907 ,0 , 75506 , 72807 , 2699 };
        createEntitiesByYearAndCurrency(2015,6,"CDS", indicators2,values9_6_2015);

        double[] values9_7_2015 ={107200 , 33394 , 22272 , 11122 ,0 , 73806 , 71552 , 2254 };
        createEntitiesByYearAndCurrency(2015,7,"CDS", indicators2,values9_7_2015);

        double[] values9_8_2015 ={107896 , 33756 , 22500 , 11256 ,0 , 74140 , 71559 , 2581 };
        createEntitiesByYearAndCurrency(2015,8,"CDS", indicators2,values9_8_2015);

        double[] values9_9_2015 ={112007 , 33598 , 22257 , 11341 ,0 , 78409 , 76125 , 2284 };
        createEntitiesByYearAndCurrency(2015,9,"CDS", indicators2,values9_9_2015);

        double[] values9_10_2015 ={132348 , 33744 , 22410 , 11334 ,0 , 98604 , 96462 , 2142 };
        createEntitiesByYearAndCurrency(2015,10,"CDS", indicators2,values9_10_2015);

        double[] values9_11_2015 ={126545 , 34030 , 22264 , 11766 ,0 , 92515 , 90153 , 2362 };
        createEntitiesByYearAndCurrency(2015,11,"CDS", indicators2,values9_11_2015);

        double[] values9_12_2015 ={124374 , 33248 , 21381 , 11867 ,0 , 91126 , 89066 , 2060 };
        createEntitiesByYearAndCurrency(2015,12,"CDS", indicators2,values9_12_2015);




        double[] values9_1_2016 ={124632 , 33294 , 21459 , 11835 ,0 , 91338 , 88642 , 2696 };
        createEntitiesByYearAndCurrency(2016,1,"CDS", indicators2,values9_1_2016);

        double[] values9_2_2016 ={131206 , 33354 , 21552 , 11802 ,0 , 97852 , 95435 , 2417 };
        createEntitiesByYearAndCurrency(2016,2,"CDS", indicators2,values9_2_2016);

        double[] values9_3_2016 ={125497 , 33117 , 21422 , 11695 ,0 , 92380 , 90018 , 2362 };
        createEntitiesByYearAndCurrency(2016,3,"CDS", indicators2,values9_3_2016);

        double[] values9_4_2016 ={121968 , 32861 , 21019 , 11842 ,0 , 89107 , 86345 , 2762 };
        createEntitiesByYearAndCurrency(2016,4,"CDS", indicators2,values9_4_2016);

        double[] values9_5_2016 ={127604 , 33145 , 21253 , 11892 ,0 , 94459 , 90432 , 4027 };
        createEntitiesByYearAndCurrency(2016,5,"CDS", indicators2,values9_5_2016);

        double[] values9_6_2016 ={140098 , 44460 , 32600 , 11860 ,0 , 95638 , 93068 , 2570 };
        createEntitiesByYearAndCurrency(2016,6,"CDS", indicators2,values9_6_2016);

        double[] values9_7_2016 ={150274 , 44363 , 32431 , 11932 ,0 , 105911 , 104229 , 1682 };
        createEntitiesByYearAndCurrency(2016,7,"CDS", indicators2,values9_7_2016);

        double[] values9_8_2016 ={149068 , 43924 , 32174 , 11750 ,0 , 105144 , 102950 , 2194 };
        createEntitiesByYearAndCurrency(2016,8,"CDS", indicators2,values9_8_2016);

        double[] values9_9_2016 ={145581 , 43417 , 31638 , 11779 ,0 , 102164 , 99969 , 2195 };
        createEntitiesByYearAndCurrency(2016,9,"CDS", indicators2,values9_9_2016);

        double[] values9_10_2016 ={145442 , 38311 , 26473 , 11838 ,0 , 107131 , 104712 , 2419 };
        createEntitiesByYearAndCurrency(2016,10,"CDS", indicators2,values9_10_2016);

        double[] values9_11_2016 ={139581 , 37333 , 25405 , 11928 ,0 , 102248 , 100145 , 2103 };
        createEntitiesByYearAndCurrency(2016,11,"CDS", indicators2,values9_11_2016);

        double[] values9_12_2016 ={142202 , 36628 , 24126 , 12502 ,0 , 105574 , 103725 , 1849 };
        createEntitiesByYearAndCurrency(2016,12,"CDS", indicators2,values9_12_2016);




        double[] values9_1_2017 ={132298 , 36838 , 24131 , 12707 ,0 , 95460 , 93806 , 1654 };
        createEntitiesByYearAndCurrency(2017,1,"CDS", indicators2,values9_1_2017);

        double[] values9_2_2017 ={127862 , 36336 , 23685 , 12651 ,0 , 91526 , 89771 , 1755 };
        createEntitiesByYearAndCurrency(2017,2,"CDS", indicators2,values9_2_2017);

        double[] values9_3_2017 ={126109 , 36415 , 23841 , 12574 ,0 , 89694 , 88096 , 1598 };
        createEntitiesByYearAndCurrency(2017,3,"CDS", indicators2,values9_3_2017);

        double[] values9_4_2017 ={128579 , 36387 , 23818 , 12569 ,0 , 92192 , 90220 , 1972 };
        createEntitiesByYearAndCurrency(2017,4,"CDS", indicators2,values9_4_2017);

        double[] values9_5_2017 ={124054 , 36455 , 23738 , 12717 ,0 , 87599 , 86037 , 1562 };
        createEntitiesByYearAndCurrency(2017,5,"CDS", indicators2,values9_5_2017);

        double[] values9_6_2017 ={124114 , 25900 , 13380 , 12520 ,0 , 98214 , 96449 , 1765 };
        createEntitiesByYearAndCurrency(2017,6,"CDS", indicators2,values9_6_2017);

        double[] values9_7_2017 ={117057 , 26528 , 13769 , 12759 ,0 , 90529 , 88907 , 1622 };
        createEntitiesByYearAndCurrency(2017,7,"CDS", indicators2,values9_7_2017);

        double[] values9_8_2017 ={112897 , 26388 , 13708 , 12680 ,0 , 86509 , 84630 , 1879 };
        createEntitiesByYearAndCurrency(2017,8,"CDS", indicators2,values9_8_2017);

        double[] values9_9_2017 ={113742 , 23248 , 10535 , 12713 ,0 , 90494 , 88612 , 1882 };
        createEntitiesByYearAndCurrency(2017,9,"CDS", indicators2,values9_9_2017);

        double[] values9_10_2017 ={118328 , 23441 , 10721 , 12720 ,0 , 94887 , 92964 , 1923 };
        createEntitiesByYearAndCurrency(2017,10,"CDS", indicators2,values9_10_2017);

        double[] values9_11_2017 ={113911 , 23638 , 10832 , 12806 ,0 , 90273 , 88514 , 1759 };
        createEntitiesByYearAndCurrency(2017,11,"CDS", indicators2,values9_11_2017);

        double[] values9_12_2017 ={120210 , 23910 , 10768 , 13142 ,0 , 96300 , 93664 , 2636 };
        createEntitiesByYearAndCurrency(2017,12,"CDS", indicators2,values9_12_2017);





        double[] values10_1_2015 ={419511 , 169099 , 64101 , 104998 ,0 , 250412 , 239433 , 10978 };
        createEntitiesByYearAndCurrency(2015,1,"SIB", indicators2,values10_1_2015);

        double[] values10_2_2015 ={423169 , 180969 , 76004 , 104965 ,0 , 242200 , 228952 , 13247 };
        createEntitiesByYearAndCurrency(2015,2,"SIB", indicators2,values10_2_2015);

        double[] values10_3_2015 ={459149 , 182290 , 75813 , 106477 ,0 , 276859 , 259600 , 17259 };
        createEntitiesByYearAndCurrency(2015,3,"SIB", indicators2,values10_3_2015);

        double[] values10_4_2015 ={465225 , 191026 , 83922 , 107104 ,0 , 274199 , 262726 , 11473 };
        createEntitiesByYearAndCurrency(2015,4,"SIB", indicators2,values10_4_2015);

        double[] values10_5_2015 ={489585 , 192466 , 84793 , 107673 ,0 , 297119 , 285786 , 11333 };
        createEntitiesByYearAndCurrency(2015,5,"SIB", indicators2,values10_5_2015);

        double[] values10_6_2015 ={487500 , 190341 , 82019 , 108322 ,0 , 297159 , 281121 , 16038 };
        createEntitiesByYearAndCurrency(2015,6,"SIB", indicators2,values10_6_2015);

        double[] values10_7_2015 ={511918 , 178772 , 67728 , 111044 ,0 , 333147 , 324233 , 8914 };
        createEntitiesByYearAndCurrency(2015,7,"SIB", indicators2,values10_7_2015);

        double[] values10_8_2015 ={537759 , 187748 , 76664 , 111084 ,0 , 350011 , 326962 , 23049 };
        createEntitiesByYearAndCurrency(2015,8,"SIB", indicators2,values10_8_2015);

        double[] values10_9_2015 ={534584 , 194831 , 84913 , 109918 ,0 , 339754 , 327082 , 12672 };
        createEntitiesByYearAndCurrency(2015,9,"SIB", indicators2,values10_9_2015);

        double[] values10_10_2015 ={572047 , 214297 , 100316 , 113981 ,0 , 357750 , 344561 , 13189 };
        createEntitiesByYearAndCurrency(2015,10,"SIB", indicators2,values10_10_2015);

        double[] values10_11_2015 ={556306 , 199244 , 84806 , 114438 ,0 , 357062 , 344960 , 12102 };
        createEntitiesByYearAndCurrency(2015,11,"SIB", indicators2,values10_11_2015);

        double[] values10_12_2015 ={546241 , 203789 , 84218 , 119571 ,0 , 342452 , 331236 , 11216 };
        createEntitiesByYearAndCurrency(2015,12,"SIB", indicators2,values10_12_2015);


        double[] values10_1_2016 ={576654 , 196155 , 74763 , 121392 ,0 , 380499 , 366293 , 14206 };
        createEntitiesByYearAndCurrency(2016,1,"SIB", indicators2,values10_1_2016);

        double[] values10_2_2016 ={565515 , 200604 , 78703 , 121901 ,0 , 364910 , 347511 , 17400 };
        createEntitiesByYearAndCurrency(2016,2,"SIB", indicators2,values10_2_2016);

        double[] values10_3_2016 ={567817 , 199285 , 78094 , 121190 ,0 , 368532 , 347365 , 21167 };
        createEntitiesByYearAndCurrency(2016,3,"SIB", indicators2,values10_3_2016);

        double[] values10_4_2016 ={600363 , 208962 , 87242 , 121720 ,0 , 391400 , 364795 , 26606 };
        createEntitiesByYearAndCurrency(2016,4,"SIB", indicators2,values10_4_2016);

        double[] values10_5_2016 ={574963 , 205766 , 83122 , 122643 ,0 , 369197 , 348829 , 20369 };
        createEntitiesByYearAndCurrency(2016,5,"SIB", indicators2,values10_5_2016);

        double[] values10_6_2016 ={556723 , 206214 , 83827 , 122387 ,0 , 350509 , 338227 , 12282 };
        createEntitiesByYearAndCurrency(2016,6,"SIB", indicators2,values10_6_2016);

        double[] values10_7_2016 ={565393 , 204259 , 83298 , 120961 ,0 , 361134 , 345074 , 16060 };
        createEntitiesByYearAndCurrency(2016,7,"SIB", indicators2,values10_7_2016);

        double[] values10_8_2016 ={565531 , 201132 , 80699 , 120432 ,0 , 364399 , 346914 , 17485 };
        createEntitiesByYearAndCurrency(2016,8,"SIB", indicators2,values10_8_2016);

        double[] values10_9_2016 ={594003 , 195221 , 75203 , 120018 ,0 , 398782 , 374813 , 23969 };
        createEntitiesByYearAndCurrency(2016,9,"SIB", indicators2,values10_9_2016);

        double[] values10_10_2067 ={581725 , 202574 , 81207 , 121366 ,0 , 379151 , 363370 , 15781 };
        createEntitiesByYearAndCurrency(2016,10,"SIB", indicators2,values10_10_2067);

        double[] values10_11_2067 ={600486 , 204131 , 83095 , 121036 ,0 , 396354 , 379755 , 16599 };
        createEntitiesByYearAndCurrency(2016,11,"SIB", indicators2,values10_11_2067);

        double[] values10_12_2067 ={616477 , 215002 , 88369 , 126633 ,0 , 401475 , 383266 , 18209 };
        createEntitiesByYearAndCurrency(2016,12,"SIB", indicators2,values10_12_2067);


        double[] values10_1_2017 ={604289 , 222488 , 90689 , 131800 ,0 , 381800 , 366190 , 15611 };
        createEntitiesByYearAndCurrency(2017,1,"SIB", indicators2,values10_1_2017);

        double[] values10_2_2017 ={634745 , 232581 , 101479 , 131101 ,0 , 402165 , 377336 , 24828 };
        createEntitiesByYearAndCurrency(2017,2,"SIB", indicators2,values10_2_2017);

        double[] values10_3_2017 ={638487 , 232737 , 100906 , 131831 ,0 , 405750 , 388541 , 17209 };
        createEntitiesByYearAndCurrency(2017,3,"SIB", indicators2,values10_3_2017);

        double[] values10_4_2017 ={644022 , 241543 , 108455 , 133088 ,0 , 402479 , 380067 , 22412 };
        createEntitiesByYearAndCurrency(2017,4,"SIB", indicators2,values10_4_2017);

        double[] values10_5_2017 ={661044 , 245001 , 106350 , 138651 ,0 , 416043 , 399036 , 17007 };
        createEntitiesByYearAndCurrency(2017,5,"SIB", indicators2,values10_5_2017);

        double[] values10_6_2017 ={665000 , 249112 , 110699 , 138413 ,0 , 415888 , 394024 , 21863 };
        createEntitiesByYearAndCurrency(2017,6,"SIB", indicators2,values10_6_2017);

        double[] values10_7_2017 ={646803 , 239792 , 100641 , 139151 ,0 , 407011 , 383470 , 23541 };
        createEntitiesByYearAndCurrency(2017,7,"SIB", indicators2,values10_7_2017);

        double[] values10_8_2017 ={618192 , 238965 , 101400 , 137565 ,0 , 379227 , 363197 , 16030 };
        createEntitiesByYearAndCurrency(2017,8,"SIB", indicators2,values10_8_2017);

        double[] values10_9_2017 ={637805 , 245849 , 110770 , 135078 ,0 , 391956 , 369137 , 22819 };
        createEntitiesByYearAndCurrency(2017,9,"SIB", indicators2,values10_9_2017);

        double[] values10_10_2017 ={626473 , 244604 , 107090 , 137515 ,0 , 381869 , 365411 , 16458 };
        createEntitiesByYearAndCurrency(2017,10,"SIB", indicators2,values10_10_2017);

        double[] values10_11_2017 ={642398 , 252957 , 114148 , 138809 ,0 , 389440 , 370301 , 19139 };
        createEntitiesByYearAndCurrency(2017,11,"SIB", indicators2,values10_11_2017);

        double[] values10_12_2017 ={667450 , 254111 , 111215 , 142897 ,0 , 413338 , 391953 , 21385 };
        createEntitiesByYearAndCurrency(2017,12,"SIB", indicators2,values10_12_2017);



        double[] values11_1_2015 ={351811  , 111836  , 27925  , 83911  ,0  , 239975  , 208060  , 31915  };
        createEntitiesByYearAndCurrency(2015,1,"SCB", indicators2,values11_1_2015);

        double[] values11_2_2015 ={342043  , 116986  , 32853  , 84133  ,0  , 225057  , 193711  , 31346  };
        createEntitiesByYearAndCurrency(2015,2,"SCB", indicators2,values11_2_2015);

        double[] values11_3_2015 ={351817  , 125530  , 40356  , 85174  ,0  , 226287  , 194041  , 32246  };
        createEntitiesByYearAndCurrency(2015,3,"SCB", indicators2,values11_3_2015);

        double[] values11_4_2015 ={353830  , 126564  , 40264  , 86300  ,0  , 227266  , 193149  , 34117  };
        createEntitiesByYearAndCurrency(2015,4,"SCB", indicators2,values11_4_2015);

        double[] values11_5_2015 ={372905  , 127265  , 40147  , 87118  ,0  , 245640  , 213675  , 31965  };
        createEntitiesByYearAndCurrency(2015,5,"SCB", indicators2,values11_5_2015);

        double[] values11_6_2015 ={347419  , 126381  , 38382  , 87999  ,0  , 221038  , 185011  , 36027  };
        createEntitiesByYearAndCurrency(2015,6,"SCB", indicators2,values11_6_2015);

        double[] values11_7_2015 ={361289  , 129047  , 40425  , 88622  ,0  , 232242  , 201263  , 30979  };
        createEntitiesByYearAndCurrency(2015,7,"SCB", indicators2,values11_7_2015);

        double[] values11_8_2015 ={360563  , 129367  , 40741  , 88626  ,0  , 231196  , 197346  , 33850  };
        createEntitiesByYearAndCurrency(2015,8,"SCB", indicators2,values11_8_2015);

        double[] values11_9_2015 ={366947  , 130291  , 40336  , 89955  ,0  , 236656  , 205484  , 31172  };
        createEntitiesByYearAndCurrency(2015,9,"SCB", indicators2,values11_9_2015);

        double[] values11_10_2015 ={355486  , 131504  , 40352  , 91152  ,0  , 223982  , 195473  , 28509  };
        createEntitiesByYearAndCurrency(2015,10,"SCB", indicators2,values11_10_2015);

        double[] values11_11_2015 ={341314  , 132212  , 40743  , 91469  ,0  , 209102  , 183661  , 25441  };
        createEntitiesByYearAndCurrency(2015,11,"SCB", indicators2,values11_11_2015);

        double[] values11_12_2015 ={345918  , 130999  , 35911  , 95088  ,0  , 214919  , 189480  , 25439  };
        createEntitiesByYearAndCurrency(2015,12,"SCB", indicators2,values11_12_2015);


        double[] values11_1_2016 ={372568  , 133758  , 37769  , 95989  ,0  , 238810  , 213064  , 25746  };
        createEntitiesByYearAndCurrency(2016,1,"SCB", indicators2,values11_1_2016);

        double[] values11_2_2016 ={359499  , 138765  , 43126  , 95639  ,0  , 220734  , 196133  , 24601  };
        createEntitiesByYearAndCurrency(2016,2,"SCB", indicators2,values11_2_2016);

        double[] values11_3_2016 ={352573  , 139579  , 43193  , 96386  ,0  , 212994  , 189359  , 23635  };
        createEntitiesByYearAndCurrency(2016,3,"SCB", indicators2,values11_3_2016);

        double[] values11_4_2016 ={372047  , 140774  , 43288  , 97486  ,0  , 231273  , 206805  , 24468  };
        createEntitiesByYearAndCurrency(2016,4,"SCB", indicators2,values11_4_2016);

        double[] values11_5_2016 ={381753  , 142062  , 44158  , 97904  ,0  , 239691  , 215208  , 24483  };
        createEntitiesByYearAndCurrency(2016,5,"SCB", indicators2,values11_5_2016);

        double[] values11_6_2016 ={386435  , 142591  , 44274  , 98317  ,0  , 243844  , 220662  , 23182  };
        createEntitiesByYearAndCurrency(2016,6,"SCB", indicators2,values11_6_2016);

        double[] values11_7_2016 ={381890  , 143962  , 45260  , 98702  ,0  , 237928  , 212801  , 25127  };
        createEntitiesByYearAndCurrency(2016,7,"SCB", indicators2,values11_7_2016);

        double[] values11_8_2016 ={368133  , 142829  , 45173  , 97656  ,0  , 225304  , 202042  , 23262  };
        createEntitiesByYearAndCurrency(2016,8,"SCB", indicators2,values11_8_2016);

        double[] values11_9_2016 ={366236  , 144665  , 46260  , 98405  ,0  , 221571  , 191813  , 29758  };
        createEntitiesByYearAndCurrency(2016,9,"SCB", indicators2,values11_9_2016);

        double[] values11_10_2067 ={377362  , 146709  , 46807  , 99902  ,0  , 230653  , 203488  , 27165  };
        createEntitiesByYearAndCurrency(2016,10,"SCB", indicators2,values11_10_2067);

        double[] values11_11_2067 ={377042  , 146890  , 46641  , 100249  ,0  , 230152  , 204266  , 25886  };
        createEntitiesByYearAndCurrency(2016,11,"SCB", indicators2,values11_11_2067);

        double[] values11_12_2067 ={406575  , 152024  , 46454  , 105570  ,0  , 254551  , 228186  , 26365  };
        createEntitiesByYearAndCurrency(2016,12,"SCB", indicators2,values11_12_2067);


        double[] values11_1_2017 ={381675  , 144171  , 39048  , 105123  ,0  , 237504  , 210671  , 26833  };
        createEntitiesByYearAndCurrency(2017,1,"SCB", indicators2,values11_1_2017);

        double[] values11_2_2017 ={387898  , 146861  , 41987  , 104874,   0  , 241037  , 214959  , 26078  };
        createEntitiesByYearAndCurrency(2017,2,"SCB", indicators2,values11_2_2017);

        double[] values11_3_2017 ={391707  , 149107  , 43505  , 105602  ,0  , 242600  , 215903  , 26697  };
        createEntitiesByYearAndCurrency(2017,3,"SCB", indicators2,values11_3_2017);

        double[] values11_4_2017 ={392190  , 150488  , 44605  , 105883  ,0  , 241702  , 215859  , 25843  };
        createEntitiesByYearAndCurrency(2017,4,"SCB", indicators2,values11_4_2017);

        double[] values11_5_2017 ={381583  , 151061  , 45465  , 105596  ,0  , 230522  , 204606  , 25916  };
        createEntitiesByYearAndCurrency(2017,5,"SCB", indicators2,values11_5_2017);

        double[] values11_6_2017 ={396576  , 164596  , 59592  , 105, 0  , 231980  , 205694  , 26286  };
        createEntitiesByYearAndCurrency(2017,6,"SCB", indicators2,values11_6_2017);

        double[] values11_7_2017 ={407765  , 165066  , 60069  , 104997  ,0  , 242699  , 217441  , 25258  };
        createEntitiesByYearAndCurrency(2017,7,"SCB", indicators2,values11_7_2017);

        double[] values11_8_2017 ={410426  , 163365  , 59309  , 104056  ,0  , 247061  , 219201  , 27860  };
        createEntitiesByYearAndCurrency(2017,8,"SCB", indicators2,values11_8_2017);

        double[] values11_9_2017 ={412755  , 166529  , 62140  , 104389 ,  0  , 246226  , 220464  , 25762  };
        createEntitiesByYearAndCurrency(2017,9,"SCB", indicators2,values11_9_2017);

        double[] values11_10_2017 ={405654  , 166845  , 61750  , 105095  ,0  , 238809  , 214353  , 24456  };
        createEntitiesByYearAndCurrency(2017,10,"SCB", indicators2,values11_10_2017);

        double[] values11_11_2017 ={424306  , 167218  , 61723  , 105495  ,0  , 257088  , 232725  , 24363  };
        createEntitiesByYearAndCurrency(2017,11,"SCB", indicators2,values11_11_2017);

        double[] values11_12_2017 ={442331  , 175778  , 63307  , 112471  ,0  , 266553  , 242328  , 24225  };
        createEntitiesByYearAndCurrency(2017,12,"SCB", indicators2,values11_12_2017);






        String[] subsidiaries= {"BDM"  , "ECOBANK", "BOA"  , "BIM", "BNDA", "BMS"  , "BAM", "BICIM", "BHM"
                , "BSIC", "BCS (ex sahel)", "B.C.I.M"  , "ORABANK (exBRS)", "FGHM", "ALIOS" };


        double[] values= {386517,301899,320343,290709,229985,269326,156242,90247,71439,53242,54106,68402,36339,13 ,63285};
        createEntitiesByYearAndSubsidiaries(2015,12,subsidiaries,"dépôts",values);

        double[] valuess= {278815,202916,258606,139508,196283,219238,143645,68053,58947,77591,61673,58518,42628,113,37722};
        createEntitiesByYearAndSubsidiaries(2015,12,subsidiaries,"Crédits",valuess);

        double[] valuesss= {441122  , 303953  , 313939  , 288810  , 253508  , 375238  , 143066  , 93501  ,0  , 55930  , 61717  , 104255  , 44174  ,13  ,0  , };
        createEntitiesByYearAndSubsidiaries(2016,12,subsidiaries,"dépôts",valuesss);

        double[] valuessz= {332469, 176307, 257691, 214248, 262043, 334556, 133337, 77704,0, 98228, 77552, 103228, 44575,135,0, };
        createEntitiesByYearAndSubsidiaries(2016,12,subsidiaries,"Crédits",valuessz);



    }

    private void createEntity( BigDecimal value, int month, int year, String subsidiary, String indicator) {
/*        Optional<IndicatorValue> indicatorValue = indicatorValueRepository.findTopByMonthAndYearAndIndicator(month,year,indicatorRepository.findByName(indicator).get());
        if (indicatorValue.isPresent()) {
            indicatorValueRepository
              .saveAndFlush(getEntity(indicatorValue.get().getId(), value, month,year, subsidiary, indicator));
        }else
        */
            indicatorValueRepository.save(getEntity(-1L,value, month,year, subsidiary, indicator));
    }

    private IndicatorValue getEntity(Long id, BigDecimal value, int month, int year, String subsidiary, String indicator){
        IndicatorValue indicatorValue =  new IndicatorValue();
        indicatorValue.setId(id);
        indicatorValue.setValue(value);
        indicatorValue.setMonth(month);
        indicatorValue.setYear(year);

        if (indicatorRepository.findByName(indicator).isPresent())
            indicatorValue.setIndicator(indicatorRepository.findByName(indicator).get());

        if (subsidiaryRepository.findByBank(subsidiary).isPresent())
            indicatorValue.setSubsidiary(subsidiaryRepository.findByBank(subsidiary).get());
        return indicatorValue;
    }


}
