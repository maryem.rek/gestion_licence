package com.gestion.domaine.services;

import com.gestion.domaine.model.Zone;
import com.gestion.domaine.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ZoneService {

    @Autowired
    private ZoneRepository zoneRepository;

    /**
     * Create Zone : Name should be unique
     * @param zone
     * @return Zone | null
     */
    public Zone create(Zone zone){
        return (zoneRepository.findByName(zone.getName()).isPresent())?
                null:zoneRepository.save(zone);
    }

    /**
     * Update Zone : Name should be unique|present, Id should be present
     * @param zone
     * @return Zone if updated else null
     */
    public Zone update(Zone zone){

        if(zone.getId()!=null && zone.getName()!=null)
        {
            Optional<Zone> zoneValueById   = zoneRepository.findById(zone.getId());
            Optional<Zone> zoneValueByName = zoneRepository.findByName(zone.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(zoneValueByName.isPresent() && !zoneValueById.get().getId().equals(zoneValueByName.get().getId()));
            return (isNameUnique)?
                    zoneRepository.saveAndFlush(zone):null;
        }
        return null;
    }


    /**
     * Get Zone by id
     * @param id
     * @return Zone if updated else null
     */
    public Zone findById(Long id){
        Optional<Zone> zone1 = zoneRepository.findById(id);
        return zone1.orElse(null);
    }

    /**
     * Get Zone by name
     * @param name
     * @return Zone if updated else null
     */
    public Zone findByName(String name){
        Optional<Zone> zone1 = zoneRepository.findByName(name);
        return zone1.orElse(null);
    }

    /**
     * Get all zones
     * @return List<Zone>
     */
    public List<Zone> findAll(){
        return zoneRepository.findAll();
    }

    /**
     * Get Page of zones
     * @param page
     * @param size
     * @return Page<Zone>
     */
    public Page<Zone> findPages(int page, int size)
    {
        return zoneRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Zone by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            zoneRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !zoneRepository.findById(id).isPresent();
    }

    /**
     * Delete Zone
     * @param zone
     * @return isDeleted
     */
    public Boolean delete(Zone zone){
        try{
            if (zone.getId()!=null){
                zoneRepository.delete(zone);
                return !zoneRepository.findById(zone.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }
}
