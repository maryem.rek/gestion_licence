package com.gestion.domaine.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lamrani.auth.model.SuperUser;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity(name = "DomainUser")
@Table(name = "USER")
public class DomainUser extends SuperUser {


    @OneToMany(mappedBy = "owner",cascade= CascadeType.ALL, orphanRemoval=true)
    private List<QueryEntity> myQueryEntities;

    @ManyToMany( cascade = CascadeType.ALL)
    private List<QueryEntity> sharedWithMe;

    
    @JsonIgnore
    private List<QueryEntity> getMyQueryEntities() {
		return myQueryEntities;
	}


    @JsonIgnore
	private void setMyQueryEntities(List<QueryEntity> myQueryEntities) {
		this.myQueryEntities = myQueryEntities;
	}


    @JsonIgnore
	private List<QueryEntity> getSharedWithMe() {
		return sharedWithMe;
	}


    @JsonIgnore
	private void setSharedWithMe(List<QueryEntity> sharedWithMe) {
		this.sharedWithMe = sharedWithMe;
	}



	public DomainUser() {
    }

}
