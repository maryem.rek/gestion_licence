package com.gestion.domaine.seeders;

import com.gestion.domaine.model.Subsidiary;
import com.gestion.domaine.repositories.CountryRepository;
import com.gestion.domaine.repositories.CurrencyRepository;
import com.gestion.domaine.repositories.SubsidiaryRepository;
import com.gestion.domaine.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Component
public class SubsidiarySeeder {

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    SubsidiaryRepository subsidiaryRepository;

    @Autowired
    ZoneRepository zoneRepository;

    @Autowired
    CurrencyRepository currencyRepository;

    public void run(){
        createEntity("ABT","Tunisie","Some detailed company",25L,
                "PDMCredits ABT","PDMDepots ABT","shareholders ABT","Logo ABT" );

        createEntity("ABM","Tunisie","Some detailed company",25L,
                "PDMCredits ABM","PDMDepots ABM","shareholders ABM","Logo ABM" );

        createEntity("CDC","Congo brazzaville","Some detailed company",25L,
                "PDMCredits CDC","PDMDepots CDC","shareholders CDC","Logo CDC" );

        createEntity("CDS","Congo brazzaville","Some detailed company",25L,
                "PDMCredits CDS","PDMDepots CDS","shareholders CDS","Logo CDC" );

        createEntity("UGB","Gabon","Some detailed company",25L,
                "PDMCredits UGB","PDMDepots UGB","shareholders UGB","Logo UGB" );

        createEntity("CBAO","Sénégal","Some detailed company",25L,
                "PDMCredits CBAO","PDMDepots CBAO","shareholders CBAO","Logo CBAO" );

        createEntity("SIB","Sénégal","Some detailed company",25L,
                "PDMCredits SIB","PDMDepots SIB","shareholders SIB","Logo SIB" );

        createEntity("SCB","Sénégal","Some detailed company",25L,
                "PDMCredits SCB","PDMDepots SCB","shareholders SCB","Logo SCB" );


        createEntity("BDM","Mali","Some detailed company",25L,
                "PDMCredits BDM","PDMDepots BDM","shareholders BDM","Logo BDM" );

        createEntity("ECOBANK","Mali","Some detailed company",25L,
                "PDMCredits ECOBANK","PDMDepots ECOBANK","shareholders ECOBANK","Logo ECOBANK" );

        createEntity("BOA","Mali","Some detailed company",25L,
                "PDMCredits BOA","PDMDepots BOA","shareholders BOA","Logo BOA" );

        createEntity("BIM","Mali","Some detailed company",25L,
                "PDMCredits BIM","PDMDepots BIM","shareholders BIM","Logo BIM" );

        createEntity("BNDA","Mali","Some detailed company",25L,
                "PDMCredits BNDA","PDMDepots BNDA","shareholders BNDA","Logo BNDA" );

        createEntity("BMS","Mali","Some detailed company",25L,
                "PDMCredits BMS","PDMDepots BMS","shareholders BMS","Logo BMS" );

        createEntity("BAM","Mali","Some detailed company",25L,
                "PDMCredits BAM","PDMDepots BAM","shareholders BAM","Logo BAM" );

        createEntity("BICIM","Mali","Some detailed company",25L,
                "PDMCredits BICIM","PDMDepots BICIM","shareholders BICIM","Logo BICIM" );

        createEntity("BHM","Mali","Some detailed company",25L,
                "PDMCredits BHM","PDMDepots BHM","shareholders BHM","Logo BHM" );

        createEntity("BSIC","Mali","Some detailed company",25L,
                "PDMCredits BSIC","PDMDepots BSIC","shareholders BSIC","Logo BSIC" );

        createEntity("BCS (ex sahel)","Mali","Some detailed company",25L,
                "PDMCredits BCS (ex sahel)","PDMDepots BCS (ex sahel)","shareholders BCS (ex sahel)","Logo BCS (ex sahel)" );

        createEntity("B.C.I.M","Mali","Some detailed company",25L,
                "PDMCredits B.C.I.M","PDMDepots B.C.I.M","shareholders B.C.I.M","Logo B.C.I.M" );

        createEntity("ORABANK (exBRS)","Mali","Some detailed company",25L,
                "PDMCredits ORABANK (exBRS)","PDMDepots ORABANK (exBRS)","shareholders ORABANK (exBRS)","Logo ORABANK (exBRS)" );

        createEntity("FGHM","Mali","Some detailed company",25L,
                "PDMCredits FGHM","PDMDepots FGHM","shareholders FGHM","Logo FGHM" );

        createEntity("ALIOS","Mali","Some detailed company",25L,
                "PDMCredits ALIOS","PDMDepots ALIOS","shareholders ALIOS","Logo ALIOS" );

    }

    private void createEntity(String name, String country, String detailedCompanyName,
                              Long numberOfAgencies, String PDMCredits, String PDMDepots,
                              String shareholders, String logo) {
        Optional<Subsidiary> subsidiary1 = subsidiaryRepository.findByBank(name);
        if (subsidiary1.isPresent()) {
            subsidiaryRepository
              .saveAndFlush(getEntity(subsidiary1.get().getId(), name, country, detailedCompanyName, numberOfAgencies, PDMCredits, PDMDepots, shareholders, logo));
        }else
            subsidiaryRepository.save(getEntity(-1L,name,country, detailedCompanyName, numberOfAgencies, PDMCredits, PDMDepots, shareholders, logo));
    }

    private Subsidiary  getEntity(Long id, String name
            ,String country, String detailedCompanyName, Long numberOfAgencies, String PDMCredits, String PDMDepots, String shareholders, String logo){
        Subsidiary subsidiary =new Subsidiary();
        subsidiary.setBank(name);
        subsidiary.setCreatedAt(new Date());
        subsidiary.setDetailedCompanyName(detailedCompanyName);
        subsidiary.setCountry(countryRepository.findByName(country).get());
        subsidiary.setNumberOfAgencies(numberOfAgencies);
        subsidiary.setPDMCredits(PDMCredits);
        subsidiary.setPDMDepots(PDMDepots);
        subsidiary.setShareholders(shareholders);
        subsidiary.setLogo(logo);

        subsidiary.setId(id);
        return subsidiary;
    }


}
