package com.gestion.domaine.rest;

import com.gestion.domaine.model.Subsidiary;
import com.gestion.domaine.services.SubsidiaryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/subsidiary")
public class SubsidiaryRest {


    @Autowired
    private SubsidiaryService subsidiaryService;

    @PostMapping()
    @ApiOperation(value = "Create a new  subsidiary, return subsidiary if success else return null")
    public ResponseEntity<?> create(@RequestBody Subsidiary subsidiary){
        Subsidiary subsidiaryDB = subsidiaryService.create(subsidiary);
        return  new ResponseEntity<>(
                subsidiaryDB,subsidiaryDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  subsidiary, return subsidiary if success else return null")
    public ResponseEntity<?> update(@RequestBody Subsidiary subsidiary){
        Subsidiary subsidiaryDB = subsidiaryService.update(subsidiary);
        return  new ResponseEntity<>(
                subsidiaryDB,subsidiaryDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }


    @GetMapping()
    @ApiOperation(value = "Get List Subsidiary if page and size !=0 otherwise Get Subsidiary page ")
    public ResponseEntity<?> findAll( @RequestParam(value = "page",required = false) Integer page,
                                      @RequestParam(value = "size",required = false) Integer size)
    {
        return new ResponseEntity<>((page!=null&&size!=null)?
                subsidiaryService.findPages(page,size):subsidiaryService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete subsidiary by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(subsidiaryService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete subsidiary")
    public ResponseEntity<?> delete(@RequestBody Subsidiary subsidiary){
        return new ResponseEntity<>(subsidiaryService.delete(subsidiary), HttpStatus.OK);

    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get subsidiary by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Subsidiary subsidiary = subsidiaryService.findById(id);
        return new ResponseEntity<>(subsidiary,subsidiary!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }


    @RequestMapping(value="/avatar/{id}" ,method = RequestMethod.POST)
    @ApiOperation(value = "Change avatar, return user")
    public ResponseEntity<?> changeAvatar(@RequestParam("file") MultipartFile file,@RequestParam("id") Long id) {
        Subsidiary subsidiary = subsidiaryService.findById(id);
        return subsidiaryService.changeLogo(subsidiary, file);
    }

    @RequestMapping(value="/avatar" ,method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete avatar, return user")
    public ResponseEntity<?> deleteAvatar(@RequestParam("id") Long id) {
        Subsidiary subsidiary = subsidiaryService.findById(id);
        return subsidiaryService.deleteLogo(subsidiary);
    }


}
