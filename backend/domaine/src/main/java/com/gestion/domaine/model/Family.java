package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Family implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Topic topic;

    @OneToMany(mappedBy = "family",cascade =CascadeType.ALL)//,orphanRemoval = true)
     private List<Indicator> indicators;


    public Family() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public Topic getTopic() {
        return topic;
    }
    @JsonSetter
    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    @JsonIgnore
    public List<Indicator> getIndicators() {
        return indicators;
    }

    @JsonSetter
    public void setIndicators(List<Indicator> indicators) {
        this.indicators = indicators;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(id, family.id) &&
                Objects.equals(name, family.name) &&
                Objects.equals(topic, family.topic) &&
                Objects.equals(indicators, family.indicators);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, topic, indicators);
    }
}
