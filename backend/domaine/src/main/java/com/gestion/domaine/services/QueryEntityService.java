package com.gestion.domaine.services;

import com.gestion.domaine.model.*;
import com.gestion.domaine.model.Currency;
import com.lamrani.domaine.model.*;
import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.model.vo.DatesInterval;
import com.gestion.domaine.model.vo.Variance;
import com.gestion.domaine.model.vo.VarianceTCAM;
import com.gestion.domaine.repositories.QueryEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class QueryEntityService {

    @Autowired
    private QueryEntityRepository queryEntityRepository;

    @Autowired
    private IndicatorValueService indicatorValueService;

    @Autowired
    private CurrencyValueService currencyValueService;

    @Autowired
    private FormulaService formulaService;

    @Autowired
    private IndicatorService indicatorService;

    @Autowired
    private SubsidiaryService subsidiaryService;

    /**
     * Create QueryEntity : Name should be unique
     * @param queryEntity
     * @return QueryEntity | null
     */
    public QueryEntity create(QueryEntity queryEntity){
        return (queryEntityRepository.findByName(queryEntity.getName()).isPresent())?
                null:queryEntityRepository.save(queryEntity);
    }

    /**
     * Update QueryEntity : Name should be unique|present, Id should be present
     * @param queryEntity
     * @return QueryEntity if updated else null
     */
    public QueryEntity update(QueryEntity queryEntity){

        if(queryEntity.getId()!=null && queryEntity.getName()!=null)
        {
            Optional<QueryEntity> queryEntityValueById   = queryEntityRepository.findById(queryEntity.getId());
            Optional<QueryEntity> queryEntityValueByName = queryEntityRepository.findByName(queryEntity.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(queryEntityValueByName.isPresent() && !queryEntityValueById.get().getId().equals(queryEntityValueByName.get().getId()));
            return (isNameUnique)?
                    queryEntityRepository.saveAndFlush(queryEntity):null;
        }
        return null;
    }


    /**
     * Get QueryEntity by id
     * @param id
     * @return QueryEntity if updated else null
     */
    public QueryEntity findById(Long id){
        Optional<QueryEntity> queryEntity1 = queryEntityRepository.findById(id);
        return queryEntity1.orElse(null);
    }

    /**
     * Get QueryEntity by name
     * @param name
     * @return QueryEntity if updated else null
     */
    public QueryEntity findByName(String name){
        Optional<QueryEntity> queryEntity1 = queryEntityRepository.findByName(name);
        return queryEntity1.orElse(null);
    }

    /**
     * Get all queryEntitys
     * @return List<QueryEntity>
     */
    public List<QueryEntity> findAll(){
        return queryEntityRepository.findAll();
    }

    /**
     * Get Page of queryEntitys
     * @param page
     * @param size
     * @return Page<QueryEntity>
     */
    public Page<QueryEntity> findPages(int page, int size)
    {
        return queryEntityRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete QueryEntity by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            queryEntityRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !queryEntityRepository.findById(id).isPresent();
    }

    /**
     * Delete QueryEntity
     * @param queryEntity
     * @return isDeleted
     */
    public Boolean delete(QueryEntity queryEntity){
        try{
            if (queryEntity.getId()!=null){
                queryEntityRepository.delete(queryEntity);
                return !queryEntityRepository.findById(queryEntity.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }

    /**
     * Get QueryEntity by id
     * @param id
     * @return QueryEntity if updated else null
     */
    public List<Comment> getQueryEntityComments(Long id)
    {
        Optional<QueryEntity> queryEntity = queryEntityRepository.findById(id);
        return queryEntity.isPresent()?queryEntity.get().getComments():null;
    }

    /**
     * seprate a list of indicators two to lists first  fed with non calculable indicators and second with calculable indicators
     * @param indicators
     * @return
     */
    public List<List<Indicator>> seprateCalculableAndNonCalculableIndicators(List<Indicator> indicators){

        List<Indicator>  nonCalculableIndicators
                = indicators.stream().filter(indicator -> indicator.getCalculable()!=null && !indicator.getCalculable()).collect(Collectors.toList());

        List<Indicator>  calculableIndicators
                = indicators.stream().filter(indicator -> indicator.getCalculable()!=null && indicator.getCalculable()).collect(Collectors.toList());

        return Arrays.asList(calculableIndicators, nonCalculableIndicators);
    }

    /**
     *  update indicatorValues including the currencyValue  based on the queryEntity CurrencyRate, date ...
     * @param indicatorValues
     * @param month
     * @param year
     * @param currency
     * @param currencyRate
     * @return List<IndicatorValue>
     */
    private List<IndicatorValue> updateIndicatorValueInculdingConstantCurrencyValue(List<IndicatorValue> indicatorValues, int month, int year, Currency currency, CurrencyRate currencyRate, QueryEntity queryEntity ){

        return indicatorValues.stream().map(indicatorValue -> {
            Long id = indicatorValue.getSubsidiary().getId();
            Subsidiary sb = queryEntity.getSubsidiaries().stream().filter((value)->{
                return value.getId().equals(id);
            }).findFirst().get();
            if(sb!=null && sb.getCountry()!=null && sb.getCountry().getCurrency()!=null && indicatorValue.getIndicator()!=null && indicatorValue.getIndicator().getConvertible()){
                Currency subsiCurrencysb= sb.getCountry().getCurrency();

                BigDecimal ratios= new BigDecimal(1.0);
                BigDecimal currencyValueValue =CurrencyRate.AVERAGE_SPOT.equals(indicatorValue.getIndicator().getCurrencyRate())?  currencyValueService
                        .getAverageCurrencyValueValueByTopByMonthLessThanAndYearAndCurrency(month
                                ,year,  currency.getId()) : currencyValueService
                        .getCurrencyValueValueByTopByMonthAndYearAndCurrency(month
                                ,year, currency);

                BigDecimal currencyValueValueSub =CurrencyRate.AVERAGE_SPOT.equals(indicatorValue.getIndicator().getCurrencyRate())?  currencyValueService
                        .getAverageCurrencyValueValueByTopByMonthLessThanAndYearAndCurrency(month
                                ,year,  subsiCurrencysb.getId()) : currencyValueService
                        .getCurrencyValueValueByTopByMonthAndYearAndCurrency(month
                                ,year, subsiCurrencysb);
                if((!subsiCurrencysb.getId().equals(currency.getId()))){
                    ratios = currencyValueValue.divide(currencyValueValueSub, 10, RoundingMode.HALF_EVEN);
                }
                indicatorValue.setValue(indicatorValue.getValue().divide(ratios,10, RoundingMode.HALF_EVEN));
            }
            return indicatorValue;
        }).collect(Collectors.toList());
    }

    /**
     *  update indicatorValues including the currencyValue for each indicator Value based on the indicator CurrencyRate
     * @param indicatorValues
     * @param  currency
     * @return List<IndicatorValue>
     */
    private List<IndicatorValue> updateIndicatorValueInculdingTheCorrespondingCurrencyValueToIndicatorValue(List<IndicatorValue> indicatorValues, Currency currency, QueryEntity queryEntity){

        return indicatorValues.stream().map(indicatorValue -> {
            Long id = indicatorValue.getSubsidiary().getId();
            Subsidiary sb = queryEntity.getSubsidiaries().stream().filter((value)->{
                return value.getId().equals(id);
            }).findFirst().get();
            if(sb!=null && sb.getCountry()!=null && sb.getCountry().getCurrency()!=null && indicatorValue.getIndicator()!=null && indicatorValue.getIndicator().getConvertible()
                    && indicatorValue.getValue()!=null){
                Currency subsiCurrencysb= sb.getCountry().getCurrency();
                BigDecimal ratios= new BigDecimal(1.0);
                if( CurrencyRate.AVERAGE_PRICE.equals(indicatorValue.getIndicator().getCurrencyRate())){
                    BigDecimal currencyValueValue =  currencyValueService
                            .getCurrencyValueValueByTopByMonthAndYearAndCurrency(indicatorValue.getMonth()
                                    ,indicatorValue.getYear(), currency);
                    BigDecimal currencyValueValueSub =  currencyValueService
                            .getCurrencyValueValueByTopByMonthAndYearAndCurrency(indicatorValue.getMonth()
                                    ,indicatorValue.getYear(), subsiCurrencysb);
                    if((!subsiCurrencysb.getId().equals(currency.getId()))){
                        ratios = currencyValueValue.divide(currencyValueValueSub, 10, RoundingMode.HALF_EVEN);
                    }
                    indicatorValue.setValue(indicatorValue.getValue().divide(ratios,10, RoundingMode.HALF_EVEN));

                }else if( CurrencyRate.AVERAGE_SPOT.equals(indicatorValue.getIndicator().getCurrencyRate())) {
                    BigDecimal currencyValueValue =  currencyValueService
                            .getAverageCurrencyValueValueByTopByMonthLessThanAndYearAndCurrency(indicatorValue.getMonth()
                                    ,indicatorValue.getYear(),  currency.getId());
                    BigDecimal currencyValueValueSub =  currencyValueService
                            .getAverageCurrencyValueValueByTopByMonthLessThanAndYearAndCurrency(indicatorValue.getMonth()
                                    ,indicatorValue.getYear(),  subsiCurrencysb.getId());
                    if((!subsiCurrencysb.getId().equals(currency.getId()))){
                        ratios = currencyValueValue.divide(currencyValueValueSub, 10, RoundingMode.HALF_EVEN);
                    }
                    indicatorValue.setValue(indicatorValue.getValue().divide(ratios,10, RoundingMode.HALF_EVEN));
                }
            }
            return indicatorValue;
        }).collect(Collectors.toList());
    }

    /**
     * get indicatorValues of  non-calculable indicators  based on the queryEntity params
     * TODO calculate Ratios
     * @param queryEntity
     * @return
     */
    private List<IndicatorValue> executeQueryForNonCalculableIndicators(QueryEntity queryEntity,List<Indicator> indicators ) {
        List<IndicatorValue> nonCalculableIndicatorValues = indicatorValueService.findByIndicatorsAndSubsidiaryAndDates(indicators, queryEntity);
            if (queryEntity.getConstantCurrency() && (CurrencyRate.AVERAGE_SPOT.equals(queryEntity.getCurrencyRate()))){
                nonCalculableIndicatorValues = updateIndicatorValueInculdingConstantCurrencyValue(nonCalculableIndicatorValues,
                        queryEntity.getCurrencyMonth(),queryEntity.getCurrencyYear(),queryEntity.getCurrency(),CurrencyRate.AVERAGE_SPOT, queryEntity);
            }else if(queryEntity.getConstantCurrency()){
                nonCalculableIndicatorValues = updateIndicatorValueInculdingConstantCurrencyValue(nonCalculableIndicatorValues,
                        queryEntity.getCurrencyMonth(),queryEntity.getCurrencyYear(),  queryEntity.getCurrency(),CurrencyRate.AVERAGE_PRICE, queryEntity);
            }else if(!queryEntity.getConstantCurrency()){
                nonCalculableIndicatorValues = updateIndicatorValueInculdingTheCorrespondingCurrencyValueToIndicatorValue
                        (nonCalculableIndicatorValues,queryEntity.getCurrency(), queryEntity);
            }
        return nonCalculableIndicatorValues;
    }

    /**
     * get indicatorValues of  calculable indicators  based on the queryEntity params
     * TODO calculate Ratios
     * @param queryEntity
     * @return
     */
    private List<IndicatorValue> executeQueryForCalculableIndicators(QueryEntity queryEntity,List<Indicator> indicators) {

        // get List Dates if isDiscreet otherwise change the date interval from to into a list of dates
        List<Date> dates = queryEntity.isDiscreet()?queryEntity.getDates():formulaService.getDates(queryEntity.getFromDate(),queryEntity.getToDate());

        //collect IndicatorValues for each indicator
        List<IndicatorValue> calculableIndicatorValues =  new ArrayList<>();

        for (Indicator indicator:indicators){
            indicator =  indicatorService.findById(indicator.getId());
            List<IndicatorValue> indicatorValues = formulaService.calculateIndicatorValuesUsingFormula(indicator.getFormula(),dates,queryEntity.getSubsidiaries(),indicator, queryEntity);
            calculableIndicatorValues = Stream.concat(calculableIndicatorValues.stream(), indicatorValues.stream())
                    .collect(Collectors.toList());
        }

        if (queryEntity.getConstantCurrency() && (CurrencyRate.AVERAGE_SPOT.equals(queryEntity.getCurrencyRate()))){
            calculableIndicatorValues = updateIndicatorValueInculdingConstantCurrencyValue(calculableIndicatorValues,
                    queryEntity.getCurrencyMonth(),queryEntity.getCurrencyYear(),queryEntity.getCurrency(),CurrencyRate.AVERAGE_SPOT, queryEntity);
        }else if(queryEntity.getConstantCurrency() && (CurrencyRate.AVERAGE_PRICE.equals(queryEntity.getCurrencyRate()))){
            calculableIndicatorValues = updateIndicatorValueInculdingConstantCurrencyValue(calculableIndicatorValues,
                    queryEntity.getCurrencyMonth(),queryEntity.getCurrencyYear(),  queryEntity.getCurrency(),CurrencyRate.AVERAGE_PRICE, queryEntity);
        }else if(!queryEntity.getConstantCurrency()){
            calculableIndicatorValues = updateIndicatorValueInculdingTheCorrespondingCurrencyValueToIndicatorValue
                    (calculableIndicatorValues,queryEntity.getCurrency(), queryEntity);
        }
        return calculableIndicatorValues;
    }

    /**
     * get indicatorValues of  non-calculable and Calculable indicators  based on the queryEntity params
     * @param queryEntity
     * @return
     */
    public List<IndicatorValue> executeQuery(QueryEntity queryEntity) {
        List<IndicatorValue> nonCalculableIndicatorValues = new ArrayList<>();
        List<IndicatorValue> calculableIndicatorValues = new ArrayList<>();
        if (queryEntity != null && queryEntity.getIndicators()!=null){
            List<List<Indicator>> indicators = seprateCalculableAndNonCalculableIndicators(queryEntity.getIndicators());
            if(queryEntity.isReRun()){
                nonCalculableIndicatorValues = executeQueryForNonCalculableIndicators(queryEntity,queryEntity.getIndicators());
            }else{
                nonCalculableIndicatorValues = executeQueryForNonCalculableIndicators(queryEntity,indicators.get(1));
                calculableIndicatorValues  = executeQueryForCalculableIndicators(queryEntity,indicators.get(0));
            }
        }
        return Stream.concat(nonCalculableIndicatorValues.stream(), calculableIndicatorValues.stream())
                .collect(Collectors.toList());
    }

    /**
     * get indicatorValue for these params
     * @param indicator
     * @param year
     * @param month
     * @param subsidiary
     * @return
     */
    private IndicatorValue getOrCalculateIndicatorValue(Indicator indicator,int year, int month, Subsidiary subsidiary){
        if (indicator.getCalculable() && !indicator.getFormula().startsWith("_$")){
            indicator =  indicatorService.findById(indicator.getId());
            String finalFormula = formulaService.getFinalFormula(indicator.getFormula());
            List<Indicator>  indicators =  formulaService.getIndicatorsFromFormula(finalFormula);
            return  formulaService.calculateIndicatorValueUsingFormula(
                    finalFormula,indicators,month,year,subsidiary,indicator);
        }
        else
            return indicatorValueService.findTopByIndicatorAndSubsidiaryAndYearAndMonth(indicator, subsidiary,year,month);
    }


    private Map<Indicator,List<IndicatorValue>> initIndicatorIndicatorValueVariancTCAMeMap(Indicator indicator, Map<Indicator,List<IndicatorValue>> indicatorListMap){
        if(indicatorListMap==null) {
            indicatorListMap = new HashMap<>();
        }
        if ( !indicatorListMap.containsKey(indicator) || indicatorListMap.get(indicator)==null){
            indicatorListMap.put(indicator,new ArrayList<>());
        }
        return indicatorListMap;
    }

    private Map<Subsidiary,Map<Indicator,List<IndicatorValue>>> initSubsidiaryMapIndicatorMap(Subsidiary subsidiary,Map<Subsidiary,Map<Indicator,List<IndicatorValue>>> listMap ){
        if(listMap==null) {
            listMap = new HashMap<>();
        }
        if ( !listMap.containsKey(subsidiary) || listMap.get(subsidiary)==null){
            listMap.put(subsidiary,new HashMap<>());
        }
        return listMap;
    }

    /**
     * get map of (subsidiary , map of(indicator, List<IndicatorValue>) ) to be prepared to be calculated
     * @param indicatorValues
     * @return
     */
    public Map<Subsidiary, Map<Indicator, List<IndicatorValue>>> prepareData(List<IndicatorValue> indicatorValues) {
        final Map<Subsidiary, Map<Indicator, List<IndicatorValue>>>[] variances = new Map[]{new HashMap<>()};
        indicatorValues.forEach(indicatorValue ->{
            Subsidiary subsidiary = indicatorValue.getSubsidiary();
            Indicator indicator = indicatorValue.getIndicator();
            variances[0] = initSubsidiaryMapIndicatorMap(indicatorValue.getSubsidiary(), variances[0]);
            Map<Indicator,List<IndicatorValue>> mapIndicator = variances[0].get(indicatorValue.getSubsidiary());
            mapIndicator = initIndicatorIndicatorValueVariancTCAMeMap(indicatorValue.getIndicator(),mapIndicator);
            List<IndicatorValue> list = mapIndicator.get(indicatorValue.getIndicator());
            list.add(indicatorValue);
            mapIndicator.put(indicatorValue.getIndicator(),list);
            variances[0].put(indicatorValue.getSubsidiary(),mapIndicator);
        } );
        return variances[0];
    }

    /**
     * get map of (subsidiary , Indicator) ) to be prepared to be calculated
     * @param indicatorValues
     * @return
     */
    public Map<Subsidiary, List<Indicator>> prepareDataAsSubsidiaryIndicators(List<IndicatorValue> indicatorValues) {
        Map<Subsidiary,List<Indicator>> mapSubsidiaryIndocators = new HashMap<>();

         indicatorValues.forEach(indicatorValue -> {
             if(!mapSubsidiaryIndocators.containsKey(indicatorValue.getSubsidiary())){
                mapSubsidiaryIndocators.put(indicatorValue.getSubsidiary(),new ArrayList<>());
             }
             List<Indicator> indicators = mapSubsidiaryIndocators.get(indicatorValue.getSubsidiary());
             if ((indicators.indexOf(indicatorValue.getIndicator())==-1)) {
                 indicators.add(indicatorValue.getIndicator());
             }
             mapSubsidiaryIndocators.put(indicatorValue.getSubsidiary(),indicators);
         });
        return mapSubsidiaryIndocators;
    }

    /**
     * calculate variance and TCAM for each selected indicator in each subsidiary
     * @param mapMap
     * @param queryEntity
     * @return
     */
    public Map<Long, Map<Long,VarianceTCAM>> calculateVarianceAndTCAM(Map<Long, List<Indicator>> mapMap, QueryEntity queryEntity) {

        List<DatesInterval> datesIntervals =  getDatesIntervals(queryEntity);

        Map<Long, Map<Long,VarianceTCAM>> calculatedVarianceAndTCAMBySubsidiary = new HashMap<>();
        datesIntervals.stream().forEach(datesInterval -> {

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(datesInterval.getStarts());

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(datesInterval.getEnds());

            mapMap.forEach((id, indicatorListMap) -> {
                calculatedVarianceAndTCAMBySubsidiary.computeIfAbsent(id, k -> new HashMap<>());
                Subsidiary subsidiary = subsidiaryService.findById(id);
                indicatorListMap.forEach(indicator -> {
                    Map<Long,VarianceTCAM> varianceTCAMList = calculatedVarianceAndTCAMBySubsidiary.get(id);
                    varianceTCAMList.computeIfAbsent(indicator.getId(), k -> new VarianceTCAM());

                    IndicatorValue indicatorValueStart =  getOrCalculateIndicatorValue(indicator,startCal.get(Calendar.YEAR),startCal.get(Calendar.MONTH)+1,subsidiary);
                    IndicatorValue indicatorValueEnd = getOrCalculateIndicatorValue(indicator,endCal.get(Calendar.YEAR),endCal.get(Calendar.MONTH)+1,subsidiary);

                    VarianceTCAM varianceTCAM =  varianceTCAMList.get(indicator.getId());
                    VarianceTCAM varianceCalculated =  calculateVariance(indicator,indicatorValueStart,indicatorValueEnd,varianceTCAM);
                        if (varianceCalculated!=null)varianceTCAM = varianceCalculated;
                    varianceTCAM.setIndicator(indicator);
                    varianceTCAMList.put(indicator.getId(),varianceTCAM);
                    VarianceTCAM varianceTCAM1 =  calculateTCAM(datesIntervals,subsidiary,indicator);
                    varianceTCAM.setTCAM(varianceTCAM1.getTCAM());
                    varianceTCAM.setEndYear(varianceTCAM1.getEndYear());
                    varianceTCAM.setEndMonth(varianceTCAM1.getEndMonth());
                    varianceTCAM.setStartMonth(varianceTCAM1.getStartMonth());
                    varianceTCAM.setStartYear(varianceTCAM1.getStartYear());
                    calculatedVarianceAndTCAMBySubsidiary.put(id, varianceTCAMList);
                });
            } );
        });

         return calculatedVarianceAndTCAMBySubsidiary;
    }

    private VarianceTCAM calculateTCAM(List<DatesInterval> datesIntervals, Subsidiary subsidiary, Indicator indicator) {

        VarianceTCAM varianceTCAM =  new VarianceTCAM();
        if(datesIntervals!=null && datesIntervals.size()>0)
        {
            Calendar startCal = Calendar.getInstance();
            startCal.setTime(datesIntervals.get(0).getEnds());

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(datesIntervals.get(datesIntervals.size()-1).getEnds());

            IndicatorValue indicatorValueStart =  getOrCalculateIndicatorValue(indicator,startCal.get(Calendar.YEAR),startCal.get(Calendar.MONTH)+1,subsidiary);
            IndicatorValue indicatorValueEnd = getOrCalculateIndicatorValue(indicator,endCal.get(Calendar.YEAR),endCal.get(Calendar.MONTH)+1,subsidiary);
            BigDecimal value = null;
            if (indicatorValueEnd !=null && indicatorValueEnd.getValue()!=null && indicatorValueStart != null && indicatorValueStart.getValue()!=null) {
                double val =  Math.pow(indicatorValueEnd.getValue().subtract(indicatorValueStart.getValue()).doubleValue(),12/monthsBetweenDates(startCal.getTime(),endCal.getTime()) -1);
                value = BigDecimal.valueOf(val);
            }


            varianceTCAM.setTCAM(value);
            varianceTCAM.setStartMonth(startCal.get(Calendar.MONTH)+1);
            varianceTCAM.setEndMonth(endCal.get(Calendar.MONTH)+1);
            varianceTCAM.setStartYear(startCal.get(Calendar.YEAR));
            varianceTCAM.setEndYear(endCal.get(Calendar.YEAR));
        }
        return varianceTCAM;
    }

    /**
     * get queries the current user saved or been shared with him
     * @param id
     * @return
     */
    public List<QueryEntity> getQueryEntitiesIwroteOrShatedWithMe(Long id) {
        return queryEntityRepository.getQueryEntitiesIwroteOrShatedWithMe(id);
    }

    /**
     * prepare dates (interval or list) to be splitted into several interval less than a year
     * @param queryEntity
     * @return
     */
    public List<DatesInterval> getDatesIntervals(QueryEntity queryEntity) {
        List<DatesInterval> datesIntervals = new ArrayList<>();
        if (queryEntity.isDiscreet()) {
            Optional<Date> maxDateOptional =  queryEntity.getDates().stream().max(Date::compareTo);
            Optional<Date> minDateOptional =  queryEntity.getDates().stream().min(Date::compareTo);
            if(maxDateOptional.isPresent() && minDateOptional.isPresent()) {
                datesIntervals = getDatesIntervalsContent(minDateOptional.get(),maxDateOptional.get());
            }else if (maxDateOptional.isPresent()){
                datesIntervals = getDatesIntervalsContent(maxDateOptional.get(),maxDateOptional.get());
            }else if (minDateOptional.isPresent()){
                datesIntervals = getDatesIntervalsContent(minDateOptional.get(),minDateOptional.get());
            }
        }else{
            datesIntervals = getDatesIntervalsContent(queryEntity.getFromDate(),queryEntity.getToDate());
        }
        return datesIntervals;
    }

    /**
     * split date interval into pieces less or equals a year
     * @param min
     * @param max
     * @return
     */
    private List<DatesInterval> getDatesIntervalsContent(Date min, Date max) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(min);
        cal.add(Calendar.YEAR,-1);
        min = cal.getTime();
        List<DatesInterval> datesIntervals = new ArrayList<>();
        while(max.compareTo(min) > 0){
            cal.setTime(min);
            cal.add(Calendar.YEAR, 1);
            if (cal.getTime().compareTo(max)<=0){
                datesIntervals.add(new DatesInterval(min,cal.getTime()));
            }else if(cal.getTime().compareTo(max)!=0)
                datesIntervals.add(new DatesInterval(min,max));
            min = cal.getTime();
        }
        return datesIntervals;
    }

    public Map<Long, List<Indicator>> transformeMapKeysIntoIdsInsteadOfSubsidiaries(List<IndicatorValue> indicatorValues) {
        Map<Subsidiary,List<Indicator>> mapMap = prepareDataAsSubsidiaryIndicators(indicatorValues);

        Map<Long,List<Indicator>> mapWithIdKey = new HashMap<>();
        mapMap.forEach(((subsidiary, indicators) -> {
           mapWithIdKey.put(subsidiary.getId(), mapMap.get(subsidiary));
        }));
        return mapWithIdKey;
    }

    private VarianceTCAM calculateVariance(Indicator indicator, IndicatorValue valueStart, IndicatorValue valueEnd, VarianceTCAM varianceTCAM){

        String varianceFormula = indicator.getVariance();
        if(varianceFormula!=null && varianceFormula!="" && valueStart!=null && valueEnd!=null){
            varianceFormula = varianceFormula.replace("N_1", valueStart.getValue()+"")
                    .replace("N",valueEnd.getValue()+"");

            BigDecimal value = null;

            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            try {
                String strVal= String.valueOf(engine.eval(varianceFormula));
                Double doubleValue = Double.parseDouble(strVal);
                value = BigDecimal.valueOf(doubleValue);
            } catch (ScriptException |NumberFormatException e) {
                e.printStackTrace();
            }
            Variance variance =  new Variance();
            variance.setEndMonth(valueEnd.getMonth());
            variance.setEndYear(valueEnd.getYear());
            variance.setStartMonth(valueStart.getMonth());
            variance.setStartYear(valueStart.getYear());
            variance.setValue(value);
            variance.setIndicatorValueAtEnd(valueEnd);
            variance.setOldValue(valueStart.getValue());
            variance.setNewValue(valueEnd.getValue());
            List<Variance> variances =  varianceTCAM.getVariance();
            if(variances==null) variances = new ArrayList<>();
            variances.add(variance);
            varianceTCAM.setVariance(variances);

            return varianceTCAM;

        }
        return null;
    }



    public int monthsBetweenDates(Date startDate, Date endDate){

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);

        Calendar end = Calendar.getInstance();
        end.setTime(endDate);

        int monthsBetween = 0;
        int dateDiff = end.get(Calendar.DAY_OF_MONTH)-start.get(Calendar.DAY_OF_MONTH);

        if(dateDiff<0) {
            int borrrow = end.getActualMaximum(Calendar.DAY_OF_MONTH);
            dateDiff = (end.get(Calendar.DAY_OF_MONTH)+borrrow)-start.get(Calendar.DAY_OF_MONTH);
            monthsBetween--;

            if(dateDiff>0) {
                monthsBetween++;
            }
        }
        else {
            monthsBetween++;
        }
        monthsBetween += end.get(Calendar.MONTH)-start.get(Calendar.MONTH);
        monthsBetween  += (end.get(Calendar.YEAR)-start.get(Calendar.YEAR))*12;
        return monthsBetween;
    }
}
