package com.gestion.domaine.services;

import com.gestion.domaine.model.*;
import com.lamrani.domaine.model.*;
import com.gestion.domaine.model.map.CountValue;
import com.gestion.domaine.model.map.YearMonth;
import com.gestion.domaine.repositories.CountryRepository;
import com.gestion.domaine.repositories.FormulaRepository;
import com.gestion.domaine.repositories.SubsidiaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FormulaService {

    @Autowired
    private FormulaRepository formulaRepository;

    @Autowired
    private IndicatorService indicatorService;

    @Autowired
    private IndicatorValueService indicatorValueService;

    @Autowired
    private SubsidiaryService subsidiaryService;

    @Autowired
    private SubsidiaryRepository subsidiaryRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    QueryEntityService queryEntityService;

    /**
     * Create Formula : Name should be unique and exist,Expression is mandatory too
     *
     * @param formula
     * @return Formula | null
     */
    public Formula create(Formula formula) {
        boolean mandatoryFields = formula.getName() != null && formula.getExpression() != null
                && !formulaRepository.findByName(formula.getName()).isPresent()
                && validateExpression(formula.getExpression());
        return mandatoryFields ? formulaRepository.save(formula) : null;
    }


    /**
     * Update Formula : Name should be unique|present, Id should be present
     *
     * @param formula
     * @return Formula if updated else null
     */
    public Formula update(Formula formula) {
        boolean mandatoryValues = formula.getId() != null && formula.getName() != null && formula.getExpression() != null
                && validateExpression(formula.getExpression());
        Optional<Formula> formulaValueById = formulaRepository.findById(formula.getId());
        Optional<Formula> formulaValueByName = formulaRepository.findByName(formula.getName());

        // !(exist && id!=id) => unique
        boolean isNameUnique = !(formulaValueByName.isPresent()
                && !formulaValueById.get().getId().equals(formulaValueByName.get().getId()));
        return (mandatoryValues && isNameUnique) ?
                formulaRepository.saveAndFlush(formula) : null;
    }

    /**
     * Get Formula by id
     *
     * @param id
     * @return Formula if updated else null
     */
    public Formula findById(Long id) {
        Optional<Formula> formula1 = formulaRepository.findById(id);
        return formula1.orElse(null);
    }

    /**
     * Get Formula by name
     *
     * @param name
     * @return Formula if updated else null
     */
    public Formula findByName(String name) {
        Optional<Formula> formula1 = formulaRepository.findByName(name);
        return formula1.orElse(null);
    }

    /**
     * Get all formulas
     *
     * @return List<Formula>
     */
    public List<Formula> findAll() {
        return formulaRepository.findAll();
    }

    /**
     * Get Page of formulas
     *
     * @param page
     * @param size
     * @return Page<Formula>
     */
    public Page<Formula> findPages(int page, int size) {
        return formulaRepository.findAll(PageRequest.of(page, size));
    }

    /**
     * Delete Formula by id
     *
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id) {
        try {
            formulaRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            return true;
        }
        return !formulaRepository.findById(id).isPresent();
    }

    /**
     * Delete Formula
     *
     * @param formula
     * @return isDeleted
     */
    public Boolean delete(Formula formula) {
        try {
            if (formula.getId() != null) {
                formulaRepository.delete(formula);
                return !formulaRepository.findById(formula.getId()).isPresent();
            }

        } catch (EmptyResultDataAccessException e) {
            return true;
        }

        return false;
    }

    /**
     * Validate the formula expression
     *
     * @param expression
     * @return
     */
    private boolean validateExpression(String expression) {
        //TODO verify if the expression is valid then continue ...
         /*
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        String foo = "40+15-14";
        System.out.println(engine.eval(foo));
        */
        return true;
    }

    /**
     * calculate indicator value for a calculable indicator given these param  if a value is missing return 0
     *
     * @param formula
     * @param indicators
     * @param month
     * @param year
     * @param subsidiary
     * @param indicator
     * @return
     */
    public IndicatorValue calculateIndicatorValueUsingFormula(String formula, List<Indicator> indicators, int month, int year, Subsidiary subsidiary, Indicator indicator) {
        //replace names with values and update calculable to false if there is no value for a specific indicator in a specific month, year and subsidiary
        Boolean calculable = true;
        for (Indicator indicator1 : indicators) {
            List<IndicatorValue> indicatorValuetmp = indicatorValueService.findByIndicatorAndMonthAndYearAndSubsidiary(indicator1, month, year, subsidiary);
            if (indicatorValuetmp.size() != 0 && indicatorValuetmp.get(0).getValue() != null) {
                formula = formula.replace(indicator1.getName(), indicatorValuetmp.get(0).getValue() + "");
            } else
                calculable = false;
        }

        //calculate expression if it s calculable otherwise it equals 0
        BigDecimal value = null;
        if (calculable) {
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            try {
                String strVal = String.valueOf(engine.eval(formula));
                Double doubleValue = Double.parseDouble(strVal);
                value = BigDecimal.valueOf(doubleValue);
            } catch (ScriptException e) {
                e.printStackTrace();
            }
        }

        // prepare IndicatorValue to return it
        IndicatorValue calculatedIndicatorValue = new IndicatorValue();
        calculatedIndicatorValue.setMonth(month);
        calculatedIndicatorValue.setYear(year);
        calculatedIndicatorValue.setIndicator(indicator);
        calculatedIndicatorValue.setSubsidiary(subsidiary);
        calculatedIndicatorValue.setValue(value);

        return calculatedIndicatorValue;
    }

    /**
     * get List of Indicator values for a calculable indicator given these params
     *
     * @param formula
     * @param dates
     * @param subsidiaries
     * @param indicator
     * @return
     */
    public List<IndicatorValue> calculateIndicatorValuesUsingFormula(String formula, List<Date> dates, List<Subsidiary> subsidiaries, Indicator indicator, QueryEntity queryEntity) {
        if (formula.startsWith("_$") && !queryEntity.isReRun()) {
            List<IndicatorValue> calculatedIndicatorValues = new ArrayList<>();
            List<Indicator> indiceStartList = new LinkedList<>();
            if (indicator != null) {
                indiceStartList.add(indicator);
                for (Subsidiary sb : subsidiaries) {
                    if (sb.getCountry() != null) {
                        List<Subsidiary> newSubsidiaries = this.subsidiaryRepository.findByCountryId(sb.getCountry().getId());
                        QueryEntity editedQueryEntity = new QueryEntity(queryEntity);
                        editedQueryEntity.setIndicators(indiceStartList);
                        editedQueryEntity.setSubsidiaries(newSubsidiaries);
                        editedQueryEntity.setReRun(true);
                        List<IndicatorValue> resultAllSubsiCalcule = queryEntityService.executeQuery(editedQueryEntity);
                        Map<YearMonth, CountValue> mp = new HashMap();
                        for (IndicatorValue indCopy : resultAllSubsiCalcule) {
                            IndicatorValue ind = new IndicatorValue(indCopy);
                            YearMonth yearMonth = new YearMonth();
                            yearMonth.setMonth(ind.getMonth());
                            yearMonth.setYear(ind.getYear());
                            yearMonth.setSubsidiaryId(sb.getId());
                            if (!mp.containsKey(yearMonth)) {
                                CountValue countValue = new CountValue();
                                countValue.setIndicatorValue(ind);
                                mp.put(yearMonth, countValue);
                            }
                            CountValue countValue = mp.get(yearMonth);
                            countValue.setSum(countValue.getSum().add(ind.getValue()));
                            if (ind.getSubsidiary().getId() == sb.getId()) {
                                countValue.setValue(ind.getValue());
                            }
                        }
                        for (YearMonth yearMonth : mp.keySet()) {
                            CountValue countValue = mp.get(yearMonth);
                            BigDecimal result = new BigDecimal(0);
                            if (!countValue.getSum().equals(result)) {
                                result = countValue.getValue().divide(countValue.getSum(), BigDecimal.ROUND_CEILING).multiply(new BigDecimal(100));
                            }
                            IndicatorValue indicatorValue = countValue.getIndicatorValue();
                            indicatorValue.setValue(result);
                            indicatorValue.setSubsidiary(sb);
                            indicatorValue.setIndicator(indicator );
                            calculatedIndicatorValues.add(indicatorValue);
                        }
                    }
                }
            }
            return calculatedIndicatorValues;
        }
        String finalFormula = getFinalFormula(formula);
        List<Indicator> indicators = getIndicatorsFromFormula(finalFormula);
        List<IndicatorValue> calculatedIndicatorValues = new ArrayList<>();

        for (Date date : dates) {
            subsidiaries.stream().map(sub -> sub.getId()).forEach(id -> {
                Calendar c = new GregorianCalendar();
                c.setTime(date);
                int curMonth = c.get(Calendar.MONTH);
                int curYear = c.get(Calendar.YEAR);

                Subsidiary subsidiary = subsidiaryService.findById(id);
                calculatedIndicatorValues.add(calculateIndicatorValueUsingFormula(
                        finalFormula, indicators, curMonth + 1, curYear, subsidiary, indicator));
            });
        }

        return calculatedIndicatorValues;
    }


    /**
     * return the final expression to be used for calculation.
     *
     * @param formula
     * @return
     */
    public String getFinalFormula(String formula) {
        String[] indicatorsNames = formula.split("(\\+|-|\\(\\s|\\{|\\}|\\[|\\]|\\s\\)|/|%|\\*)");

        List<String> indicatorsName = Arrays.stream(indicatorsNames).collect(Collectors.toList());

        for (String indicatorName : indicatorsNames) {
            if (!"".equals(indicatorName)) {
                Indicator indicator = indicatorService.findByName(indicatorName.trim());
                if (indicator != null) {
                    if (indicator.getCalculable()) {
                        formula = formula.replace(indicator.getName(), "(" + getFinalFormula(indicator.getFormula()) + ")");
                    }
                }
            }
        }
        return formula;
    }


    /**
     * get indicators from formula
     *
     * @param formula
     * @return
     */
    public List<Indicator> getIndicatorsFromFormula(String formula) {
        String[] indicators = formula.split("(\\+|-|\\(\\s|\\{|\\}|\\[|\\]|\\s\\)|/|%|\\*)");
        List<Indicator> indicators1 = new ArrayList<>();
        for (String indicator : indicators) {
            Indicator indicator1 = indicatorService.findByName(indicator.trim());
            if (indicator1 != null) {
                indicators1.add(indicator1);
            }
        }
        return indicators1;
    }

    /**
     * get List of dates from interval of dates
     *
     * @param from
     * @param to
     * @return
     */
    public List<Date> getDates(Date from, Date to) {
        List<Date> dates = new ArrayList<>();
        if (from.compareTo(to) > 0)
            return dates;
        else if (from.compareTo(to) == 0)
            dates.add(from);
        else {
            do {
                dates.add(from);
                Calendar c = new GregorianCalendar();
                c.setTime(from);
                c.add(Calendar.MONTH, 1);
                from = c.getTime();
            }
            while (from.compareTo(to) <= 0);
        }
        return dates;
    }
}
