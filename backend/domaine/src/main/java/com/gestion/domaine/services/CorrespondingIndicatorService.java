package com.gestion.domaine.services;

import com.gestion.domaine.model.CorrespondingIndicator;
import com.gestion.domaine.repositories.CorrespondingIndicatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CorrespondingIndicatorService {

    @Autowired
    private CorrespondingIndicatorRepository correspondingRepository;

    /**
     * Create CorrespondingIndicator : Name should be unique
     * @param corresponding
     * @return CorrespondingIndicator | null
     */
    public CorrespondingIndicator create(CorrespondingIndicator corresponding){
        return (correspondingRepository.findByName(corresponding.getName()).isPresent())?
                null:correspondingRepository.save(corresponding);
    }

    /**
     * Update CorrespondingIndicator : Name should be unique|present, Id should be present
     * @param corresponding
     * @return CorrespondingIndicator if updated else null
     */
    public CorrespondingIndicator update(CorrespondingIndicator corresponding){

        if(corresponding.getId()!=null && corresponding.getName()!=null)
        {
            Optional<CorrespondingIndicator> correspondingValueById   = correspondingRepository.findById(corresponding.getId());
            Optional<CorrespondingIndicator> correspondingValueByName = correspondingRepository.findByName(corresponding.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(correspondingValueByName.isPresent() && !correspondingValueById.get().getId().equals(correspondingValueByName.get().getId()));
            return (isNameUnique)?
                    correspondingRepository.saveAndFlush(corresponding):null;
        }
        return null;
    }


    /**
     * Get CorrespondingIndicator by id
     * @param id
     * @return CorrespondingIndicator if updated else null
     */
    public CorrespondingIndicator findById(Long id){
        Optional<CorrespondingIndicator> corresponding1 = correspondingRepository.findById(id);
        return corresponding1.orElse(null);
    }

    /**
     * Get CorrespondingIndicator by name
     * @param name
     * @return CorrespondingIndicator if updated else null
     */
    public CorrespondingIndicator findByName(String name){
        Optional<CorrespondingIndicator> corresponding1 = correspondingRepository.findByName(name);
        return corresponding1.orElse(null);
    }

    /**
     * Get all correspondings
     * @return List<CorrespondingIndicator>
     */
    public List<CorrespondingIndicator> findAll(){
        return correspondingRepository.findAll();
    }

    /**
     * Get Page of correspondings
     * @param page
     * @param size
     * @return Page<CorrespondingIndicator>
     */
    public Page<CorrespondingIndicator> findPages(int page, int size)
    {
        return correspondingRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete CorrespondingIndicator by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            correspondingRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !correspondingRepository.findById(id).isPresent();
    }

    /**
     * Delete CorrespondingIndicator
     * @param corresponding
     * @return isDeleted
     */
    public Boolean delete(CorrespondingIndicator corresponding){
        try{
            if (corresponding.getId()!=null){
                correspondingRepository.delete(corresponding);
                return !correspondingRepository.findById(corresponding.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }
}
