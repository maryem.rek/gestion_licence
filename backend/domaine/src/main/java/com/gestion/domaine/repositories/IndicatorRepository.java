package com.gestion.domaine.repositories;


import com.gestion.domaine.model.Indicator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IndicatorRepository extends JpaRepository<Indicator,Long> {

    Optional<Indicator> findByName(String name);

    @Query(nativeQuery = true, value = "select * from indicator where " +
            ":name LIKE CONCAT('%',name) or :name LIKE CONCAT(name, '%')")
    Optional<Indicator> findByNameLike(@Param("name") String name);

    List<Indicator> findByParentId(Long parentId);


}
