package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.Topic;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TopicVO {

    private Long id;
    private String  name;
    private int familiesCount;

    public TopicVO() {
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFamiliesCount() {
        return familiesCount;
    }

    public void setFamiliesCount(int familiesCount) {
        this.familiesCount = familiesCount;
    }

    public List<TopicVO> topicVOS(List<Topic> topics) {
        List<TopicVO> list = new ArrayList<>();
        for ( int i =0; i<topics.size();i++){
            list.add(new TopicVO(topics.get(i)));
        }
        return list;
    }

    public TopicVO(Topic topic){
        if ((topic!=null)) {
            this.id = topic.getId();
            this.name = topic.getName();
            this.familiesCount = (topic.getFamilies()!=null)? topic.getFamilies().size():0;
        }
    }
}
