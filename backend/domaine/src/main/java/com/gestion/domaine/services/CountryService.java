package com.gestion.domaine.services;

import com.gestion.domaine.model.Country;
import com.gestion.domaine.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepository;

    /**
     * Create Country : Name should be unique
     * @param country
     * @return Country | null
     */
    public Country create(Country country){
        return (countryRepository.findByName(country.getName()).isPresent())?
                null:countryRepository.save(country);
    }

    /**
     * Update Country : Name should be unique|present, Id should be present
     * @param country
     * @return Country if updated else null
     */
    public Country update(Country country){

        if(country.getId()!=null && country.getName()!=null)
        {
            Optional<Country> countryValueById   = countryRepository.findById(country.getId());
            Optional<Country> countryValueByName = countryRepository.findByName(country.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(countryValueByName.isPresent() && !countryValueById.get().getId().equals(countryValueByName.get().getId()));
            return (isNameUnique)?
                    countryRepository.saveAndFlush(country):null;
        }
        return null;
    }

    /**
     * Get Country by id
     * @param id
     * @return Country if updated else null
     */
    public Country findById(Long id){
        Optional<Country> country1 = countryRepository.findById(id);
        return country1.orElse(null);
    }

    /**
     * Get Country by name
     * @param name
     * @return Country if updated else null
     */
    public Country findByName(String name){
        Optional<Country> country1 = countryRepository.findByName(name);
        return country1.orElse(null);
    }

    /**
     * Get all countrys
     * @return List<Country>
     */
    public List<Country> findAll(){
        return countryRepository.findAll();
    }

    /**
     * Get Page of countrys
     * @param page
     * @param size
     * @return Page<Country>
     */
    public Page<Country> findPages(int page, int size)
    {
        return countryRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Country by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            countryRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !countryRepository.findById(id).isPresent();
    }

    /**
     * Delete Country
     * @param country
     * @return isDeleted
     */
    public Boolean delete(Country country){
        try{
            if (country.getId()!=null){
                countryRepository.delete(country);
                return !countryRepository.findById(country.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }
}
