package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Zone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "zone",cascade=CascadeType.ALL) //, orphanRemoval=true)
    private Set<Country> countries;

    public Zone() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Country> getCountries() {
        return countries;
    }

    @JsonIgnore
    public void setCountries(Set<Country> countries) {
        this.countries = countries;
    }

    @JsonSetter
    public void addChild(Country country)
    {
        this.countries.add(country);
    }


    public void removeChild(Country country)
    {
        this.countries.remove(country);
    }


}
