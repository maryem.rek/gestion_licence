package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IndicatorValue implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private BigDecimal value;

    @Range(min = 1970)
    private int year;

    @Range(min = 0,max = 12, message = "the month value is between 1 and 12")
    private int month;

    @ManyToOne(fetch = FetchType.LAZY)
    private Subsidiary subsidiary;

    @ManyToOne(fetch = FetchType.LAZY)
    private Indicator indicator;

    private Date dateValue;

    public IndicatorValue() { }

    public IndicatorValue(IndicatorValue ind) {
        id = ind.id;
        value = ind.value;
        year = ind.year;
        month = ind.month;
        subsidiary = ind.subsidiary;
        indicator = ind.indicator;
        dateValue = ind.dateValue;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Subsidiary getSubsidiary() {
        return subsidiary;
    }

    public void setSubsidiary(Subsidiary subsidiary) {
        this.subsidiary = subsidiary;
    }

    public Indicator getIndicator() {
        return indicator;
    }

    public void setIndicator(Indicator indicator) {
        this.indicator = indicator;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date date) {
        this.dateValue = date;
    }

}
