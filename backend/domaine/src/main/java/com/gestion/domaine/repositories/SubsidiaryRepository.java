package com.gestion.domaine.repositories;


import com.gestion.domaine.model.Subsidiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface SubsidiaryRepository extends JpaRepository<Subsidiary,Long> {
    Optional<Subsidiary> findByBank(String bank);

    @Query(value = "Select sa from Subsidiary sa where sa.id=:id")
    Optional<Subsidiary> findOneById(@Param("id") Long id);

    List<Subsidiary> findByCountryId(Long id);
}
