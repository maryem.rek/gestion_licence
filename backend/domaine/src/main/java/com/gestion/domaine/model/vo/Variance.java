package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.IndicatorValue;

import java.math.BigDecimal;

public class Variance {

    private BigDecimal value;
    private int endYear;
    private int endMonth;
    private int startYear;
    private int startMonth;
    private IndicatorValue indicatorValueAtEnd;
    private BigDecimal oldValue;
    private BigDecimal newValue;

    public Variance() {
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public IndicatorValue getIndicatorValueAtEnd() {
        return indicatorValueAtEnd;
    }

    public void setIndicatorValueAtEnd(IndicatorValue indicatorValueAtEnd) {
        this.indicatorValueAtEnd = indicatorValueAtEnd;
    }

	public BigDecimal getOldValue() {
		return oldValue;
	}

	public void setOldValue(BigDecimal oldValue) {
		this.oldValue = oldValue;
	}

	public BigDecimal getNewValue() {
		return newValue;
	}

	public void setNewValue(BigDecimal newValue) {
		this.newValue = newValue;
	}
    
    

}
