package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Formula  implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String expression;

    private boolean indicatorCalculator;

    @OneToMany
    private List<QueryEntity> queryEntities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Formula() { }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public boolean isIndicatorCalculator() {
        return indicatorCalculator;
    }

    public void setIndicatorCalculator(boolean indicatorCalculator) {
        this.indicatorCalculator = indicatorCalculator;
    }

    @JsonIgnore
    public List<QueryEntity> getQueryEntities() {
        return queryEntities;
    }

    @JsonSetter
    public void setQueryEntities(List<QueryEntity> queryEntities) {
        this.queryEntities = queryEntities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
