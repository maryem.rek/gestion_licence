package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.gestion.domaine.model.listners.CurrencyListner;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(CurrencyListner.class)
public class Currency {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String symbole;

    @OneToMany(mappedBy = "currency",cascade = CascadeType.DETACH) //, orphanRemoval=true)
    private List<Country> countries;

    @OneToMany(mappedBy = "currency",cascade=CascadeType.ALL)
    private List<CurrencyValue> currencyValues;

    @OneToMany(mappedBy = "currency",cascade=CascadeType.ALL) //, orphanRemoval=true)
    private List<QueryEntity> queryEntities;

    public Currency() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbole() {
        return symbole;
    }

    public void setSymbole(String symbole) {
        this.symbole = symbole;
    }

    @JsonIgnore
    public List<Country> getCountries() {
        return countries;
    }

    @JsonSetter
    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @JsonIgnore
    public List<CurrencyValue> getCurrencyValues() {
        return currencyValues;
    }

    @JsonSetter
    public void setCurrencyValues(List<CurrencyValue> currencyValues) {
        this.currencyValues = currencyValues;
    }

    @JsonIgnore
    public List<QueryEntity> getQueryEntities() {
        return queryEntities;
    }

    @JsonSetter
    public void setQueryEntities(List<QueryEntity> queryEntities) {
        this.queryEntities = queryEntities;
    }

    /*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return Objects.equals(id, currency.id) &&
                Objects.equals(name, currency.name) &&
                Objects.equals(queryEntities, currency.queryEntities) &&
                Objects.equals(symbole, currency.symbole) &&
                Objects.equals(countries, currency.countries) &&
                Objects.equals(currencyValues, currency.currencyValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, symbole, queryEntities, countries, currencyValues);
    }
*/

}
