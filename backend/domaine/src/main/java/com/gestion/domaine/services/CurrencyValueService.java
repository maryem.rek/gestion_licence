package com.gestion.domaine.services;

import com.gestion.domaine.model.Currency;
import com.gestion.domaine.model.CurrencyValue;
import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.repositories.CurrencyValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CurrencyValueService {

    @Autowired
    private CurrencyValueRepository currencyValueRepository;


    /**
     * Create CurrencyValue
     * @param currencyValue
     * @return CurrencyValue | null
     */
    public CurrencyValue create(CurrencyValue currencyValue){
        currencyValue.setDateValue(new GregorianCalendar(currencyValue.getYear(),currencyValue.getMonth()-1,1).getTime());
        return currencyValueRepository.save(currencyValue);
    }

    /**
     * Update CurrencyValue : Id should be present
     * @param currencyValue
     * @return CurrencyValue if updated else null
     */
    public CurrencyValue update(CurrencyValue currencyValue){
        currencyValue.setDateValue(new GregorianCalendar(currencyValue.getYear(),currencyValue.getMonth()-1,1).getTime());
        return (currencyValue.getId()!=null && currencyValueRepository.findById(currencyValue.getId()).isPresent())?
            currencyValueRepository.saveAndFlush(currencyValue):null;
    }

    /**
     * Get CurrencyValue by id
     * @param id
     * @return CurrencyValue if updated else null
     */
    public CurrencyValue findById(Long id){
        Optional<CurrencyValue> currencyValue1 = currencyValueRepository.findById(id);
        return currencyValue1.orElse(null);
    }

    /**
     * Get CurrencyValue by year, month, currency and currencyRate
     * @param month
     * @param year
     * @param currencyRate
     * @param currency
     * @return CurrencyValue if updated else null
     */
    public CurrencyValue findByYearAndCurrencyRateAndCurrency(int month, int year, CurrencyRate currencyRate, Currency currency){
        CurrencyValue currencyValue1
                =(CurrencyRate.AVERAGE_SPOT.equals(currencyRate))?
                      currencyValueRepository
                        .findTopByYearAndCurrencyRateAndCurrency(year,currencyRate,currency)
                    : currencyValueRepository
                        .findTopByYearAndMonthAndCurrencyRateAndCurrency(year,month,currencyRate,currency);
        return currencyValue1;
    }

    /**
     * Get all currencyValues
     * @return List<CurrencyValue>
     */
    public List<CurrencyValue> findAll(){
        return currencyValueRepository.findAll();
    }

    /**
     * Get Page of currencyValues
     * @param page
     * @param size
     * @return Page<CurrencyValue>
     */
    public Page<CurrencyValue> findPages(int page, int size)
    {
        return currencyValueRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete CurrencyValue by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            currencyValueRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !currencyValueRepository.findById(id).isPresent();
    }

    /**
     * Delete CurrencyValue
     * @param currencyValue
     * @return isDeleted
     */
    public Boolean delete(CurrencyValue currencyValue){
        try{
            if (currencyValue.getId()!=null){
                currencyValueRepository.delete(currencyValue);
                return !currencyValueRepository.findById(currencyValue.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }


    /**
     *  get currencyValue value  by these params : used when currencyRate equals spot...
     * @param month
     * @param year
     * @param currency
     * @return
     */
    public BigDecimal getCurrencyValueValueByTopByMonthAndYearAndCurrency(int month, int year, Currency currency){
        Optional<CurrencyValue> currencyValue = currencyValueRepository
                .findTopByMonthAndYearAndCurrency(month,year,currency);
        if (currencyValue.isPresent()) return currencyValue.get().getValue();
        return BigDecimal.valueOf(1L);
    }

    /**
     *  get average currencyValue value by theses year and currencyId and month less than ...: used when currencyRate equals average
     * @param month
     * @param year
     * @param currencyId
     * @return
     */
    public BigDecimal getAverageCurrencyValueValueByTopByMonthLessThanAndYearAndCurrency(int month, int year, Long currencyId){
        BigDecimal value  = currencyValueRepository
                .findAverageCurrencyValueValueByYearAndMonthLessThanAndCurrency(year,month,currencyId);
        if (value!=null) return value;
        return BigDecimal.valueOf(1L);
    }


    public List<Date> findFirstDateAndLastDateEver() {
        List<Date> dates =  new ArrayList<>();
        dates.add(currencyValueRepository.findFirstDateEver());
        dates.add(currencyValueRepository.findLastDateEver());
        return dates;
    }
}
