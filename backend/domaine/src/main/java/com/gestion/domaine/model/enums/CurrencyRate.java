package com.gestion.domaine.model.enums;

public enum CurrencyRate {
    AVERAGE_PRICE, AVERAGE_SPOT
}
