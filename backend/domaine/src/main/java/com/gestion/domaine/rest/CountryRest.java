package com.gestion.domaine.rest;

import com.gestion.domaine.model.Country;
import com.gestion.domaine.model.vo.CountryVO;
import com.gestion.domaine.services.CountryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/country")
public class CountryRest {

    @Autowired
    private CountryService countryService;

    @Autowired
    private CountryVO countryVo;

    @PostMapping()
    @ApiOperation(value = "Create a new  country, return country if success else return null")
    public ResponseEntity<?> create(@RequestBody Country country){
        Country countryDB = countryService.create(country);
        return  new ResponseEntity<>(
                new CountryVO(countryDB),countryDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  country, return country if success else return null")
    public ResponseEntity<?> update(@RequestBody Country country){
        Country countryDB = countryService.update(country);
        return  new ResponseEntity<>(
                new CountryVO(countryDB),countryDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }


    @GetMapping()
    @ApiOperation(value = "Get List Country")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(countryVo.countryVOS(countryService.findAll()), HttpStatus.OK);
    }



    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete country by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(countryService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete country")
    public ResponseEntity<?> delete(@RequestBody Country country){
        return new ResponseEntity<>(countryService.delete(country), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get country by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Country country = countryService.findById(id);
        return new ResponseEntity<>( new CountryVO(country),country!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }


}
