package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.Family;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FamilyVO {

    private Long id;

    private String name;

    private TopicVO topicVO;

    private List<IndicatorVO> indicators;

    public FamilyVO() {
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public TopicVO getTopic() {
        return topicVO;
    }

    public void setTopic(TopicVO topicVO) {
        this.topicVO = topicVO;
    }

    public List<FamilyVO> familyVOS(List<Family> familys) {
        List<FamilyVO> list = new ArrayList<>();
        for ( int i =0; i<familys.size();i++){
            list.add(new FamilyVO(familys.get(i)));
        }
        return list;
    }

    public FamilyVO(Family family){
        if (family!=null){
            this.id = family.getId();
            this.name = family.getName();
            this.topicVO =  new TopicVO(family.getTopic());
        }

    }
}
