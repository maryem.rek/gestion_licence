package com.gestion.domaine.services;

import com.gestion.domaine.model.Family;
import com.gestion.domaine.repositories.FamilyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FamilyService {

    @Autowired
    private FamilyRepository familyRepository;

    /**
     * Create Family : Name should be unique
     * @param family
     * @return Family | null
     */
    public Family create(Family family){
        return (familyRepository.findByName(family.getName()).isPresent())?
                null:familyRepository.save(family);
    }

    /**
     * Update Family : Name should be unique|present, Id should be present
     * @param family
     * @return Family if updated else null
     */
    public Family update(Family family){

        if(family.getId()!=null && family.getName()!=null)
        {
            Optional<Family> familyValueById   = familyRepository.findById(family.getId());
            Optional<Family> familyValueByName = familyRepository.findByName(family.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(familyValueByName.isPresent() && !familyValueById.get().getId().equals(familyValueByName.get().getId()));
            return (isNameUnique)?
                    familyRepository.saveAndFlush(family):null;
        }
        return null;
    }


    /**
     * Get Family by id
     * @param id
     * @return Family if updated else null
     */
    public Family findById(Long id){
        Optional<Family> family1 = familyRepository.findById(id);
        return family1.orElse(null);
    }

    /**
     * Get Family by name
     * @param name
     * @return Family if updated else null
     */
    public Family findByName(String name){
        Optional<Family> family1 = familyRepository.findByName(name);
        return family1.orElse(null);
    }

    /**
     * Get all families
     * @return List<Family>
     */
    public List<Family> findAll(){
        return familyRepository.findAll();
    }

    /**
     * Get Page of families
     * @param page
     * @param size
     * @return Page<Family>
     */
    public Page<Family> findPages(int page, int size)
    {
        return familyRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Family by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            familyRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !familyRepository.findById(id).isPresent();
    }

    /**
     * Delete Family
     * @param family
     * @return isDeleted
     */
    public Boolean delete(Family family){
        try{
            if (family.getId()!=null){
                familyRepository.delete(family);
                return !familyRepository.findById(family.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }
}
