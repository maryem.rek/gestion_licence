package com.gestion.domaine.model.listners;


import com.gestion.domaine.model.Currency;
import com.gestion.domaine.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PreRemove;


@Component
public class CurrencyListner {

    @Autowired
    private CountryRepository countryRepository;

    @PreRemove
    public void currencyPreRemove(Currency currency){
        currency.getCountries().forEach(country ->{
            country.setCurrency(null);
            //countryRepository.saveAndFlush(country);
            //TODO
            // it doesnt work: u need to set child to null when parent is deleted
            // for countries , queries,
        });
    }
}
