package com.gestion.domaine.repositories;


import com.gestion.domaine.model.Formula;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FormulaRepository extends JpaRepository<Formula,Long> {

    Optional<Formula> findByName(String name);

}
