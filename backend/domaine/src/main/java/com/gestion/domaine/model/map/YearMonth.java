package com.gestion.domaine.model.map;


import java.util.Objects;

public class YearMonth {
    private int year;

    private int month;

    private long subsidiaryId;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public long getSubsidiaryId() {
        return subsidiaryId;
    }

    public void setSubsidiaryId(long subsidiaryId) {
        this.subsidiaryId = subsidiaryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YearMonth yearMonth = (YearMonth) o;
        return year == yearMonth.year &&
                month == yearMonth.month &&
                subsidiaryId == yearMonth.subsidiaryId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, subsidiaryId);
    }
}
