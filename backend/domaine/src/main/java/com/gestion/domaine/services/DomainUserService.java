package com.gestion.domaine.services;

import com.gestion.domaine.model.DomainUser;
import com.gestion.domaine.repositories.DomainUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DomainUserService {

    @Autowired
    private DomainUserRepository domainUserRepository;

    public List<DomainUser> findAll() {
            return domainUserRepository.findAll();
    }
}
