package com.gestion.domaine.services;

import com.gestion.domaine.model.Comment;
import com.gestion.domaine.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    /**
     * Create Comment
     * @param comment
     * @return Comment | null
     */
    public Comment create(Comment comment){
        return commentRepository.save(comment);
    }

    /**
     * Update Comment : Id should be present
     * @param comment
     * @return Comment if updated else null
     */
    public Comment update(Comment comment){
        return (comment.getId()!=null && commentRepository.findById(comment.getId()).isPresent())?
            commentRepository.saveAndFlush(comment):null;
    }

    /**
     * Get Comment by id
     * @param id
     * @return Comment if updated else null
     */
    public Comment findById(Long id){
        Optional<Comment> comment1 = commentRepository.findById(id);
        return comment1.orElse(null);
    }

    /**
     * Get Comment by name
     * @param name
     * @return Comment if updated else null
     */
    public Comment findByName(String name){
        Optional<Comment> comment1 = commentRepository.findByName(name);
        return comment1.orElse(null);
    }

    /**
     * Get all comments
     * @return List<Comment>
     */
    public List<Comment> findAll(){
        return commentRepository.findAll();
    }

    /**
     * Get Page of comments
     * @param page
     * @param size
     * @return Page<Comment>
     */
    public Page<Comment> findPages(int page, int size)
    {
        return commentRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Comment by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            commentRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !commentRepository.findById(id).isPresent();
    }

    /**
     * Delete Comment
     * @param comment
     * @return isDeleted
     */
    public Boolean delete(Comment comment){
        try{
            if (comment.getId()!=null){
                commentRepository.delete(comment);
                return !commentRepository.findById(comment.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }

    /**
     * Get Comment by id
     * @param id
     * @return Comment if updated else null
     */
    public List<Comment> findByQueryEntityId(Long id){
        return commentRepository.findByQueryEntityId(id);
    }
}
