package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.Country;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CountryVO {

    private long id;
    private String name;
    private ZoneVO zoneVO;
    private CurrencyVO currencyVO;

    public ZoneVO getZone() {
        return zoneVO;
    }

    public void setZone(ZoneVO zoneVO) {
        this.zoneVO = zoneVO;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CurrencyVO getCurrency() {
        return currencyVO;
    }

    public void setCurrency(CurrencyVO currencyVO) {
        this.currencyVO = currencyVO;
    }

    public CountryVO() {
    }

    public CountryVO(Country country) {
        if (country !=null){
            this.id = country.getId();
            this.zoneVO = new ZoneVO(country.getZone());
            this.name = country.getName();
            this.currencyVO = new CurrencyVO(country.getCurrency());
        }
    }

    public List<CountryVO> countryVOS(List<Country> countries) {
        List<CountryVO> list = new ArrayList<>();
        for ( int i =0; i<countries.size();i++){
            list.add(new CountryVO(countries.get(i)));
        }
        return list;
    }

}
