package com.gestion.domaine.seeders;

import com.gestion.domaine.model.Indicator;
import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.repositories.FamilyRepository;
import com.gestion.domaine.repositories.IndicatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class IndicatorSeeder {

    @Autowired
    IndicatorRepository indicatorRepository;

    @Autowired
    FamilyRepository familyRepository;

    public void run(){
        createEntity( "Dépôts clientèle (Spot)", false, true,
                CurrencyRate.AVERAGE_SPOT, true,"Encours en ligne",
                null, "N-(N-1)/(N-1)" ,"");

        createEntity( "Dépôts rémunérés (Spot)", false, true,
                CurrencyRate.AVERAGE_SPOT, true,"Encours en ligne",
                null, "N-(N-1)/(N-1)" ,"Dépôts clientèle (Spot)");

        createEntity( "DAT (Spot)", false, true,
                CurrencyRate.AVERAGE_SPOT, true,"Encours en ligne",
                null, "N-(N-1)/(N-1)" ,"Dépôts rémunérés (Spot)");

        createEntity( "Comptes d'épargne (Spot)", false, true,
                CurrencyRate.AVERAGE_SPOT, true,"Encours en ligne",
                null, "N-(N-1)/(N-1)" ,"Dépôts rémunérés (Spot)");

        createEntity( "Certificats de dépôt (Spot)", false, true,
                CurrencyRate.AVERAGE_SPOT, true,"Encours en ligne",
                null, "N-(N-1)/(N-1)" ,"Dépôts rémunérés (Spot)");

        createEntity( "Dépôts non rémunérés (Spot)", false, true,
                CurrencyRate.AVERAGE_SPOT, true,"Encours en ligne",
                null, "N-(N-1)/(N-1)" ,"Dépôts clientèle (Spot)");

        createEntity( "Dont DAV (Spot)", false, true,
                CurrencyRate.AVERAGE_SPOT, true,"Encours en ligne",
                null, "N-(N-1)/(N-1)" ,"Dépôts non rémunérés (Spot)");

        createEntity( "poids dépôts non rémunérés(Spot)", true, false,
                null, true,"Encours en ligne",
                "Dépôts non rémunérés (Spot)/Dépôts clientèle (Spot)"
                , "(N-'N-1')x 100" ,"Dépôts non rémunérés (Spot)");

        createEntity( "dépôts", false, true,
                CurrencyRate.AVERAGE_SPOT, false,"Parts de marché",
                null, "N-(N-1)/(N-1)" ,"");

        createEntity( "Crédits", false, true,
                CurrencyRate.AVERAGE_SPOT, false,"Parts de marché",
                null, "N-(N-1)/(N-1)" ,"");


        createEntity( "RAO : Résultat net / total bilan", true, false,
                null, false,"Ratios",
                "Résultat net/Total bilan", "(N-'N-1')*10000" ,"");
        createEntity( "PNB/ Total bilan", true, false,
                null, false,"Ratios",
                "PNB/ Total bilan", "(N-'N-1')*10000" ,"RAO : Résultat net / total bilan");
        createEntity( "MNI/ Total bilan", true, false,
                null, false,"Ratios",
                "MNI/ Total bilan", "(N-'N-1')*10000" ,"PNB/ Total bilan");
        createEntity( "marge sur commissions / Total Bilan", true, false,
                null, false,"Ratios",
                "marge sur commissions / Total Bilan", "(N-'N-1')*10000" ,"PNB/ Total bilan");
        createEntity( "Résultats marché / Total bilan", true, false,
                null, false,"Ratios",
                "Résultats marché / Total bilan", "(N-'N-1')*10000" ,"PNB/ Total bilan");
        createEntity( "Autres PNB/ Total bilan", true, false,
                null, false,"Ratios",
                "Autres PNB/ Total bilan", "(N-'N-1')*10000" ,"PNB/ Total bilan");
        createEntity( "Autres pdts bancaires / Total bilan", true, false,
                null, false,"Ratios",
                "Autres pdts bancaires / Total bilan", "(N-'N-1')*10000" ,"RAO : Résultat net / total bilan");
        createEntity( "Charges / Total bilan", true, false,
                null, false,"Ratios",
                "Charges / Total bilan", "(N-'N-1')*10000" ,"RAO : Résultat net / total bilan");
        createEntity( "Autres pdts non bancaires / Total bilan", true, false,
                null, false,"Ratios",
                "Autres pdts non bancaires / Total bilan", "(N-'N-1')*10000" ,"RAO : Résultat net / total bilan");
        createEntity( "Coût du risque / Total bilan", true, false,
                null, false,"Ratios",
                "Coût du risque / Total bilan", "(N-'N-1')*10000" ,"RAO : Résultat net / total bilan");
        createEntity( "Résultat non courant/Total bilan", true, false,
                null, false,"Ratios",
                "Résultat non courant/Total bilan", "(N-'N-1')*10000" ,"RAO : Résultat net / total bilan");
        createEntity( "IS / Total bilan", true, false,
                null, false,"Ratios",
                "IS / Total bilan", "(N-'N-1')*10000" ,"RAO : Résultat net / total bilan");
    }

    private void createEntity( String name, boolean calculable, boolean convertible, CurrencyRate currencyRate, boolean displayOnAdhoc, String family, String formula, String variance,String parent) {
        Optional<Indicator> indicator1 = indicatorRepository.findByName(name);
        if (indicator1.isPresent()) {
            indicatorRepository
              .saveAndFlush(getEntity(indicator1.get().getId(), name, calculable, convertible, currencyRate, displayOnAdhoc, family, formula, variance,parent));
        }else
            indicatorRepository.save(getEntity(-1L, name, calculable, convertible, currencyRate, displayOnAdhoc, family, formula, variance,parent));
    }

    private Indicator  getEntity(Long id, String name
            , boolean calculable, boolean convertible, CurrencyRate currencyRate
            ,boolean displayOnAdhoc, String family, String formula, String variance,String parent){
        Indicator subsidiary =new Indicator();
        subsidiary.setName(name);
        subsidiary.setCalculable(calculable);
        subsidiary.setConvertible(convertible);
        subsidiary.setCurrencyRate(currencyRate);
        subsidiary.setDisplayedOnAdHocFile(displayOnAdhoc);
        if (familyRepository.findByName(family).isPresent())
            subsidiary.setFamily(familyRepository.findByName(family).get());
        if (indicatorRepository.findByName(parent).isPresent())
            subsidiary.setParent(indicatorRepository.findByName(parent).get());
        subsidiary.setFormula(formula);
        subsidiary.setVariance(variance);
        subsidiary.setId(id);
        return subsidiary;
    }


}
