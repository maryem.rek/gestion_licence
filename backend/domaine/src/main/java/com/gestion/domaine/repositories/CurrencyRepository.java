package com.gestion.domaine.repositories;


import com.gestion.domaine.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CurrencyRepository extends JpaRepository<Currency,Long> {
    Optional<Currency> findByName(String name);

}
