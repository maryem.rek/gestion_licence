package com.gestion.domaine.services;

import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.model.Subsidiary;
import com.gestion.domaine.repositories.SubsidiaryRepository;
import com.lamrani.auth.utils.FilesUtil;
import com.lamrani.auth.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SubsidiaryService {

    @Autowired
    private SubsidiaryRepository subsidiaryRepository;

    /**
     * Create Subsidiary : Name should be unique
     * @param subsidiary
     * @return Subsidiary | null
     */
    public Subsidiary create(Subsidiary subsidiary){
        return (subsidiaryRepository.findByBank(subsidiary.getBank()).isPresent())?
                null:subsidiaryRepository.save(subsidiary);
    }

    public Subsidiary saveOrUpdate(Subsidiary subsidiary){
    	Optional<Subsidiary> sb= subsidiaryRepository.findByBank(subsidiary.getBank());
    	if(sb.isPresent()) {
    		subsidiary.setId(sb.get().getId());
    		subsidiary= this.update(subsidiary);
    	}else {
    		subsidiary= subsidiaryRepository.save(subsidiary);
    	}
        return subsidiary;
    }
    
    /**
     * Update Subsidiary : Name should be unique|present, Id should be present
     * @param subsidiary
     * @return Subsidiary if updated else null
     */
    public Subsidiary update(Subsidiary subsidiary){

        if(subsidiary.getId()!=null && subsidiary.getBank()!=null)
        {
            Optional<Subsidiary> subsidiaryValueById   = subsidiaryRepository.findById(subsidiary.getId());
            Optional<Subsidiary> subsidiaryValueByBank = subsidiaryRepository.findByBank(subsidiary.getBank());

            // !(exist && id!=id) => unique
            boolean isBankUnique =
                    !(subsidiaryValueByBank.isPresent() && !subsidiaryValueById.get().getId().equals(subsidiaryValueByBank.get().getId()));
            return (isBankUnique)?
                    subsidiaryRepository.saveAndFlush(subsidiary):null;
        }
        return null;
    }

    /**
     * Get Subsidiary by id
     * @param id
     * @return Subsidiary if updated else null
     */
    public Subsidiary findById(Long id){
            Optional<Subsidiary> subsidiary1 = subsidiaryRepository.findById(id);
            return subsidiary1.orElse(null);
    }

    /**
     * Get Subsidiary by bank name
     * @param name
     * @return Subsidiary if updated else null
     */
    public Subsidiary findByBank(String name){
        Optional<Subsidiary> subsidiary1 = subsidiaryRepository.findByBank(name);
        return subsidiary1.orElse(null);
    }

    /**
     * Get all subsidiarys
     * @return List<Subsidiary>
     */
    public List<Subsidiary> findAll(){
        return subsidiaryRepository.findAll();
    }

    /**
     * Get Page of subsidiarys
     * @param page
     * @param size
     * @return Page<Subsidiary>
     */
    public Page<Subsidiary> findPages(int page, int size)
    {
        return subsidiaryRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Subsidiary by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            deleteLogo(subsidiaryRepository.findById(id).get());
            subsidiaryRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !subsidiaryRepository.findById(id).isPresent();
    }

    /**
     * Delete Subsidiary
     * @param subsidiary
     * @return isDeleted
     */
    public Boolean delete(Subsidiary subsidiary){
        try{
            if (subsidiary.getId()!=null){
                deleteLogo(subsidiary);
                subsidiaryRepository.delete(subsidiary);
                return !subsidiaryRepository.findById(subsidiary.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }

    /**
     * get IndicatorValues by SubsidiaryId
     * @param id
     * @return
     */
    public List<IndicatorValue> getIndicatorValues(Long id){
       Optional<Subsidiary> subsidiary =  subsidiaryRepository.findById(id);
        return subsidiary.isPresent()? subsidiary.get().getIndicatorValues() : new ArrayList<>();
    }

    /**
     * update subsidiary logo
     * @param subsidiary
     * @param file
     * @return
     */
    public ResponseEntity<?> changeLogo(Subsidiary subsidiary, MultipartFile file) {
            String name= subsidiary.getLogo();
            FilesUtil.deleteFile(name);
            subsidiary.setLogo(null);
            try {
                String newLogo= subsidiary.getId()+"_"+(new Date()).getTime()+'.'+FilesUtil.getFileExtension(file);
                FilesUtil.moveFile(file, newLogo);
                subsidiary.setLogo(newLogo);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ResponseUtil.ok(subsidiaryRepository.save(subsidiary));
    }

    /**
     * delete subsidiary logo
     * @param subsidiary
     * @return
     */
    public ResponseEntity<?> deleteLogo(Subsidiary subsidiary) {
        String name= subsidiary.getLogo();
        FilesUtil.deleteFile(name);
        subsidiary.setLogo(null);
        return ResponseUtil.ok(subsidiaryRepository.save(subsidiary));
    }
}
