package com.gestion.domaine.model.vo;

import java.util.Date;

public class DatesInterval {

    private Date starts;

    private Date ends;

    public DatesInterval(Date starts, Date ends) {
        this.starts = starts;
        this.ends = ends;
    }

    public DatesInterval() {
    }

    public Date getStarts() {
        return starts;
    }

    public void setStarts(Date starts) {
        this.starts = starts;
    }

    public Date getEnds() {
        return ends;
    }

    public void setEnds(Date ends) {
        this.ends = ends;
    }
}
