package com.gestion.domaine.seeders;

import com.gestion.domaine.model.CorrespondingIndicator;
import com.gestion.domaine.repositories.CorrespondingIndicatorRepository;
import com.gestion.domaine.repositories.IndicatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CorrespondingIndicatorSeeder {

    @Autowired
    CorrespondingIndicatorRepository correspondingIndicatorRepository;

    @Autowired
    IndicatorRepository indicatorRepository;

    public void run(){
        createEntity( "produits net bancaire","PNB");
        createEntity( "produits clientele","PC");
        createEntity( "charge clientele","CC" );
        createEntity( "DAT","DAT" );
        createEntity( "DUT DAV SPOT","DDS");
        createEntity( "poids de depots non remunere","PDNR");
        createEntity( "depots","depots");
        createEntity( "credits","credits");
        createEntity( "PDMCredits","PDMC");
        createEntity( "PDMDepots","PDMD");
        createEntity( "produits net bancaire bis","PNBB");
        createEntity( "marge d'interet","MDI");

    }

    private void createEntity(String indicator,String name ) {
        Optional<CorrespondingIndicator> correspondingIndicator1 = correspondingIndicatorRepository.findByName(name);
        if (correspondingIndicator1.isPresent()) {
            correspondingIndicatorRepository.saveAndFlush(getEntity(name,indicator,correspondingIndicator1.get().getId()));
        }else
            correspondingIndicatorRepository.save(getEntity(name,indicator,-1L));
    }

    private CorrespondingIndicator  getEntity(String name,String indicator,Long id){
         CorrespondingIndicator correspondingIndicator =new CorrespondingIndicator();
         correspondingIndicator.setName(name);
         correspondingIndicator.setId(id);
         if(indicatorRepository.findByName(indicator).isPresent())correspondingIndicator.setIndicator(indicatorRepository.findByName(indicator).get());
         return correspondingIndicator;
     }
}

