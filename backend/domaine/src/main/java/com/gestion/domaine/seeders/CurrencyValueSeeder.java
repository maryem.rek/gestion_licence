package com.gestion.domaine.seeders;

import com.gestion.domaine.model.CurrencyValue;
import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.repositories.CurrencyRepository;
import com.gestion.domaine.repositories.CurrencyValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

@Component
public class CurrencyValueSeeder {

    @Autowired
    CurrencyRepository currencyRepository;

    @Autowired
    CurrencyValueRepository currencyValueRepository;

        public void createEntitiesByYearAndCurrency(int year, String currency,double[] values){
            for (int i =0;i<values.length-1;i++){
                createEntity(BigDecimal.valueOf( values[i]),i+1,year,currency,CurrencyRate.AVERAGE_PRICE);
            }
            createEntity(BigDecimal.valueOf( values[12]),0,year,currency,CurrencyRate.AVERAGE_SPOT);
        }

    public void run(){


        double[] values2016 = {10.7615000,10.7749900,10.9750000, 10.9879900, 10.8900000, 10.8684900
                , 10.8904900, 10.8785000, 10.9155000, 10.8034700, 10.6739900, 10.6540000, 10.8394933};
        createEntitiesByYearAndCurrency(2016,"Eur",
                    values2016);

        double[] values2015 = {10.8155004,10.7965003,10.6989997, 10.8954999, 10.8084996, 10.9114999
                , 10.8535000, 10.9054996, 10.8895005, 10.8514999, 10.6575000, 10.7829900, 10.8222492};
        createEntitiesByYearAndCurrency(2015,"Eur",
                values2015);

        double[] values2017 = {10.7440000,10.6770000,10.6979900, 10.7880000, 10.9100000, 10.9784900
                , 11.1044900, 11.1455000, 11.1225000, 11.0700000, 11.1609900, 11.1884900, 10.9656208};
        createEntitiesByYearAndCurrency(2017,"Eur",
                values2017);




        double[] values2_2017 = {9.9561500,10.0510000,10.0208500, 9.8935500, 9.7211000, 9.6243000
                , 9.4459000, 9.3886000, 9.4209000, 9.4948500, 9.3663500, 9.3275500, 9.6425917};
        createEntitiesByYearAndCurrency(2017,"USD",
                    values2_2017);

        double[] values2_2016 = {9.9311500,9.9118000,9.6295500, 9.6108000, 9.7493000, 9.7793500
                , 9.7483000, 9.7653500, 9.7137000, 9.8716000, 10.0550000, 10.0825000, 9.8207000};
        createEntitiesByYearAndCurrency(2016,"USD",
                values2_2016);

        double[] values2_2015 = {9.5671500,9.6334500,9.9655000, 9.7413000, 9.8645500, 9.7190000
                , 9.8009000, 9.7274500, 9.7499000, 9.8036000, 10.0780000, 9.9007500, 9.7959625};
        createEntitiesByYearAndCurrency(2015,"USD",
                values2016);





        double[] values__2015 = {4.9547500,5.0088000,5.1075999,5.0936500,5.0450000,5.0233501
             ,5.0184000,5.0113000,5.0009499,4.9335000,4.9218400,4.9311400,5.0041900   };
        createEntitiesByYearAndCurrency(2015,"TND",
                values__2015);

        double[] values__2016 = {4.8938500,4.8933500,4.8498000,4.8508500,4.7392000,4.4833500
                ,4.4101000,4.4268500,4.4310900,4.4154400,4.4035900,4.3769000,4.5978642};
        createEntitiesByYearAndCurrency(2016,"TND",
                values__2016);

        double[] values__2017 = {4.3882500,4.4080000,4.3927500, 4.1371000, 4.0520400, 4.0116900
                , 3.8880500, 3.8645000, 3.8778000, 3.8194500, 3.8044000, 3.8001000, 4.0370108};
        createEntitiesByYearAndCurrency(2017,"TND",
                values__2017);




        double[] values1_2015 = {0.0164881,0.0164592,0.0163105, 0.0166101, 0.0164775, 0.0166345
                , 0.0165461, 0.0166253, 0.0166009, 0.0165430, 0.0162473, 0.0164386, 0.0164984};
        createEntitiesByYearAndCurrency(2015,"FCFA",
                values1_2015);
        createEntitiesByYearAndCurrency(2015,"XOF (FCFA)",
                values1_2015);
        createEntitiesByYearAndCurrency(2015,"XAF (FCFA)",
                values1_2015);

        double[] values1_2016 = {0.0164058,0.0164264,0.0167313, 0.0167511, 0.0166017, 0.0165689
                , 0.0166024, 0.0165842, 0.0166406, 0.0164698, 0.0162724, 0.0162419, 0.0165247};
        createEntitiesByYearAndCurrency(2016,"FCFA",
                values1_2016);
        createEntitiesByYearAndCurrency(2016,"XOF (FCFA)",
                values1_2016);
        createEntitiesByYearAndCurrency(2016,"XAF (FCFA)",
                values1_2016);

        double[] values1_2017 = {0.0163791,0.0162770,0.0163090, 0.0164462, 0.0166322, 0.0167366
                , 0.0169287, 0.0169912, 0.0169561, 0.0168761, 0.0170148, 0.0170567, 0.0167170};
        createEntitiesByYearAndCurrency(2017,"FCFA",
                values1_2017);
        createEntitiesByYearAndCurrency(2017,"XOF (FCFA)",
                values1_2017);
        createEntitiesByYearAndCurrency(2017,"XAF (FCFA)",
                values1_2017);





        double[] values3_2015 = {0.0307740,0.0306440,0.0314715,0.0304225,0.0307260,0.0298295
                ,0.0299345,0.0296325,0.0295245,0.0292805,0.0300400,0.0292500,0.0301275};
        createEntitiesByYearAndCurrency(2015,"MRO",
                values3_2015);

        double[] values3_2016 = {0.0291200,0.0287400,0.0275700,0.0272900,0.0275400,0.0275500
                ,0.0274500,0.0274300,0.0272400,0.0276600,0.0281700,0.0282100,0.0278308};
        createEntitiesByYearAndCurrency(2016,"MRO",
                values3_2016);

        double[] values3_2017 = {0.0278200,0.0280400,0.0278900, 0.0275400, 0.0270600, 0.0267900
                , 0.0262900, 0.0261300, 0.0262200, 0.0268100, 0.0264600, 0.0263200, 0.0269475};
        createEntitiesByYearAndCurrency(2017,"MRO",
                values3_2017);




        double[] values4_2016 = {1.2700000,1.2700000,1.0800000, 1.0800000, 1.1000000, 1.1019000
                , 1.0986000, 1.1002000, 1.0978000, 1.1116000, 0.5613000, 0.5569000, 1.0356917};
        createEntitiesByYearAndCurrency(2016,"EGP",
                values4_2016);

        double[] values4_2017 = {0.5287000,0.6401000,0.5514000, 0.5480000, 0.5356000, 0.5308000
                , 0.5285000, 0.5330000, 0.5334000, 0.5382000, 0.5300000, 0.5249000, 0.5435500};
        createEntitiesByYearAndCurrency(2017,"EGP",
                values4_2017);




        double[] values10_2015 = {2.5946500,2.6229500,2.7132500, 2.6521500, 2.6856500, 2.6460500
                , 2.6684000, 2.6483500, 2.6545500, 2.6692000, 2.7437500, 2.6955500, 2.6662083};
        createEntitiesByYearAndCurrency(2015,"EAD",
                values10_2015);

        double[] values10_2016 = {3.6872603,3.6727540,3.6729015, 3.6729823, 3.6730587, 3.6730221
                , 3.6729501, 3.6730228, 3.6729012, 3.6728608, 3.6730752, 3.6729981, 3.6741489};
        createEntitiesByYearAndCurrency(2016,"USD/EAD",
                values10_2016);




        double[] values5_2015 = {1,1,1, 1, 1, 1
                , 1, 1, 1, 1, 1, 1, 1};
        createEntitiesByYearAndCurrency(2017,"MAD",
                values5_2015);

        double[] values5_2016 = {1,1,1, 1, 1, 1
                , 1, 1, 1, 1, 1, 1, 1};
        createEntitiesByYearAndCurrency(2017,"MAD",
                values5_2016);

        double[] values5_2017 = {1,1,1, 1, 1, 1
                , 1, 1, 1, 1, 1, 1, 1};
        createEntitiesByYearAndCurrency(2017,"MAD",
                values5_2017);

    }

    private void createEntity( BigDecimal value, int month, int year, String currency,CurrencyRate currencyRate) {
        Optional<CurrencyValue> currencyValue = currencyValueRepository
                .findTopByMonthAndYearAndCurrency(month,year,currencyRepository.findByName(currency).get());
        if (currencyValue.isPresent()) {
            currencyValueRepository
              .saveAndFlush(getEntity(currencyValue.get().getId(), value, month,year, currency,currencyRate));
        }else
            currencyValueRepository.save(getEntity(-1L,value, month,year,  currency,currencyRate));
    }

    private CurrencyValue getEntity(Long id, BigDecimal value, int month, int year, String currency, CurrencyRate currencyRate){
        CurrencyValue currencyValue =  new CurrencyValue();
        currencyValue.setId(id);
        currencyValue.setValue(value);
        currencyValue.setMonth(month);
        currencyValue.setYear(year);
        currencyValue.setCurrencyRate(currencyRate);

        if (currencyRepository.findByName(currency).isPresent())
            currencyValue.setCurrency(currencyRepository.findByName(currency).get());

        return currencyValue;
    }


}
