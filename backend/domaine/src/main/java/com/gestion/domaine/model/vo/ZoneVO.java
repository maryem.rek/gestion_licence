package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.Zone;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ZoneVO {

    private Long id;
    private String  name;
    private int countryCount;

    public ZoneVO() {
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getCountryCount() {
        return countryCount;
    }

    public void setCountryCount(int countryCount) {
        this.countryCount = countryCount;
    }

    public List<ZoneVO> zoneVOS(List<Zone> zones) {
        List<ZoneVO> list = new ArrayList<>();
        for ( int i =0; i<zones.size();i++){
            list.add(new ZoneVO(zones.get(i)));
        }
        return list;
    }

    public ZoneVO(Zone zone){
        if ((zone!=null)) {
            this.id = zone.getId();
            this.name = zone.getName();
            this.countryCount = (zone.getCountries()!=null)? zone.getCountries().size():0;
        }
    }
}
