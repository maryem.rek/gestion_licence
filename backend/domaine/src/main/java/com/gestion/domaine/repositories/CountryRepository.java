package com.gestion.domaine.repositories;

import com.gestion.domaine.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CountryRepository extends JpaRepository<Country,Long> {
    Optional<Country> findByName(String name);
}
