package com.gestion.domaine.rest;

import com.gestion.domaine.model.Family;
import com.gestion.domaine.model.vo.FamilyVO;
import com.gestion.domaine.services.FamilyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/family")
public class FamilyRest {

    @Autowired
    private FamilyService familyService;

    @Autowired
    private FamilyVO familyVo;


    @PostMapping()
    @ApiOperation(value = "Create a new  family, return family if success else return null")
    public ResponseEntity<?> create(@RequestBody Family family){
        Family familyDB = familyService.create(family);
        return  new ResponseEntity<>(
                new FamilyVO(familyDB),familyDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  family, return family if success else return null")
    public ResponseEntity<?> update(@RequestBody Family family){
        Family familyDB = familyService.update(family);
        return  new ResponseEntity<>(
                new FamilyVO(familyDB),familyDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List Family")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                familyVo.familyVOS(familyService.findAll()), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete family by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(familyService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete family")
    public ResponseEntity<?> delete(@RequestBody Family family){
        return new ResponseEntity<>(familyService.delete(family), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get family by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Family family = familyService.findById(id);
        return new ResponseEntity<>(new FamilyVO(family),family!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

}
