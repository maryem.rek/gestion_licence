package com.gestion.domaine.services;

import com.gestion.domaine.model.Currency;
import com.gestion.domaine.repositories.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;

    /**
     * Create Currency : Name should be unique
     * @param currency
     * @return Currency | null
     */
    public Currency create(Currency currency){
        return (currencyRepository.findByName(currency.getName()).isPresent())?
                null:currencyRepository.save(currency);
    }

    /**
     * Update Currency : Name should be unique|present, Id should be present
     * @param currency
     * @return Currency if updated else null
     */
    public Currency update(Currency currency){

        if(currency.getId()!=null && currency.getName()!=null)
        {
            Optional<Currency> currencyValueById   = currencyRepository.findById(currency.getId());
            Optional<Currency> currencyValueByName = currencyRepository.findByName(currency.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(currencyValueByName.isPresent() && !currencyValueById.get().getId().equals(currencyValueByName.get().getId()));
            return (isNameUnique)?
                    currencyRepository.saveAndFlush(currency):null;
        }
        return null;
    }

    /**
     * Get Currency by id
     * @param id
     * @return Currency if updated else null
     */
    public Currency findById(Long id){
        Optional<Currency> currency1 = currencyRepository.findById(id);
        return currency1.orElse(null);
    }

    /**
     * Get Currency by name
     * @param name
     * @return Currency if updated else null
     */
    public Currency findByName(String name){
        Optional<Currency> currency1 = currencyRepository.findByName(name);
        return currency1.orElse(null);
    }

    /**
     * Get all currencys
     * @return List<Currency>
     */
    public List<Currency> findAll(){
        return currencyRepository.findAll();
    }

    /**
     * Get Page of currencys
     * @param page
     * @param size
     * @return Page<Currency>
     */
    public Page<Currency> findPages(int page, int size)
    {
        return currencyRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Currency by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            currencyRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !currencyRepository.findById(id).isPresent();
    }

    /**
     * Delete Currency
     * @param currency
     * @return isDeleted
     */
    public Boolean delete(Currency currency){
        try{
            if (currency.getId()!=null){
                currencyRepository.delete(currency);
                return !currencyRepository.findById(currency.getId()).isPresent();
            }
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return false;
    }

}
