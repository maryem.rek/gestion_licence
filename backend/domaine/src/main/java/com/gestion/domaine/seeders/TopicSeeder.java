package com.gestion.domaine.seeders;

import com.gestion.domaine.model.Topic;
import com.gestion.domaine.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TopicSeeder {

    @Autowired
    TopicRepository topicRepository;

    public void run(){
        createEntity("Activité et résultats des filiales BDI");
        createEntity("Comparatif sectoriel");
    }

    private void createEntity(String name) {
        Optional<Topic> topic1 = topicRepository.findByName(name);
        if (topic1.isPresent()) {
            topicRepository.saveAndFlush(getEntity(name,topic1.get().getId()));
        }else
            topicRepository.save(getEntity(name,-1L));
    }

    private Topic  getEntity(String name,Long id){
         Topic topic =new Topic();
         topic.setName(name);
         topic.setId(id);
         return topic;
     }
}

