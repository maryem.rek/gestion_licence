package com.gestion.domaine.repositories;


import com.gestion.domaine.model.Indicator;
import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.model.Subsidiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IndicatorValueRepository extends JpaRepository<IndicatorValue,Long> {
    @Query(nativeQuery = true, value =
            "select * from indicator_value as iv where iv.indicator_id = :indicator_id and MAKEDATE(iv.year,iv.month) between :startedDate and :endDate")
    List<IndicatorValue> findByIndicatorAndStartDateAndEndDate
            (@Param("indicator_id") long indicatorId,
             @Param("startedDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                     String startedDate,
             @Param("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                     String endDate);

    @Query(nativeQuery = true, value =
            "select * from indicator_value as iv where iv.indicator_id = :indicator_id and iv.subsidiary_id = :subsidiary_id and MAKEDATE(iv.year,iv.month) between :startedDate and :endDate")
    List<IndicatorValue> findByIndicatorAndSubsidiaryAndStartDateAndEndDate
            (@Param("indicator_id") long indicatorId,
             @Param("subsidiary_id") long subsidiaryId,
             @Param("startedDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                     String startedDate,
             @Param("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                     String endDate);

    @Query(nativeQuery = true, value =
            "select id, month, year, value, indicator_id,subsidiary_id  from indicator_value as iv where iv.indicator_id in :indicatorsIds and iv.subsidiary_id in :subsidiariesIds and MAKEDATE(iv.year,iv.month) between :fromDate and :toDate")
    List<IndicatorValue> executeQuery(@Param("indicatorsIds") List<Long> indicatorsIds,@Param("subsidiariesIds") List<Long> subsidiariesIds,@Param("fromDate") String fromDate,@Param("toDate") String toDate);

    @Query(nativeQuery = true, value =
            "select iv.*  from indicator_value as iv where iv.indicator_id in :indicatorsIds " +
                    "and iv.subsidiary_id in :subsidiariesIds and iv.date_value between :fromDate and :toDate")
    List<IndicatorValue> executeQueryfromTo(@Param("indicatorsIds") List<Long> indicatorsIds,@Param("subsidiariesIds") List<Long> subsidiariesIds,@Param("fromDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String fromDate,@Param("toDate")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String toDate);

    @Query(nativeQuery = true, value =
            "select iv.*  from indicator_value as iv where iv.indicator_id in :indicatorsIds " +
                    "and iv.subsidiary_id in :subsidiariesIds and iv.date_value in :dates")
    List<IndicatorValue> executeQueryInTheseDates(@Param("indicatorsIds") List<Long> indicatorsIds,@Param("subsidiariesIds") List<Long> subsidiariesIds,@Param("dates") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) List<String> dates);

    Optional<IndicatorValue> findTopByMonthAndYear(int month, int year);

    Optional<IndicatorValue> findTopByMonthAndYearAndIndicator(int month, int year, Indicator indicator);


    List<IndicatorValue> findByIndicatorAndMonthAndYearAndSubsidiary(Indicator indicator, int month, int year, Subsidiary subsidiary);


    @Query(nativeQuery = true , value = "SELECT MIN(date_value) FROM indicator_value")
    Date findFirstDateEver();

    @Query(nativeQuery = true , value = "SELECT MAX(date_value) FROM indicator_value")
    Date findLastDateEver();

    List<IndicatorValue> findByIndicatorIdAndSubsidiaryId(Long indicatorId, Long subsidiaryId);

    IndicatorValue findTopByIndicatorAndSubsidiaryAndYearAndMonth(Indicator indicator, Subsidiary subsidiary, int year, int month);
}

