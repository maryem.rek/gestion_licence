package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Country  implements Serializable{

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Zone zone;

    @OneToMany(mappedBy = "country",cascade=CascadeType.ALL)// , orphanRemoval=true)
    private List<Subsidiary> subsidiaries;

    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    public Country() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    @JsonIgnore
    public List<Subsidiary> getSubsidiaries() {
        return subsidiaries;
    }

    @JsonSetter
    public void setSubsidiaries(List<Subsidiary> subsidiaries) {
        this.subsidiaries = subsidiaries;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(id, country.id) &&
                Objects.equals(name, country.name) &&
                Objects.equals(zone, country.zone) &&
                Objects.equals(subsidiaries, country.subsidiaries) &&
                Objects.equals(currency, country.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, zone, subsidiaries, currency);
    }
*/
}
