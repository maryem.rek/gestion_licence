package com.gestion.domaine.repositories;


import com.gestion.domaine.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment,Long> {
    Optional<Comment> findByName(String name);
    List<Comment> findByQueryEntityId(Long id);
}
