package com.gestion.domaine.services;

import com.gestion.domaine.model.Topic;
import com.gestion.domaine.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;

    /**
     * Create Topic : Name should be unique
     * @param topic
     * @return Topic | null
     */
    public Topic create(Topic topic){
        return (topicRepository.findByName(topic.getName()).isPresent())?
                null:topicRepository.save(topic);
    }

    /**
     * Update Topic : Name should be unique|present, Id should be present
     * @param topic
     * @return Topic if updated else null
     */
    public Topic update(Topic topic){

        if(topic.getId()!=null && topic.getName()!=null)
        {
            Optional<Topic> topicValueById   = topicRepository.findById(topic.getId());
            Optional<Topic> topicValueByName = topicRepository.findByName(topic.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(topicValueByName.isPresent() && !topicValueById.get().getId().equals(topicValueByName.get().getId()));
            return (isNameUnique)?
                    topicRepository.saveAndFlush(topic):null;
        }
        return null;
    }


    /**
     * Get Topic by id
     * @param id
     * @return Topic if updated else null
     */
    public Topic findById(Long id){
        Optional<Topic> topic1 = topicRepository.findById(id);
        return topic1.orElse(null);
    }

    /**
     * Get Topic by name
     * @param name
     * @return Topic if updated else null
     */
    public Topic findByName(String name){
        Optional<Topic> topic1 = topicRepository.findByName(name);
        return topic1.orElse(null);
    }

    /**
     * Get all topics
     * @return List<Topic>
     */
    public List<Topic> findAll(){
        return topicRepository.findAll();
    }

    /**
     * Get Page of topics
     * @param page
     * @param size
     * @return Page<Topic>
     */
    public Page<Topic> findPages(int page, int size)
    {
        return topicRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Topic by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            topicRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !topicRepository.findById(id).isPresent();
    }

    /**
     * Delete Topic
     * @param topic
     * @return isDeleted
     */
    public Boolean delete(Topic topic){
        try{
            if (topic.getId()!=null){
                topicRepository.delete(topic);
                return !topicRepository.findById(topic.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }

    public List<Topic> findAllTopicsFamiliesIndicators(){
       List<Topic> topics = topicRepository.findAll();
       return null;
    }
}
