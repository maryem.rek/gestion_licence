package com.gestion.domaine.rest;

import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.model.QueryEntity;
import com.gestion.domaine.services.IndicatorValueService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/indicatorValue")
public class IndicatorValueRest {


    @Autowired
    private IndicatorValueService indicatorValueService;

    @PostMapping()
    @ApiOperation(value = "Create a new  indicatorValue, return indicatorValue if success else return null")
    public ResponseEntity<?> create(@RequestBody IndicatorValue indicatorValue){
        IndicatorValue indicatorValueDB = indicatorValueService.create(indicatorValue);
        return  new ResponseEntity<>(
                indicatorValueDB,indicatorValueDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  indicatorValue, return indicatorValue if success else return null")
    public ResponseEntity<?> update(@RequestBody IndicatorValue indicatorValue){
        IndicatorValue indicatorValueDB = indicatorValueService.update(indicatorValue);
        return  new ResponseEntity<>(
                indicatorValueDB,indicatorValueDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List IndicatorValue")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                indicatorValueService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete indicatorValue by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(indicatorValueService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete indicatorValue")
    public ResponseEntity<?> delete(@RequestBody IndicatorValue indicatorValue){
        return new ResponseEntity<>(indicatorValueService.delete(indicatorValue), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get indicatorValue by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        IndicatorValue indicatorValue = indicatorValueService.findById(id);
        return new ResponseEntity<>(indicatorValue,
                indicatorValue!=null? HttpStatus.OK :HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{indicatorId}/{startedDate}/{endDate}/")
    @ApiOperation(value = "get indicatorValue by IndicatorId between two dates")
    public ResponseEntity<?> findByIndicatorAndYearAndMonth(@PathVariable("indicatorId") Long indicatorId,
                                                               @PathVariable("startedDate") String startedDate,
                                                               @PathVariable("endDate") String endDate) {
        return new ResponseEntity<>(indicatorValueService.
                findByIndicatorAndYearAndMonth(indicatorId, startedDate, endDate),HttpStatus.OK);
    }

    @GetMapping("/{indicatorId}/{subsidiaryId}/{startedDate}/{endDate}/")
    @ApiOperation(value = "get indicatorValue by IndicatorId between two dates")
    public ResponseEntity<?> findByIndicatorAndYearAndMonth(@PathVariable("indicatorId") Long indicatorId,
                                                             @PathVariable("subsidiaryId") Long subsidiaryId,
                                                             @PathVariable("startedDate") String startedDate,
                                                             @PathVariable("endDate") String endDate) {
        return new ResponseEntity<>(indicatorValueService.
                findByIndicatorAndSubsidiaryAndStartDateAndEndDate(indicatorId,subsidiaryId, startedDate, endDate),HttpStatus.OK);
    }


    @GetMapping("/indicator/{indicatorId}/subsidiary/{subsidiaryId}/")
    @ApiOperation(value = "get indicatorValue by IndicatorId between two dates")
    public ResponseEntity<?> findByIndicatorAndYearAndMonth(@PathVariable("indicatorId") Long indicatorId,
                                                            @PathVariable("subsidiaryId") Long subsidiaryId) {
        return new ResponseEntity<>(indicatorValueService.
                findByIndicatorIdAndSubsidiaryId(indicatorId,subsidiaryId),HttpStatus.OK);
    }


    @PostMapping("/excute")
    @ApiOperation(value = "excute query")
    public ResponseEntity<?> excuteQuery(@RequestBody QueryEntity queryEntity) {
        return new ResponseEntity<>(
                indicatorValueService.executeQuery(queryEntity),HttpStatus.OK);
    }



}