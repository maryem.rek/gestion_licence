package com.gestion.domaine.rest;

import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.model.vo.IndicatorVO;
import com.gestion.domaine.services.IndicatorService;
import com.gestion.domaine.model.Indicator;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/indicator")
public class IndicatorRest {

    @Autowired
    private IndicatorService indicatorService;

    @Autowired
    private IndicatorVO indicatorVO;


    @PostMapping()
    @ApiOperation(value = "Create a new  indicator, return indicator if success else return null")
    public ResponseEntity<?> create(@RequestBody Indicator indicator){
        Indicator indicatorDB = indicatorService.create(indicator);
        return  new ResponseEntity<>(
                new IndicatorVO(indicatorDB),indicatorDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  indicator, return indicator if success else return null")
    public ResponseEntity<?> update(@RequestBody Indicator indicator){
        Indicator indicatorDB = indicatorService.update(indicator);
        return  new ResponseEntity<>(
                new IndicatorVO(indicatorDB),indicatorDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List Indicator")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                indicatorVO.indicatorVOS(indicatorService.findAll()), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete indicator by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(indicatorService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete indicator")
    public ResponseEntity<?> delete(@RequestBody Indicator indicator){
        return new ResponseEntity<>(indicatorService.delete(indicator), HttpStatus.OK);
    }

    @GetMapping("/parent/{parentId}")
    @ApiOperation(value = "get List of Indicator Children")
    public ResponseEntity<?> findByParentId(@PathVariable("parentId") Long parentId){
        return new ResponseEntity<>(
                indicatorVO.indicatorVOS(indicatorService.findByParentId(parentId)), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get indicator by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Indicator indicator = indicatorService.findById(id);
        return new ResponseEntity<>(new IndicatorVO(indicator) ,indicator!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}/indicatorvalues")
    @ApiOperation(value = "get indicator  indicatorValues by indicator id")
    public ResponseEntity<?> getIndicatorValues(@PathVariable("id") Long id){
        Indicator indicator = indicatorService.findById(id);
        List<IndicatorValue> indicatorValues = indicator!=null? indicator.getIndicatorValues():null;
        return new ResponseEntity<>(indicatorValues,indicatorValues!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

}
