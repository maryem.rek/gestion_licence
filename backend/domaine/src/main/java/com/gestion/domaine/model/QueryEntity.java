package com.gestion.domaine.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.model.enums.ExportFormat;
import com.gestion.domaine.model.enums.Frequency;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QueryEntity implements Serializable{

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Topic topic;

    @ManyToMany()
    private List<Indicator> indicators;

    @OneToMany(mappedBy = "queryEntity", cascade= CascadeType.ALL )//, orphanRemoval=true)
    private List<Comment> comments;

    @ManyToMany()
    private List<Subsidiary> subsidiaries;

    private Date fromDate;

    private Date toDate;

    private boolean discreet;

    @ElementCollection
    private List<Date> dates;

//    @Range(min = 1970)
    private int currencyYear;

//    @Range(min = 1,max = 12, message = "the month value is between 1 and 12")
    private int currencyMonth;

    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    @Enumerated(EnumType.STRING)
    private CurrencyRate currencyRate;

    private Boolean constantCurrency;

    @ManyToOne(fetch = FetchType.LAZY)
    private Formula ratio;

    private Frequency ratioFrequency;

    private ExportFormat exportFormat;

    @ManyToOne(fetch = FetchType.LAZY)
    private DomainUser owner;

    @ManyToMany()
    private List<DomainUser>  sharedWith;

    private String pdfFile;

    @Transient
    private boolean reRun;

    public QueryEntity() {
        this.reRun = false;
    }

    public QueryEntity(QueryEntity queryEntity) {
        this.name = queryEntity.name;
        this.topic = queryEntity.topic;
        this.indicators = queryEntity.indicators;
        this.comments = queryEntity.comments;
        this.subsidiaries = queryEntity.subsidiaries;
        this.fromDate = queryEntity.fromDate;
        this.toDate = queryEntity.toDate;
        this.discreet = queryEntity.discreet;
        this.dates = queryEntity.dates;
        this.currencyYear = queryEntity.currencyYear;
        this.currencyMonth = queryEntity.currencyMonth;
        this.currency = queryEntity.currency;
        this.currencyRate = queryEntity.currencyRate;
        this.constantCurrency = queryEntity.constantCurrency;
        this.ratio = queryEntity.ratio;
        this.ratioFrequency = queryEntity.ratioFrequency;
        this.exportFormat = queryEntity.exportFormat;
        this.owner = queryEntity.owner;
        this.sharedWith = queryEntity.sharedWith;
        this.pdfFile = queryEntity.pdfFile;
        this.reRun = queryEntity.reRun;
    }

    public String getPdfFile() {
        return pdfFile;
    }

    public void setPdfFile(String pdfFile) {
        this.pdfFile = pdfFile;
    }

    public DomainUser getOwner() {
        return owner;
    }

    public void setOwner(DomainUser owner) {
        this.owner = owner;
    }

    public List<DomainUser> getSharedWith() {
        return sharedWith;
    }

    @JsonSetter
    public void setSharedWith(List<DomainUser> sharedWith) {
        this.sharedWith = sharedWith;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public List<Indicator> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Indicator> indicators) {
        this.indicators = indicators;
    }

    
    public List<Comment> getComments() {
        return comments;
    }

    @JsonSetter
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date from) {
        this.fromDate = from;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date to) {
        this.toDate = to;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public CurrencyRate getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(CurrencyRate currencyRate) {
        this.currencyRate = currencyRate;
    }

    public Formula getRatio() {
        return ratio;
    }

    public void setRatio(Formula ratio) {
        this.ratio = ratio;
    }

    public Frequency getRatioFrequency() {
        return ratioFrequency;
    }

    public void setRatioFrequency(Frequency ratioFrequency) {
        this.ratioFrequency = ratioFrequency;
    }

    public ExportFormat getExportFormat() {
        return exportFormat;
    }

    public void setExportFormat(ExportFormat exportFormat) {
        this.exportFormat = exportFormat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Subsidiary> getSubsidiaries() {
        return subsidiaries;
    }

    public void setSubsidiaries(List<Subsidiary> subsidiaries) {
        this.subsidiaries = subsidiaries;
    }

    public int getCurrencyYear() {
        return currencyYear;
    }

    public void setCurrencyYear(int currencyYear) {
        this.currencyYear = currencyYear;
    }

    public int getCurrencyMonth() {
        return currencyMonth;
    }

    public void setCurrencyMonth(int currencyMonth) {
        this.currencyMonth = currencyMonth;
    }

    public boolean isDiscreet() {
        return discreet;
    }

    public void setDiscreet(boolean discreet) {
        this.discreet = discreet;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public Boolean getConstantCurrency() {
        return constantCurrency;
    }

    public void setConstantCurrency(Boolean constantCurrency) {
        this.constantCurrency = constantCurrency;
    }

    public boolean isReRun() {
        return reRun;
    }

    public void setReRun(boolean reRun) {
        this.reRun = reRun;
    }
}

