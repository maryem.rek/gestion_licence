package com.gestion.domaine.seeders;

import com.gestion.domaine.model.Zone;
import com.gestion.domaine.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ZoneSeeder {

    @Autowired
    ZoneRepository zoneRepository;

    public void run(){
        createEntity("Afrique du nord");
        createEntity("CEMAC");
        createEntity("UEMOA");
    }

    private void createEntity(String name) {
        Optional<Zone> zone1 = zoneRepository.findByName(name);
        if (zone1.isPresent()) {
            zoneRepository.saveAndFlush(getEntity(name,zone1.get().getId()));
        }else
            zoneRepository.save(getEntity(name,-1L));
    }

    private Zone getEntity(String name,Long id){
         Zone zone =new Zone();
         zone.setName(name);
         zone.setId(id);
         return zone;
     }
}

