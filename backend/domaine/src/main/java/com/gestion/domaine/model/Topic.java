package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Topic implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "topic",cascade=CascadeType.ALL)//, orphanRemoval=true)
    private List<Family> families;

    @OneToMany(mappedBy = "topic",cascade= CascadeType.ALL)//, orphanRemoval=true)
    private List<QueryEntity> queryEntities;

    public Topic() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> familyList) {
        this.families = familyList;
    }

    @JsonIgnore
    public List<QueryEntity> getQueryEntities() {
        return queryEntities;
    }

    @JsonSetter
    public void setQueryEntities(List<QueryEntity> queryEntities) {
        this.queryEntities = queryEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Topic topic = (Topic) o;
        return Objects.equals(id, topic.id) &&
                Objects.equals(name, topic.name) &&
                Objects.equals(queryEntities, topic.queryEntities) &&
                Objects.equals(families, topic.families);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, queryEntities, families);
    }
}
