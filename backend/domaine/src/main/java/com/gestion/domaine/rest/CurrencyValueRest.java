package com.gestion.domaine.rest;

import com.gestion.domaine.model.CurrencyValue;
import com.gestion.domaine.model.QueryEntity;
import com.gestion.domaine.services.CurrencyService;
import com.gestion.domaine.services.CurrencyValueService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/currencyValue")
public class CurrencyValueRest {

    @Autowired
    private CurrencyValueService currencyValueService;

    @Autowired
    private CurrencyService currencyService;

    @PostMapping()
    @ApiOperation(value = "Create a new  currencyValue, return currencyValue if success else return null")
    public ResponseEntity<?> create(@RequestBody CurrencyValue currencyValue){
        CurrencyValue currencyValueDB = currencyValueService.create(currencyValue);
        return  new ResponseEntity<>(
                currencyValueDB,currencyValueDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  currencyValue, return currencyValue if success else return null")
    public ResponseEntity<?> update(@RequestBody CurrencyValue currencyValue){
        CurrencyValue currencyValueDB = currencyValueService.update(currencyValue);
        return  new ResponseEntity<>(
                currencyValueDB,currencyValueDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List CurrencyValue")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                currencyValueService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete currencyValue by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(currencyValueService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete currencyValue")
    public ResponseEntity<?> delete(@RequestBody CurrencyValue currencyValue){
        return new ResponseEntity<>(currencyValueService.delete(currencyValue), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get currencyValue by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        CurrencyValue currencyValue = currencyValueService.findById(id);
        return new ResponseEntity<>(currencyValue,
                currencyValue!=null? HttpStatus.OK :HttpStatus.NOT_FOUND);
    }


    @PostMapping("/query")
    @ApiOperation(value = "get currencyValue by queryEntity")
    public ResponseEntity<?> findByMonthAndYearAndCurrencyAndCurrencyRate
            (@RequestBody QueryEntity queryEntity){
        boolean isValid = queryEntity.getCurrency()!=null && queryEntity.getCurrencyRate()!=null
                && queryEntity.getCurrencyYear()!=0 && queryEntity.getCurrencyYear()!=0;
        if (isValid){
            CurrencyValue currencyValue = currencyValueService.findByYearAndCurrencyRateAndCurrency
                    (queryEntity.getCurrencyMonth(),queryEntity.getCurrencyYear(),queryEntity.getCurrencyRate(),queryEntity.getCurrency());
            return new ResponseEntity<>(currencyValue,
                    currencyValue!=null? HttpStatus.OK :HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }


}
