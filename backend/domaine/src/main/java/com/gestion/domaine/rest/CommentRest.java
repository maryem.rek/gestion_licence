package com.gestion.domaine.rest;

import com.gestion.domaine.model.Comment;
import com.gestion.domaine.services.CommentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comment")
public class CommentRest {

    @Autowired
    private CommentService commentService;

    @PostMapping()
    @ApiOperation(value = "Create a new  comment, return comment if success else return null")
    public ResponseEntity<?> create(@RequestBody Comment comment){
        Comment commentDB = commentService.create(comment);
        return  new ResponseEntity<>(
                commentDB,commentDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  comment, return comment if success else return null")
    public ResponseEntity<?> update(@RequestBody Comment comment){
        Comment commentDB = commentService.update(comment);
        return  new ResponseEntity<>(
                commentDB,commentDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List Comment")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                commentService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete comment by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(commentService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete comment")
    public ResponseEntity<?> delete(@RequestBody Comment comment){
        return new ResponseEntity<>(commentService.delete(comment), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get comment by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Comment comment = commentService.findById(id);
        return new ResponseEntity<>(comment,
                comment!=null? HttpStatus.OK :HttpStatus.NOT_FOUND);
    }

}
