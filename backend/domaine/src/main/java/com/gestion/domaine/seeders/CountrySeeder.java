package com.gestion.domaine.seeders;

import com.gestion.domaine.model.Country;
import com.gestion.domaine.repositories.CountryRepository;
import com.gestion.domaine.repositories.CurrencyRepository;
import com.gestion.domaine.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CountrySeeder {

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    ZoneRepository zoneRepository;

    @Autowired
    CurrencyRepository currencyRepository;

    public void run(){
        createEntity("Mali","UEMOA","XOF (FCFA)");
        createEntity("Sénégal","UEMOA","XOF (FCFA)");
        createEntity("Côte d'ivoire","UEMOA","XOF (FCFA)");
        createEntity("Congo brazzaville","CEMAC","XAF (FCFA)");
        createEntity("Gabon","CEMAC","XAF (FCFA)");
        createEntity("Cameroun","CEMAC","XAF (FCFA)");
        createEntity("Tunisie","Afrique du nord","TND");
        createEntity("Mauritanie","Afrique du nord","MRO");
    }

    private void createEntity(String name, String zone, String currency) {
        Optional<Country> country1 = countryRepository.findByName(name);
        if (country1.isPresent()) {
            countryRepository
              .saveAndFlush(getEntity(country1.get().getId(), name, zone, currency));
        }else
            countryRepository.save(getEntity(-1L,name, zone, currency));
    }

    private Country  getEntity(Long id, String name
            ,String zone, String currency){
        Country country =new Country();
        country.setName(name);
        country.setCurrency(currencyRepository.findByName(currency).get());
       // country.setSubsidiaries(null);
        country.setZone(zoneRepository.findByName(zone).get());
        country.setId(id);
        return country;
    }


}
