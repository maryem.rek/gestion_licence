package com.gestion.domaine.rest;

import com.gestion.domaine.model.Comment;
import com.gestion.domaine.model.Indicator;
import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.model.QueryEntity;
import com.gestion.domaine.services.CurrencyValueService;
import com.gestion.domaine.services.QueryEntityService;
import com.lamrani.auth.model.User;
import com.lamrani.auth.service.UserService;
import com.lamrani.domaine.model.*;
import com.gestion.domaine.model.vo.VarianceTCAM;
import com.gestion.domaine.services.IndicatorValueService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/queryEntity")
public class QueryEntityRest {

    @Autowired
    private QueryEntityService queryEntityService;

    @Autowired
    private IndicatorValueService indicatorValueService;

    @Autowired
    private CurrencyValueService currencyValueService;

    @Autowired
    private UserService userService;

    @PostMapping()
    @ApiOperation(value = "Create a new  queryEntity, return queryEntity if success else return null")
    public ResponseEntity<?> create(@RequestBody QueryEntity queryEntity){
        QueryEntity queryEntityDB = queryEntityService.create(queryEntity);
        return  new ResponseEntity<>(
                queryEntityDB,queryEntityDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  queryEntity, return queryEntity if success else return null")
    public ResponseEntity<?> update(@RequestBody QueryEntity queryEntity){
        QueryEntity queryEntityDB = queryEntityService.update(queryEntity);
        return  new ResponseEntity<>(
                queryEntityDB,queryEntityDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List QueryEntity")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                queryEntityService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete queryEntity by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(queryEntityService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete queryEntity")
    public ResponseEntity<?> delete(@RequestBody QueryEntity queryEntity){
        return new ResponseEntity<>(queryEntityService.delete(queryEntity), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get queryEntity by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        QueryEntity queryEntity = queryEntityService.findById(id);
        return new ResponseEntity<>(queryEntity,
                queryEntity!=null? HttpStatus.OK :HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}/comments")
    @ApiOperation(value = "get the queryEntity comments")
    public ResponseEntity<?> getQueryEntityComments(@PathVariable("id") Long id){
        List<Comment> comments =  queryEntityService.getQueryEntityComments(id);
        return new ResponseEntity<>(comments,HttpStatus.OK);
    }

    @PostMapping("/execute")
    @ApiOperation(value = "get indicator values based on query params")
    public ResponseEntity<?> excuteQuery(@RequestBody  QueryEntity queryEntity){
        List<IndicatorValue> indicatorValues = queryEntityService.executeQuery(queryEntity);

        Map<Long,List<Indicator>> mapIdKey =
        queryEntityService.transformeMapKeysIntoIdsInsteadOfSubsidiaries(indicatorValues);

        Map<Long, Map<Long, VarianceTCAM>> variancesAndTcam = queryEntityService.calculateVarianceAndTCAM(mapIdKey, queryEntity);

        ModelMap model = new ModelMap();
        model.put("variancesAndTcam",variancesAndTcam);
        model.put("indicatorValues",indicatorValues);

        return new ResponseEntity<>(model,HttpStatus.OK);
    }

    @GetMapping("/myqueryentities")
    @ApiOperation(value = "get queryEntities i wrote or shared with me")
    public ResponseEntity<?> getQueryEntitiesIwroteOrShatedWithMe(){
        User user = userService.getAuthenticatedUser();
        List<QueryEntity> queryEntities = new ArrayList<>();
        if (user != null){
            queryEntities =  queryEntityService.getQueryEntitiesIwroteOrShatedWithMe(user.getId());
        }
        return new ResponseEntity<>(queryEntities,HttpStatus.OK);
    }

}


