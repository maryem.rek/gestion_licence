package com.gestion.domaine.seeders;

import com.gestion.domaine.model.Family;
import com.gestion.domaine.repositories.FamilyRepository;
import com.gestion.domaine.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class FamilySeeder {


    @Autowired
    FamilyRepository familyRepository;

    @Autowired
    TopicRepository topicRepository;

    public void run(){
        createEntity("Ratios","Activité et résultats des filiales BDI");
        createEntity("Parts de marché","Comparatif sectoriel");
        createEntity("P&L social","Activité et résultats des filiales BDI");
    }

    private void createEntity(String name, String topic) {
        Optional<Family> family1 = familyRepository.findByName(name);
        if (family1.isPresent()) {
            familyRepository
              .saveAndFlush(getEntity(family1.get().getId(), name,topic));
        }else
            familyRepository.save(getEntity(-1L,name,topic));
    }

    private Family  getEntity(Long id, String name
            ,String topic){
        Family family =new Family();
        family.setName(name);
        family.setTopic(topicRepository.findByName(topic).get());
        family.setId(id);
        return family;
    }


}
