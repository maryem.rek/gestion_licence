package com.gestion.domaine.rest;

import com.gestion.domaine.model.Zone;
import com.gestion.domaine.model.vo.ZoneVO;
import com.gestion.domaine.services.ZoneService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/zone")
public class ZoneRest {

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private ZoneVO zoneVo;

    @PostMapping()
    @ApiOperation(value = "Create a new  zone, return zone if success else return null")
    public ResponseEntity<?> create(@RequestBody Zone zone){
        Zone zoneDB = zoneService.create(zone);
        return  new ResponseEntity<>(
                new ZoneVO(zoneDB),zoneDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  zone, return zone if success else return null")
    public ResponseEntity<?> update(@RequestBody Zone zone){
        Zone zoneDB = zoneService.update(zone);
        return  new ResponseEntity<>(
                new ZoneVO(zoneDB),zoneDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List Zone")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                zoneVo.zoneVOS(zoneService.findAll()), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete zone by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(zoneService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete zone")
    public ResponseEntity<?> delete(@RequestBody Zone zone){
        return new ResponseEntity<>(zoneService.delete(zone), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get zone by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Zone zone = zoneService.findById(id);
        return new ResponseEntity<>(new ZoneVO(zone),zone!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }


}
