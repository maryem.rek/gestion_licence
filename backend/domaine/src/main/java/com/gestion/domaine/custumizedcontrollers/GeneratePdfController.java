package com.gestion.domaine.custumizedcontrollers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.gestion.domaine.model.QueryEntity;
import com.gestion.domaine.services.QueryEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/pdf")
public class GeneratePdfController {

    @Autowired
    private QueryEntityService queryEntityService;

    @PostMapping("/generate")
    public ResponseEntity<?> exportPdf(@RequestBody PrintMaping print, HttpServletRequest request) {
        byte[] out= generatePDF(print.getHtml(), print.getId(), print.getSaved());
        if(print.getSaved()) {
        	
        	return null;
        }
        
        ModelMap model = new ModelMap();
	    model.put("body",out);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(model, headers, HttpStatus.OK);
    }

    @GetMapping("/download/{pdfName}")
    public ResponseEntity<?> downloadPdf(@PathVariable("pdfName") String pdfName) throws IOException {

    	byte[] out = null;
		try {
			InputStream inputStream = new FileInputStream("peaqock_import"+File.separator+"exportPdf"+File.separator+pdfName);
				out = org.apache.commons.io.IOUtils.toByteArray(inputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		ModelMap model = new ModelMap();
	    model.put("body",out);

        HttpHeaders headers = new HttpHeaders();
        return new ResponseEntity<>(model, headers, HttpStatus.OK);
			
    }


    //TODO these methods should be placed into some service
    //generte pdf
    private byte[] generatePDF(String html, Long Id, boolean saved){
            String name= String.valueOf(new Date().getTime())+"_exportPdf";
            this.saveFile(html,name);
            try {
                File f= new File("peaqock_import"+ File.separator+"exportPdf"+File.separator+name.concat(".html"));
                Runtime rt = Runtime.getRuntime();
                String cmd = null;
                String OS = System.getProperty("os.name");
                if(OS.startsWith("Windows")){
                    cmd="cmd.exe /c electron-pdf " + "peaqock_import"+File.separator+"exportPdf"+File.separator+name.concat(".html") + " " +"peaqock_import"+File.separator+"exportPdf"+File.separator+name.concat(".pdf");
                }
                else{
                    cmd="electron-pdf " + f.getAbsolutePath() + " "+"peaqock_import"+File.separator+"exportPdf"+File.separator+name.concat(".pdf");
                }
                cmd = cmd+ " -p A3";
                System.out.println(cmd);
                Process pr = rt.exec(cmd);
                pr.waitFor();
                InputStream inputStrem = pr.getInputStream();
                BufferedReader r = new BufferedReader(new InputStreamReader(inputStrem));
                String line;
                while (true) {
                    line = r.readLine();
                    if (line == null) { break; }
                    if("Shutting down...".equals(line)){
                        if(saved) {
                            QueryEntity queryEntity = queryEntityService.findById(Id);
                            if(queryEntity!=null) {
                                queryEntity.setPdfFile(name.concat(".pdf"));
                                queryEntityService.update(queryEntity);
                            }
                        }
                        return this.downloadPDF(name, saved);
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        return null;
        }

    private void saveFile(String html, String name){
            BufferedWriter writer = null;
            try{
                writer = new BufferedWriter( new FileWriter("peaqock_import"+File.separator+"exportPdf"+File.separator+name+".html"));
                writer.write(html);
            }catch ( IOException e){
                e.printStackTrace();
            }
            finally {
                try{
                    if ( writer != null)
                        writer.close( );
                }
                catch ( IOException e){
                    e.printStackTrace();
                }
            }
        }

    private byte[] downloadPDF(String fileName, boolean saved){

        if(fileName!=null){
                InputStream inputStream = null;
                byte[] out = null;
                try {
                    inputStream = new FileInputStream("peaqock_import"+File.separator+"exportPdf"+File.separator+fileName.concat(".pdf"));
                    out = org.apache.commons.io.IOUtils.toByteArray(inputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File pdf = new File("peaqock_import"+File.separator+"exportPdf"+File.separator+fileName.concat(".pdf"));
                if(!saved) {
                    pdf.delete();
                }
                File index = new File("peaqock_import"+File.separator+"exportPdf"+File.separator+fileName.concat(".html"));
                index.delete();
                return out;
            }
            return null;
        }

}

//TODO this class should be placed into model package..
class PrintMaping{
    Long id;
    String html;
    Boolean saved;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getSaved() {
        return saved;
    }

    public void setSaved(Boolean saved) {
        this.saved = saved;
    }
}




