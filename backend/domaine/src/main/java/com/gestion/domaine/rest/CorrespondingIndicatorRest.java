package com.gestion.domaine.rest;

import com.gestion.domaine.model.CorrespondingIndicator;
import com.gestion.domaine.services.CorrespondingIndicatorService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/correspondingIndicator")
public class CorrespondingIndicatorRest {

    @Autowired
    private CorrespondingIndicatorService correspondingIndicatorService;


    @PostMapping()
    @ApiOperation(value = "Create a new  correspondingIndicator, return correspondingIndicator if success else return null")
    public ResponseEntity<?> create(@RequestBody CorrespondingIndicator correspondingIndicator){
        CorrespondingIndicator correspondingIndicatorDB = correspondingIndicatorService.create(correspondingIndicator);
        return  new ResponseEntity<>(
                correspondingIndicatorDB,correspondingIndicatorDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  correspondingIndicator, return correspondingIndicator if success else return null")
    public ResponseEntity<?> update(@RequestBody CorrespondingIndicator correspondingIndicator){
        CorrespondingIndicator correspondingIndicatorDB = correspondingIndicatorService.update(correspondingIndicator);
        return  new ResponseEntity<>(
                correspondingIndicatorDB,correspondingIndicatorDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List CorrespondingIndicator")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                correspondingIndicatorService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete correspondingIndicator by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(correspondingIndicatorService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete correspondingIndicator")
    public ResponseEntity<?> delete(@RequestBody CorrespondingIndicator correspondingIndicator){
        return new ResponseEntity<>(correspondingIndicatorService.delete(correspondingIndicator), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get correspondingIndicator by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        CorrespondingIndicator correspondingIndicator = correspondingIndicatorService.findById(id);
        return new ResponseEntity<>(correspondingIndicator,correspondingIndicator!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }


}
