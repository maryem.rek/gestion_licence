package com.gestion.domaine.repositories;


import com.gestion.domaine.model.DomainUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDomainRepository extends JpaRepository<DomainUser,Long> {
}
