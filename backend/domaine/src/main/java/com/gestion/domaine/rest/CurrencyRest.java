package com.gestion.domaine.rest;

import com.gestion.domaine.model.Currency;
import com.gestion.domaine.model.CurrencyValue;
import com.gestion.domaine.services.CurrencyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/currency")
public class CurrencyRest {

    @Autowired
    private CurrencyService currencyService;

    @PostMapping()
    @ApiOperation(value = "Create a new  currency, return currency if success else return null")
    public ResponseEntity<?> create(@RequestBody Currency currency){
        Currency currencyDB = currencyService.create(currency);
        return  new ResponseEntity<>(
                currencyDB,currencyDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  currency, return currency if success else return null")
    public ResponseEntity<?> update(@RequestBody Currency currency){
        Currency currencyDB = currencyService.update(currency);
        return  new ResponseEntity<>(
                currencyDB,currencyDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }


    @GetMapping()
    @ApiOperation(value = "Get List Currency")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(currencyService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete currency by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(currencyService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete currency")
    public ResponseEntity<?> delete(@RequestBody Currency currency){
        return new ResponseEntity<>(currencyService.delete(currency), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get currency by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Currency currency = currencyService.findById(id);
        return new ResponseEntity<>(currency,currency!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}/currencyvalues")
    @ApiOperation(value = "get currency  currencyValues by currency id")
    public ResponseEntity<?> getCurrencyValues(@PathVariable("id") Long id){
        Currency currency = currencyService.findById(id);
        List<CurrencyValue> currencyValues = currency!=null? currency.getCurrencyValues():null;
        return new ResponseEntity<>(currencyValues,currencyValues!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

}
