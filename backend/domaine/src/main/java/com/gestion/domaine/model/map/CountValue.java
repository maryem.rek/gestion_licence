package com.gestion.domaine.model.map;

import com.gestion.domaine.model.IndicatorValue;

import java.math.BigDecimal;
import java.util.Objects;

public class CountValue {

    private BigDecimal value;

    private BigDecimal sum;

    private IndicatorValue indicatorValue;

    public CountValue() {
        value = new BigDecimal(0);
        sum = new BigDecimal(0);
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public IndicatorValue getIndicatorValue() {
        return indicatorValue;
    }

    public void setIndicatorValue(IndicatorValue indicatorValue) {
        this.indicatorValue = indicatorValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountValue that = (CountValue) o;
        return Objects.equals(value, that.value) &&
                Objects.equals(sum, that.sum);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value, sum);
    }
}
