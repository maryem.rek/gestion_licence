package com.gestion.domaine.rest;

import com.gestion.domaine.model.Formula;
import com.gestion.domaine.services.FormulaService;
import com.gestion.domaine.model.Indicator;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/formula")
public class FormulaRest {

    @Autowired
    private FormulaService formulaService;


    @PostMapping()
    @ApiOperation(value = "Create a new  formula, return formula if success else return null")
    public ResponseEntity<?> create(@RequestBody Formula formula){
        Formula formulaDB = formulaService.create(formula);
        return  new ResponseEntity<>(
                formulaDB,formulaDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  formula, return formula if success else return null")
    public ResponseEntity<?> update(@RequestBody Formula formula){
        Formula formulaDB = formulaService.update(formula);
        return  new ResponseEntity<>(
                formulaDB,formulaDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List Formula")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                formulaService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete formula by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(formulaService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete formula")
    public ResponseEntity<?> delete(@RequestBody Formula formula){
        return new ResponseEntity<>(formulaService.delete(formula), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get formula by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Formula formula = formulaService.findById(id);
        return new ResponseEntity<>(formula,formula!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

    @GetMapping("/formula/{formula}/{from}/{to}")
    @ApiOperation(value = "get formula by id")
    public ResponseEntity<?> calculateFormula(
            @PathVariable("formula") String formula,
            @PathVariable("to") String to
            ,@PathVariable("from") String from){
        List<Indicator> indicators = formulaService.getIndicatorsFromFormula(formulaService.getFinalFormula(formula));
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date fromDate = df.parse(from);
            Date toDate = df.parse(to);
            List<Date> dates = formulaService.getDates(fromDate,toDate);
            int s = dates.size();
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return new ResponseEntity<>(indicators,HttpStatus.OK);
    }
}
