package com.gestion.domaine.model.enums;

public enum Frequency {
    MONTHLY, QUARTERLY, ANNUAL
}
