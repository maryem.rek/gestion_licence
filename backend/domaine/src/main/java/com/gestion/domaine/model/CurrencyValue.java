package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gestion.domaine.model.enums.CurrencyRate;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CurrencyValue {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Range(min = 1970)
    private int year;

    @Range(min = 0,max = 12, message = "the month value is between 1 and 12")
    private int month;

    private Date dateValue;

    private BigDecimal value;

    @Enumerated(EnumType.STRING)
    private CurrencyRate currencyRate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Currency currency;

    public CurrencyValue() {
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date date) {
        this.dateValue = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public CurrencyRate getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(CurrencyRate currencyRate) {
        this.currencyRate = currencyRate;
    }
}
