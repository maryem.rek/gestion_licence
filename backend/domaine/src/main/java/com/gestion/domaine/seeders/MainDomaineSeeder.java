package com.gestion.domaine.seeders;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class MainDomaineSeeder implements CommandLineRunner {

    @Autowired
    private
    ZoneSeeder zoneSeeder;

    @Autowired
    private
    CurrencySeeder currencySeeder;

    @Autowired
    private
    CountrySeeder countrySeeder;

    @Autowired
    private
    SubsidiarySeeder subsidiarySeeder;

    @Autowired
    private
    TopicSeeder topicSeeder;

    @Autowired
    private
    FamilySeeder familySeeder;

    @Autowired
    private
    IndicatorSeeder indicatorSeeder;

    @Autowired
    private
    CorrespondingIndicatorSeeder correspondingIndicatorSeeder;

    @Autowired
    private  CurrencyValueSeeder currencyValueSeeder;

    @Autowired
    private IndicatorValueSeeder indicatorValueSeeder;


    @Override
    public void run(String... args) throws Exception {
    	new File(new File("").getAbsolutePath()+File.separator+"peaqock_import"+File.separator+"exportPdf").mkdirs();

        /*
        zoneSeeder.run();
        currencySeeder.run();
        countrySeeder.run();
        subsidiarySeeder.run();
        topicSeeder.run();
        familySeeder.run();
        indicatorSeeder.run();
        //correspondingIndicatorSeeder.run();
        indicatorValueSeeder.run();
        currencyValueSeeder.run();
*/

    }
}
