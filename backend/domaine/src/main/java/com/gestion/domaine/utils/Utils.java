package com.gestion.domaine.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class Utils {

    public String saveFile(MultipartFile file, String url, String prefix)
    {
        try {
        String orgName = file.getOriginalFilename();
        String[] parts = orgName.split("\\.");
        String extension = parts[parts.length-1];
        Date currentDate = new Date();
        String name = currentDate.getTime()+"_"+prefix+"."+extension;
        String newURL = url+name;
        File fileToSave = new File(newURL);
        fileToSave.createNewFile();
        FileOutputStream fos = new FileOutputStream(fileToSave);
        fos.write(file.getBytes());
        fos.close();
        return name;
    } catch (IOException e) {
        e.printStackTrace();
        return null;
    }
    }

    public void deleteFile(String url, String imagename){
        if(imagename!=null){
            String newURL = url + imagename;
            File img = new File(newURL);
            img.delete();
        }
    }

}
