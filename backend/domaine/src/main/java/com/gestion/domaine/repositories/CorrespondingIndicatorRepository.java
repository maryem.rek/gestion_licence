package com.gestion.domaine.repositories;


import com.gestion.domaine.model.CorrespondingIndicator;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CorrespondingIndicatorRepository extends JpaRepository<CorrespondingIndicator,Long> {
    Optional<CorrespondingIndicator> findByName(String name);
}
