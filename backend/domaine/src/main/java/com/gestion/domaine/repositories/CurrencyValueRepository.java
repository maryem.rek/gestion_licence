package com.gestion.domaine.repositories;


import com.gestion.domaine.model.Currency;
import com.gestion.domaine.model.CurrencyValue;
import com.gestion.domaine.model.enums.CurrencyRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

public interface CurrencyValueRepository extends JpaRepository<CurrencyValue,Long> {

    CurrencyValue findTopByYearAndCurrencyRateAndCurrency
            (int year, CurrencyRate currencyRate, Currency currency);


    CurrencyValue findTopByYearAndMonthAndCurrencyRateAndCurrency
            (int year,  int month, CurrencyRate currencyRate, Currency currency);

    Optional<CurrencyValue> findTopByMonthAndYear(int month, int year);

    /**
     * get currencyValue by month year and currency
     * @param month
     * @param year
     * @param currency
     * @return
     */
    Optional<CurrencyValue> findTopByMonthAndYearAndCurrency(int month, int year, Currency currency);


    Optional<CurrencyValue> findTopAverageCurrencyValueValueByYearAndMonthLessThanAndCurrency(int year, int month, Currency currency);


    /**
     * find currencyValue by year, month  less than , and currency
     * @param year
     * @param month
     * @param currencyId
     * @return
     */
    @Query(nativeQuery = true, value = "select avg(value) from currency_value where currency_id= :currencyId and year= :year and month <= :month")
    BigDecimal findAverageCurrencyValueValueByYearAndMonthLessThanAndCurrency(@Param("year")  int year,@Param("month") int month,@Param("currencyId") Long currencyId);

    @Query(nativeQuery = true , value = "SELECT MIN(date_value) FROM currency_value")
    Date findFirstDateEver();

    @Query(nativeQuery = true , value = "SELECT MAX(date_value) FROM currency_value")
    Date findLastDateEver();
}
