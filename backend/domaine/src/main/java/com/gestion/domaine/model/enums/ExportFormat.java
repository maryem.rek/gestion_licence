package com.gestion.domaine.model.enums;

public enum ExportFormat {
    PDF,PPT,EXCEL
}
