package com.gestion.domaine.custumizedcontrollers;

import com.gestion.domaine.model.vo.*;
import com.gestion.domaine.services.*;
import com.lamrani.domaine.model.vo.*;
import com.lamrani.domaine.services.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/custumized")
public class custumizedEndPoints {

    @Autowired
    private IndicatorService indicatorService;

    @Autowired
    private FamilyService familyService;

    @Autowired
    private FamilyVO familyVo;

    @Autowired
    private IndicatorVO indicatorVO;

    @Autowired
    private ZoneVO zoneVO;

    @Autowired
    private CurrencyVO currencyVO;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private TopicVO topicVO;

    @Autowired
    private TopicService topicService;

    @Autowired
    private SubsidiaryService subsidiaryService;

    @Autowired
    private IndicatorValueService indicatorValueService;
    @Autowired
    private CurrencyValueService currencyValueService;

    @GetMapping("/indicators-families")
    @ApiOperation(value = "Get families and indicators")
    public ResponseEntity<?> indicatorsFamilies(){
        ModelMap model = new ModelMap();
        model.put("indicators",indicatorVO.indicatorVOS(indicatorService.findAll()));
        model.put("families",familyVo.familyVOS(familyService.findAll()));
        return new ResponseEntity<>( model, HttpStatus.OK);
    }

    @GetMapping("/indicators-subsidiaries-currencies")
    @ApiOperation(value = "Get topics and indicators")
    public ResponseEntity<?> indicatorsTopics(){
        ModelMap model = new ModelMap();
        model.put("indicators",indicatorVO.indicatorVOS(indicatorService.findAll()));
        model.put("subsidiaries",subsidiaryService.findAll());
        model.put("currencies",currencyVO.CurrencyVOS(currencyService.findAll()));
        model.put("currencyDates",currencyValueService.findFirstDateAndLastDateEver());
        model.put("indicatorValueDates",indicatorValueService.findFirstDateAndLastDateEver());
        return new ResponseEntity<>( model, HttpStatus.OK);
    }

    @GetMapping("/zones-currencies")
    @ApiOperation(value = "Get zones and currencies")
    public ResponseEntity<?> findAll(){
        ModelMap model = new ModelMap();
        model.put("zones",zoneVO.zoneVOS(zoneService.findAll()));
        model.put("currencies",currencyVO.CurrencyVOS(currencyService.findAll()));
        return new ResponseEntity<>( model, HttpStatus.OK);
    }

}
