package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.Country;
import com.gestion.domaine.model.IndicatorValue;
import com.gestion.domaine.model.Subsidiary;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class SubsidiaryVO {

    private Long id;

    private String  detailedCompanyName;

    private String bank;

    private String logo;

    private byte[] avatar;

    private Date createdAt;

    private String shareholders;

    private Country country;

    private Long numberOfAgencies;

    private String PDMCredits;

    private String PDMDepots;

    private List<IndicatorValue> indicatorValues;

    public SubsidiaryVO() {
    }

    public SubsidiaryVO(Subsidiary subsidiary){
        if ((subsidiary!=null)) {
            this.logo = subsidiary.getLogo();
            this.bank= subsidiary.getBank();
            this.country= subsidiary.getCountry();
            this.createdAt = subsidiary.getCreatedAt();
            this.detailedCompanyName = subsidiary.getDetailedCompanyName();
            this.id = subsidiary.getId();
            this.indicatorValues = subsidiary.getIndicatorValues();
            this.numberOfAgencies = subsidiary.getNumberOfAgencies();
            this.PDMCredits = subsidiary.getPDMCredits();
            this.PDMDepots = subsidiary.getPDMDepots();
            this.shareholders = subsidiary.getShareholders();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetailedCompanyName() {
        return detailedCompanyName;
    }

    public void setDetailedCompanyName(String detailedCompanyName) {
        this.detailedCompanyName = detailedCompanyName;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getShareholders() {
        return shareholders;
    }

    public void setShareholders(String shareholders) {
        this.shareholders = shareholders;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Long getNumberOfAgencies() {
        return numberOfAgencies;
    }

    public void setNumberOfAgencies(Long numberOfAgencies) {
        this.numberOfAgencies = numberOfAgencies;
    }

    public String getPDMCredits() {
        return PDMCredits;
    }

    public void setPDMCredits(String PDMCredits) {
        this.PDMCredits = PDMCredits;
    }

    public String getPDMDepots() {
        return PDMDepots;
    }

    public void setPDMDepots(String PDMDepots) {
        this.PDMDepots = PDMDepots;
    }

    public List<IndicatorValue> getIndicatorValues() {
        return indicatorValues;
    }

    public void setIndicatorValues(List<IndicatorValue> indicatorValues) {
        this.indicatorValues = indicatorValues;
    }

    public byte[] getAvatar() {
        if(this.logo!=null){
            String filePath = new File("").getAbsolutePath()+"\\peaqock_src\\image\\subsidiaries\\"+this.logo;
            byte[] bFile;
            try {
                bFile = Files.readAllBytes(new File(filePath).toPath());
                return bFile;
            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubsidiaryVO that = (SubsidiaryVO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(detailedCompanyName, that.detailedCompanyName) &&
                Objects.equals(bank, that.bank) &&
                Objects.equals(logo, that.logo) &&
                Arrays.equals(avatar, that.avatar) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(shareholders, that.shareholders) &&
                Objects.equals(country, that.country) &&
                Objects.equals(numberOfAgencies, that.numberOfAgencies) &&
                Objects.equals(PDMCredits, that.PDMCredits) &&
                Objects.equals(PDMDepots, that.PDMDepots) &&
                Objects.equals(indicatorValues, that.indicatorValues);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, detailedCompanyName, bank, logo, createdAt, shareholders, country, numberOfAgencies, PDMCredits, PDMDepots, indicatorValues);
        result = 31 * result + Arrays.hashCode(avatar);
        return result;
    }
}
