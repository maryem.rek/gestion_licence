package com.gestion.domaine.rest;

import com.gestion.domaine.model.Topic;
import com.gestion.domaine.services.TopicService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/topic")
public class TopicRest {

    @Autowired
    private TopicService topicService;


    @PostMapping()
    @ApiOperation(value = "Create a new  topic, return topic if success else return null")
    public ResponseEntity<?> create(@RequestBody Topic topic){
        Topic topicDB = topicService.create(topic);
        return  new ResponseEntity<>(
                topicDB,topicDB!=null ?
                HttpStatus.CREATED : HttpStatus.BAD_REQUEST);
    }

    @PutMapping()
    @ApiOperation(value = "Update  topic, return topic if success else return null")
    public ResponseEntity<?> update(@RequestBody Topic topic){
        Topic topicDB = topicService.update(topic);
        return  new ResponseEntity<>(
                topicDB,topicDB!=null ?
                HttpStatus.ACCEPTED: HttpStatus.BAD_REQUEST);
    }

    @GetMapping()
    @ApiOperation(value = "Get List Topic")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(
                topicService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete topic by id")
    public ResponseEntity<?> delete(@PathVariable("id") Long id){
        return new ResponseEntity<>(topicService.delete(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ApiOperation(value = "Delete topic")
    public ResponseEntity<?> delete(@RequestBody Topic topic){
        return new ResponseEntity<>(topicService.delete(topic), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "get topic by id")
    public ResponseEntity<?> findById(@PathVariable("id") Long id){
        Topic topic = topicService.findById(id);
        return new ResponseEntity<>(topic,topic!=null? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

}
