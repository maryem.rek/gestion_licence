package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.enums.CurrencyRate;
import com.gestion.domaine.model.Indicator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IndicatorVO {

    private Long id;
    private String name;
    private String variance;
    private String formula;
    private Boolean calculable;
    private Boolean convertible;
    private CurrencyRate currencyRate;
    private Boolean displayedOnAdHocFile;
    private IndicatorVO parent;
    private FamilyVO familyVo;

    public IndicatorVO() {
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVariance() {
        return variance;
    }

    public void setVariance(String variance) {
        this.variance = variance;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public Boolean getCalculable() {
        return calculable;
    }

    public void setCalculable(Boolean calculable) {
        this.calculable = calculable;
    }

    public Boolean getConvertible() {
        return convertible;
    }

    public void setConvertible(Boolean convertible) {
        this.convertible = convertible;
    }

    public CurrencyRate getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(CurrencyRate currencyRate) {
        this.currencyRate = currencyRate;
    }

    public Boolean getDisplayedOnAdHocFile() {
        return displayedOnAdHocFile;
    }

    public void setDisplayedOnAdHocFile(Boolean displayedOnAdHocFile) {
        this.displayedOnAdHocFile = displayedOnAdHocFile;
    }


    public FamilyVO getFamily() {
        return familyVo;
    }

    public void setFamily(FamilyVO familyVo) {
        this.familyVo = familyVo;
    }

    public IndicatorVO getParent() {
        return parent;
    }

    public void setParent(IndicatorVO parent) {
        this.parent = parent;
    }

    public List<IndicatorVO> indicatorVOS(List<Indicator> indicators) {
        List<IndicatorVO> list = new ArrayList<>();
        for ( int i =0; i<indicators.size();i++){
            list.add(new IndicatorVO(indicators.get(i)));
        }
        return list;
    }

    public IndicatorVO(Indicator indicator){
        if (indicator!=null){
            this.id = indicator.getId();
            this.name = indicator.getName();
            this.variance = indicator.getVariance();
            this.formula = indicator.getFormula();
            this.calculable = indicator.getCalculable();
            this.convertible = indicator.getConvertible();
            this.currencyRate = indicator.getCurrencyRate();
            this.displayedOnAdHocFile = indicator.getDisplayedOnAdHocFile();
            this.familyVo =  new FamilyVO(indicator.getFamily());
            this.parent = new IndicatorVO(indicator.getParent());
        }
    }


}
