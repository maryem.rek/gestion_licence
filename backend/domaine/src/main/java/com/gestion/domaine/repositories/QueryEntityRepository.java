package com.gestion.domaine.repositories;


import com.gestion.domaine.model.QueryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface QueryEntityRepository extends JpaRepository<QueryEntity,Long> {

    Optional<QueryEntity> findByName(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM query_entity where owner_id =  :id or id in (SELECT query_entity_id from query_entity_shared_with WHERE shared_with_id= :id)")
    List<QueryEntity> getQueryEntitiesIwroteOrShatedWithMe(@Param("id") Long id);
}
