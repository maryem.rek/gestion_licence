package com.gestion.domaine.services;

import com.gestion.domaine.model.Indicator;
import com.gestion.domaine.repositories.IndicatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IndicatorService {

    @Autowired
    private IndicatorRepository indicatorRepository;

    /**
     * Create Indicator : Name should be unique
     * @param indicator
     * @return Indicator | null
     */
    public Indicator create(Indicator indicator){
        return (indicatorRepository.findByName(indicator.getName()).isPresent())?
                null:indicatorRepository.save(indicator);
    }

    /**
     * Update Indicator : Name should be unique|present, Id should be present
     * @param indicator
     * @return Indicator if updated else null
     */
    public Indicator update(Indicator indicator){

        if(indicator.getId()!=null && indicator.getName()!=null)
        {
            Optional<Indicator> indicatorValueById   = indicatorRepository.findById(indicator.getId());
            Optional<Indicator> indicatorValueByName = indicatorRepository.findByName(indicator.getName());

            // !(exist && id!=id) => unique
            boolean isNameUnique =
                    !(indicatorValueByName.isPresent() && !indicatorValueById.get().getId().equals(indicatorValueByName.get().getId()));
            return (isNameUnique)?
                    indicatorRepository.saveAndFlush(indicator):null;
        }
        return null;
    }


    /**
     * Get Indicator by id
     * @param id
     * @return Indicator if updated else null
     */
    public Indicator findById(Long id){
        Optional<Indicator> indicator1 = indicatorRepository.findById(id);
        return indicator1.orElse(null);
    }

    /**
     * Get Indicator by name
     * @param name
     * @return Indicator if updated else null
     */
    public Indicator findByName(String name){
        Optional<Indicator> indicator1 = indicatorRepository.findByName(name);
        return indicator1.orElse(null);
    }

    /**
     * Get all indicators
     * @return List<Indicator>
     */
    public List<Indicator> findAll(){
        return indicatorRepository.findAll();
    }

    /**
     * Get Page of indicators
     * @param page
     * @param size
     * @return Page<Indicator>
     */
    public Page<Indicator> findPages(int page, int size)
    {
        return indicatorRepository.findAll(PageRequest.of(page,size));
    }

    /**
     * Delete Indicator by id
     * @param id
     * @return isDeleted
     */
    public Boolean delete(Long id){
        try{
            indicatorRepository.deleteById(id);
        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }
        return !indicatorRepository.findById(id).isPresent();
    }

    /**
     * Delete Indicator
     * @param indicator
     * @return isDeleted
     */
    public Boolean delete(Indicator indicator){
        try{
            if (indicator.getId()!=null){
                indicatorRepository.delete(indicator);
                return !indicatorRepository.findById(indicator.getId()).isPresent();
            }

        }catch(EmptyResultDataAccessException e)
        {
            return true;
        }

        return false;
    }

    /**
     *  get Indicator Children by the parent id
     * @param parentId
     * @return
     */
    public List<Indicator> findByParentId(Long parentId) {
        return indicatorRepository.findByParentId(parentId);
    }

    public Optional<Indicator> findByNameLike(String name){
        return this.indicatorRepository.findByNameLike(name);
    }
}
