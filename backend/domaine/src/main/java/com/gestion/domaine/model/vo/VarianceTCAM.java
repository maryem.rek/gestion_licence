package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.Indicator;

import java.math.BigDecimal;
import java.util.List;

public class VarianceTCAM {

    private List<Variance> variance;
    private Indicator indicator;
    private BigDecimal TCAM;
    private int endYear;
    private int endMonth;
    private int startYear;
    private int startMonth;
    private BigDecimal oldValue;
    private BigDecimal newValue;

    public VarianceTCAM() {
    }

    public List<Variance> getVariance() {
        return variance;
    }

    public void setVariance(List<Variance> variance) {
        this.variance = variance;
    }

    public BigDecimal getTCAM() {
        return TCAM;
    }

    public void setTCAM(BigDecimal TCAM) {
        this.TCAM = TCAM;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public Indicator getIndicator() {
        return indicator;
    }

    public void setIndicator(Indicator indicator) {
        this.indicator = indicator;
    }

	public BigDecimal getOldValue() {
		return oldValue;
	}

	public void setOldValue(BigDecimal oldValue) {
		this.oldValue = oldValue;
	}

	public BigDecimal getNewValue() {
		return newValue;
	}

	public void setNewValue(BigDecimal newValue) {
		this.newValue = newValue;
	}
    
    
}
