package com.gestion.domaine.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Subsidiary implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Lob
    private String  detailedCompanyName;

    private String bank;

    private String logo;

    private Date createdAt;

    @Lob
    private String shareholders;

    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;

    private Long numberOfAgencies;

    private String PDMCredits;

    private String PDMDepots;

    @OneToMany(mappedBy = "subsidiary",cascade=CascadeType.ALL)//, orphanRemoval=true)
    private List<IndicatorValue> indicatorValues;

    @ManyToMany(mappedBy = "subsidiaries",cascade=CascadeType.ALL)
    private List<QueryEntity> queryEntities;


    public Subsidiary() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetailedCompanyName() {
        return detailedCompanyName;
    }

    public void setDetailedCompanyName(String detailedCompanyName) {
        this.detailedCompanyName = detailedCompanyName;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getShareholders() {
        return shareholders;
    }

    public void setShareholders(String shareholders) {
        this.shareholders = shareholders;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Long getNumberOfAgencies() {
        return numberOfAgencies;
    }

    public void setNumberOfAgencies(Long numberOfAgencies) {
        this.numberOfAgencies = numberOfAgencies;
    }

    public String getPDMCredits() {
        return PDMCredits;
    }

    public void setPDMCredits(String PDMCredits) {
        this.PDMCredits = PDMCredits;
    }

    public String getPDMDepots() {
        return PDMDepots;
    }

    public void setPDMDepots(String PDMDepots) {
        this.PDMDepots = PDMDepots;
    }

    @JsonIgnore
    public List<IndicatorValue> getIndicatorValues() {
        return indicatorValues;
    }

    @JsonSetter
    public void setIndicatorValues(List<IndicatorValue> indicatorValues) {
        this.indicatorValues = indicatorValues;
    }

    @JsonIgnore
    public List<QueryEntity> getQueryEntities() {
        return queryEntities;
    }

    @JsonSetter
    public void setQueryEntities(List<QueryEntity> queryEntities) {
        this.queryEntities = queryEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subsidiary that = (Subsidiary) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(bank, that.bank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bank);
    }
}
