package com.gestion.domaine.model.vo;

import com.gestion.domaine.model.Currency;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CurrencyVO {

    private Long id;
    private String name;
    private String symbole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbole() {
        return symbole;
    }

    public void setSymbole(String symbole) {
        this.symbole = symbole;
    }

    public CurrencyVO(){}

    public CurrencyVO(Currency currency) {
        if (currency!=null){
            this.id = currency.getId();
            this.name = currency.getName();
            this.symbole = currency.getSymbole();
        }
    }

    public List<CurrencyVO> CurrencyVOS(List<Currency> currencies) {
        List<CurrencyVO> list = new ArrayList<>();
        for (Currency currency : currencies) {
            list.add(new CurrencyVO(currency));
        }
        return list;
    }



}
