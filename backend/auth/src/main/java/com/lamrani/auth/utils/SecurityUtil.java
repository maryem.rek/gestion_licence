package com.lamrani.auth.utils;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.lamrani.auth.model.Authority;
import com.lamrani.auth.model.AuthorityName;
import com.lamrani.auth.model.User;

/**
 * <h1>Security helpers</h1>
 *
 * @author  Mohamed Lamrani Alaoui
 * @author Zemouri Badr
 * @version 1.0
 * @since   2018-05-16
 */
@Component("securityService")
public class SecurityUtil {
	
	/**
	 * If the current user has the specified roles
	 * @param permissions
	 * @return boolean
	 */
	public boolean hasRole( AuthorityName...permissions){
		// loop over each submitted role and validate the user has at least one
		Collection<? extends GrantedAuthority> userAuthorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for( AuthorityName permission : permissions){
			if( userAuthorities.contains( new SimpleGrantedAuthority(permission.name())))
				return true;
		}
		// no matching role found
		return false;
	}
	
	/**
	 * If the user has the specified roles
	 * @param user
	 * @param permissions
	 * @return boolean
	 */
	public boolean userHasRole(User user, AuthorityName...permissions){
		List<Authority> userPermissions = user.getAuthorities();
		if(userPermissions!=null && permissions!=null) {
			for( AuthorityName permission : permissions){
				for(Authority userPermission : userPermissions){
					if(userPermission.getName().toString().equals(permission.toString()))
						return true;
				}
			}
		}
		// no matching role found
		return false;
	}
}
