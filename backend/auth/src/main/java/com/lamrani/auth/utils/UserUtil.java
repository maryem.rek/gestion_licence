package com.lamrani.auth.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.lamrani.auth.config.JwtTokenUtil;
import com.lamrani.auth.exception.UnauthorizedException;
import com.lamrani.auth.model.User;
import com.lamrani.auth.repositories.UserRepository;

/**
 * <h1>User helpers</h1>
 *
 * @author  Mohamed Lamrani Alaoui
 * @author Zemouri Badr
 * @version 1.0
 * @since   2018-05-16
 */
@Component
public class UserUtil {
	
	@Value("${jwt.header}")
	private String tokenHeader;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	/**
	 * Get current user
	 * @return User
	 */
	public User getCurrentUser() {
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
		        .getRequest();
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);

		User user = userRepository.findByEmail(username);
		if(user==null) {
			throw new UnauthorizedException();
		}
		return user;
	}
	
}
