package com.lamrani.auth.service.interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.lamrani.auth.model.User;

/**
* <h1>User admin management interface</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
public interface UserAdminServiceInterface {
	
	/**
	 * Get all users excluding SUPER_ADMINs and images
	 * @return List<User>
	 */
	List<User> findAll();
	
	/**
	 * Add new user using global info
	 * @param input new user
	 * @return ResponseEntity with following status: <br/>
	 * 400: Bad or missing data <br/>
	 * 403: Super admin found <br/>
	 * 409: Email already exist <br/>
	 * 200: User
	 */
	ResponseEntity<?> addUser(User input);

	/**
	 * Edit user by id using global info excluding image
	 * @param input user to be edited containing the id of the user to change
	 * @return ResponseEntity with following status: <br/>
	 * 403: Super admin found <br/>
	 * 404: User not found <br/>
	 * 409: Email already exist <br/>
	 * 200: User
	 */
	ResponseEntity<?> editUser(User input);

	/**
	 * Delete user by id
	 * @param id
	 * @return ResponseEntity with following status: <br/>
	 * 403: Super admin found <br/>
	 * 404: User not found <br/>
	 * 204: Success
	 */
	ResponseEntity<?> delete(Long id);
	
	/**
	 * enable user by id
	 * @param id
	 * @param enable true or false
	 * @return ResponseEntity with following status: <br/> 
	 * 403: Super admin found <br/>
	 * 404: User not found <br/>
	 * 204: Success
	 */
	ResponseEntity<?> enable(Long id, boolean enable);

	/**
	 * find user by id excluding image
	 * @param id
	 * @return ResponseEntity with following status: <br/>
	 * 403: Super admin found <br/>
	 * 404: User not found <br/>
	 * 200: User
	 */
	ResponseEntity<?> findOne(Long id);

}
