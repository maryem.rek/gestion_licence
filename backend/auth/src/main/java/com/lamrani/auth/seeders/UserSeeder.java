package com.lamrani.auth.seeders;

import com.lamrani.auth.model.Authority;
import com.lamrani.auth.model.AuthorityName;
import com.lamrani.auth.model.User;
import com.lamrani.auth.repositories.AuthorityRepository;
import com.lamrani.auth.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
* <h1>User roles and superAdmin user seeder</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
@Component
public class UserSeeder{

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
	private AuthorityRepository authorityRepository;

    public void run(){
    	
    	/*Insert Roles*/
    	List<Authority> authoritiesToStore= new LinkedList<Authority>();
    	
    	authoritiesToStore.add(new Authority(1L, AuthorityName.ROLE_SUPER_ADMIN));
    	authoritiesToStore.add(new Authority(2L, AuthorityName.ROLE_ADMIN));
    	authoritiesToStore.add(new Authority(3L, AuthorityName.ROLE_USER));
    	
    	List<Authority> authoritiesFromDb= authorityRepository.findAll();
    	for (Authority authorityToStore : authoritiesToStore) {
    		boolean found= false;
    		for(Authority authorityFromDb : authoritiesFromDb){
    			if(authorityToStore.getName().toString().equals(authorityFromDb.getName().toString())) {
    				found= true;
    			}
    		}
    		if(!found) {
    			authorityRepository.save(authorityToStore);
    		}
		}
    	
    	/*Insert User*/
        User user =  userRepository.findByEmail("admin@admin.com");

        if(user==null){
            List<Authority> authorities = new ArrayList<>();
            authorities.add(authorityRepository.findByName(AuthorityName.ROLE_SUPER_ADMIN));

            user = new User();
            user.setAuthorities(authorities);
            user.setEmail("admin@admin.com");
            user.setFirstname("admin");
            user.setLastname("admin");
            user.setEnabled(true);
            user.setLastPasswordResetDate(new Date());
            user.setPassword(encodePassword("admin1"));
            userRepository.save(user);
        }
    }

    private String encodePassword(String password){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}
