package com.lamrani.auth.repositories;


import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lamrani.auth.model.AuthorityName;
import com.lamrani.auth.model.User;


/**
* <h1>User repository</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);

	User findByToken(String token);

	List<User> findByAuthorities_Name(AuthorityName name);

	@Query("select u from User u where u.id IN (SELECT u FROM User u LEFT JOIN u.authorities b WHERE b.name!='ROLE_SUPER_ADMIN' or b IS NULL)")
	List<User> findNotAdmin(Collection<AuthorityName> authorites);

}
