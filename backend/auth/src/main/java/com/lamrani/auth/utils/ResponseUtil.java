package com.lamrani.auth.utils;


/**
 * <h1>Response generator</h1>
 *
 * @author  Mohamed Lamrani Alaoui
 * @author Zemouri Badr
 * @version 1.0
 * @since   2018-05-16
 */
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseUtil {
	
	public static ResponseEntity<?> noContent(){
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	public static <T> ResponseEntity<?> ok(T value){
		return new ResponseEntity<>(value, new HttpHeaders(), HttpStatus.OK);
	}
	
}
