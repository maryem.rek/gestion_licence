package com.lamrani.auth.exception;

import javax.xml.ws.http.HTTPException;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends HTTPException{
	private static final long serialVersionUID = -7192057116702909616L;

	public UnauthorizedException() {
        super(HttpStatus.UNAUTHORIZED.value());
    }
}