package com.lamrani.auth.seeders;

import com.lamrani.auth.utils.FilesUtil;
import org.springframework.stereotype.Component;

import java.io.File;

/**
* <h1>Needed folders seeder</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
@Component
public class FoldersSeeder{

    public void run(){
    	/*Profil picture folder*/
    	new File(FilesUtil.getProfilUrl()).mkdirs();
    	new File(FilesUtil.getProfilUrl()).mkdirs();

    }
}
