package com.lamrani.auth.mapping;

/**
* <h1>Error types</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
public enum ErrorName {
	USER_TOKEN_REQUIRED,
	USER_PASSWORD_REQUIRED,
	
	USER_INVALID_TOKEN,
	USER_INVALID_TOKEN_TIME,
	USER_INVALID_PASSWORD,
	USER_INVALID_CREDENTIALS,
	
	USER_NOT_FOUND,
	USER_ALREADY_EXIST,
	USER_DISABLED,
	
	USER_FORBIDDEN_SUPER_ADMIN
}
