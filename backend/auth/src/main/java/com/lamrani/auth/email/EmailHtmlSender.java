package com.lamrani.auth.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by aboullaite on 3/2/17.
 */

@Service
public class EmailHtmlSender {

    @Value("${spring.mail.static-url}")
    private String staticURL;
    
    @Value("${app.name}")
    private String appName;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private TemplateEngine templateEngine;
    @Async
    public void send(String to, String subject, String templateName, Context context) {
        context.setVariable("staticurl", staticURL);
        context.setVariable("appname", appName);
        String body = templateEngine.process(templateName, context);
        emailSender.sendHtml(to, subject, body);
    }
}
