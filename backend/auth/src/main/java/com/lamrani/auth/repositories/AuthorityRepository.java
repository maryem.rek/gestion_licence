package com.lamrani.auth.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lamrani.auth.model.Authority;
import com.lamrani.auth.model.AuthorityName;

/**
* <h1>Authority repository</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

	List<Authority> findByNameIn(Collection<AuthorityName> authorities);
	Authority findByName(AuthorityName authority);

}
