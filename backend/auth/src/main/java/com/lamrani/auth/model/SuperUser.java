package com.lamrani.auth.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.lamrani.auth.utils.FilesUtil;

@MappedSuperclass
@JsonIgnoreProperties({"token"})
public class SuperUser {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

	@Column(name = "email", length = 150,unique = true)
    @NotNull
    @Size(min = 3, max = 150,  message="email should be between 3 and 150 caracters")
    @Email
    private String email;

    @Column(name = "password", length = 200)
    @Size(min = 3, max = 100,  message="password should be between 3 and 100 caracters")
    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;

    @Column(name = "firstname", length = 50)
    @NotNull
    @Size(min = 1, max = 50, message="firstname should be between 1 and 50 caracters")
    private String firstname;

    @Column(name = "lastname", length = 50)
    @NotNull
    @Size(min = 1, max = 50, message="lastname should be between 1 and 50 caracters")
    private String lastname;
    
    
    @Column(name = "image", length = 150)
    private String image;

    @Column(name = "enabled", nullable= true)
    private Boolean enabled;

    @Column(name = "lastpasswordresetdate", nullable= true)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty(access = Access.READ_ONLY)
    private Date lastPasswordResetDate;
    
    @Column(name = "token", length = 301)
    private String token;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "USER_AUTHORITY",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "authority_id", referencedColumnName = "ID")})
    private List<Authority> authorities;
    
    @Column(updatable= false, insertable= false)
    private byte[] avatar;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public byte[] getAvatar() {
		if(this.image!=null){
			String filePath = FilesUtil.getProfilUrl()+this.image;
			byte[] bFile;
			try {
				bFile = Files.readAllBytes(new File(filePath).toPath());
				return bFile;
			} catch (IOException e) {
				return null;
			}
		}
		return null;
	}
    
    public SuperUser() {
    }
    
    

}
