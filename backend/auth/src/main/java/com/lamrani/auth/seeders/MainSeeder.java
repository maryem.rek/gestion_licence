package com.lamrani.auth.seeders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


/**
* <h1>Seeders launcher</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
@Component
public class MainSeeder implements CommandLineRunner {

    @Autowired
    UserSeeder userSeeder;

    @Autowired
    FoldersSeeder foldersSeeder;

    @Override
    public void run(String... args) throws Exception {
        userSeeder.run();
        foldersSeeder.run();
    }
}
