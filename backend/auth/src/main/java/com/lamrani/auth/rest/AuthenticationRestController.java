package com.lamrani.auth.rest;

import java.util.Date;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lamrani.auth.config.JwtAuthenticationRequest;
import com.lamrani.auth.config.JwtTokenUtil;
import com.lamrani.auth.config.JwtUser;
import com.lamrani.auth.exception.AuthenticationException;
import com.lamrani.auth.mapping.ErrorName;
import com.lamrani.auth.service.JwtAuthenticationResponse;
import com.lamrani.auth.service.UserService;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin("*")
public class AuthenticationRestController {

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	UserService userService;

	@RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
	@ApiOperation(value = "authenticate using header authentication, return token or status: 401 with message: disabled, bad credentials")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {
		// Perform the security
		authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
		// Reload password post-security so we can generate token
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
		final String token = jwtTokenUtil.generateToken(userDetails);
		// Return the token
		return ResponseEntity.ok(new JwtAuthenticationResponse(token));
	}

	@RequestMapping(value = "/auth/test", method = RequestMethod.GET)
	public ResponseEntity<?> test() {
		System.out.println(new Date());
		return null;
	}
	
	@RequestMapping(value = "${jwt.route.authentication.refresh}", method = RequestMethod.GET)
	public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

		if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
			String refreshedToken = jwtTokenUtil.refreshToken(token);
			return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@ExceptionHandler({AuthenticationException.class})
	public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
	}

	/**
	 * Authenticates the user. If something is wrong, an {@link AuthenticationException} will be thrown
	 */
	private void authenticate(String username, String password) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new AuthenticationException(ErrorName.USER_DISABLED.toString(), e);
		} catch (BadCredentialsException e) {
			throw new AuthenticationException(ErrorName.USER_INVALID_CREDENTIALS.toString(), e);
		}
	}

	@RequestMapping(value = "/auth/forget", method = RequestMethod.POST)
	@ApiOperation(value = "Forget password, return 204 or status: 404")
	public ResponseEntity<?> forgetPassword(HttpServletRequest request, @RequestParam("email") String email) {
		return userService.forgetPassword(email);
	}
	
	@RequestMapping(value = "/auth/reset", method = RequestMethod.POST)
	@ApiOperation(value = "Reset password, return 204 or status: 400, 404")
	public ResponseEntity<?> resetPassword(HttpServletRequest request, @RequestParam("token") String token, @RequestParam("password") String password) {
		return userService.resetPassword(token, password);
	}
	
}
