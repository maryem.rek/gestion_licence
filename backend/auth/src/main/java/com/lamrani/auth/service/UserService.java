package com.lamrani.auth.service;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import com.lamrani.auth.config.JwtTokenUtil;
import com.lamrani.auth.config.JwtUser;
import com.lamrani.auth.email.EmailHtmlSender;
import com.lamrani.auth.exception.UnauthorizedException;
import com.lamrani.auth.mapping.ErrorName;
import com.lamrani.auth.model.User;
import com.lamrani.auth.repositories.UserRepository;
import com.lamrani.auth.service.interfaces.UserServiceInterface;
import com.lamrani.auth.utils.FilesUtil;
import com.lamrani.auth.utils.MyError;
import com.lamrani.auth.utils.RandomString;
import com.lamrani.auth.utils.ResponseUtil;

/**
* <h1>User management</h1>
*
* @author  Mohamed Lamrani Alaoui
* @author Zemouri Badr
* @version 1.0
* @since   2018-05-16
*/
@Service
public class UserService implements UserServiceInterface {

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private EmailHtmlSender emailHtmlSender;
	
	@Value("${app.email-object-forget}")
    private String appObjectName;

	@Override
	public ResponseEntity<?> changePassword(User user, String password) {
		if(password!=null) {
			if(password.length()>5 && password.length()<101) {
				BCryptPasswordEncoder bc= new BCryptPasswordEncoder();
				String t= bc.encode(password);
				user.setPassword(t);
				return ResponseUtil.ok(userRepository.save(user));
			}
		}
		return MyError.getBadRequest(ErrorName.USER_INVALID_PASSWORD.toString());
	}

	@Override
	public ResponseEntity<?> changeInfo(User user, User newValues) {
		if(!user.getEmail().equalsIgnoreCase(newValues.getEmail())) {
			User oldUser= userRepository.findByEmail(newValues.getEmail());
			if(oldUser!=null) {
				return MyError.getConflict(ErrorName.USER_ALREADY_EXIST.toString());
			}
		}
		user.setEmail(newValues.getEmail());
		user.setFirstname(newValues.getFirstname());
		user.setLastname(newValues.getLastname());
		user= userRepository.save(user);
		return ResponseUtil.ok(user);
	}

	@Override
	public ResponseEntity<?> changeAvatar(User user, MultipartFile file) {
		String name= user.getImage();
		FilesUtil.deleteFile(name);
		user.setImage(null);
		try {
			String newName= user.getId()+"_"+(new Date()).getTime()+'.'+FilesUtil.getFileExtension(file);
			FilesUtil.moveFile(file, newName);
			user.setImage(newName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ResponseUtil.ok(userRepository.save(user));
	}

	@Override
	public ResponseEntity<?> deleteAvatar(User user) {
		String name= user.getImage();
		FilesUtil.deleteFile(name);
		user.setImage(null);
		return ResponseUtil.ok(userRepository.save(user));
	}

	@Override
	public JwtUser getAuthenticatedJwtUser(HttpServletRequest request) {
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
		return user;
	}
	
	@Override
	public User getAuthenticatedUser() {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String token = request.getHeader(tokenHeader);
		String username = jwtTokenUtil.getUsernameFromToken(token);

		User user = userRepository.findByEmail(username);
		if(user==null) {
			throw new UnauthorizedException();
		}
		return user;
	}
	
	@Override
	public ResponseEntity<?> forgetPassword(String email) {
		User user= userRepository.findByEmail(email);
		if(user!=null) {
			String token = user.getId()+ "_" + (new RandomString(100)).nextString();
			user.setToken(token);
			user.setLastPasswordResetDate(new Date());
			userRepository.save(user);
			final Context ctx = new Context();
			ctx.setVariable("token", token);
			ctx.setVariable("name", user.getFirstname()+" "+user.getLastname());
			emailHtmlSender.send(user.getEmail(), appObjectName, "email/email-password", ctx);
			return ResponseUtil.noContent();
		}else {
			return MyError.getNotFound(ErrorName.USER_NOT_FOUND.toString());
		}
	}
	
	@Override
	public ResponseEntity<?> resetPassword(String token, String password) {
		if(token!=null) {
			if(!token.equals("")) {
				User user= userRepository.findByToken(token);
				if(user!=null) {
					if(user.getLastPasswordResetDate()!=null) {
						long diff = ((new Date()).getTime() - user.getLastPasswordResetDate().getTime())/(1000 * 60 * 60 * 24);
						if(diff<3) {
							if(password!=null) {
								if(password.length()>5 && password.length()<101) {
									BCryptPasswordEncoder bc= new BCryptPasswordEncoder();
									String t= bc.encode(password);
									user.setPassword(t);
									user.setLastPasswordResetDate(null);
									user.setToken(null);
									userRepository.save(user);
									return new ResponseEntity<>(HttpStatus.NO_CONTENT);
								}else {
									return MyError.getBadRequest(ErrorName.USER_INVALID_PASSWORD.toString());
								}
							}else {
								return MyError.getBadRequest(ErrorName.USER_PASSWORD_REQUIRED.toString());
							}
						}else {
							return MyError.getBadRequest(ErrorName.USER_INVALID_TOKEN_TIME.toString());
						}
					}else {
						return MyError.getBadRequest(ErrorName.USER_INVALID_TOKEN.toString());
					}
				}else {
					return MyError.getNotFound(ErrorName.USER_NOT_FOUND.toString());
				}
			}
		}
		return MyError.getBadRequest(ErrorName.USER_TOKEN_REQUIRED.toString());
	}

	// for Security Usage to not be touched
	@Override
	public User loadUserByEmail(String email) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(email);
		return user;
	}

}
