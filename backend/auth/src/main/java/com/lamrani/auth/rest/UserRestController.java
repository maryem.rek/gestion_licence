package com.lamrani.auth.rest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lamrani.auth.model.User;
import com.lamrani.auth.service.UserService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/user")
@CrossOrigin("*")
public class UserRestController {
	
	@Autowired
	UserService userService;
	
    @RequestMapping(value="/current" ,method = RequestMethod.GET)
    @ApiOperation(value = "Get current user info, return user")
	public ResponseEntity<?> getAuthenticatedUser() {
        return new ResponseEntity<>(userService.getAuthenticatedUser(), new HttpHeaders(), HttpStatus.OK);
	}
    
	@RequestMapping(value="/password", method = RequestMethod.POST)
	@ApiOperation(value = "Change password, return user or status: 400")
	public ResponseEntity<?> changePassword(@RequestBody String password, HttpServletRequest request) {
        return userService.changePassword(userService.getAuthenticatedUser(), password);
	}
	
	@RequestMapping(value="/avatar" ,method = RequestMethod.POST)
	@ApiOperation(value = "Change avatar, return user")
	public ResponseEntity<?> changeAvatar(@RequestParam("file") MultipartFile file) {
        return userService.changeAvatar(userService.getAuthenticatedUser(), file);
	}
	
	@RequestMapping(value="/avatar" ,method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete avatar, return user")
	public ResponseEntity<?> deleteAvatar() {
        return userService.deleteAvatar(userService.getAuthenticatedUser());
	}
	
	@RequestMapping(value="/info" ,method = RequestMethod.POST)
	@ApiOperation(value = "Change informations, return user or status: 400, 409")
	public ResponseEntity<?> changeInfo(@Valid @RequestBody User input, Errors errors) {
		if (errors.hasErrors()) {
			return new ResponseEntity<>(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
		}
        return userService.changeInfo(userService.getAuthenticatedUser(), input);
	}
}
