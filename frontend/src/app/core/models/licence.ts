export interface Licence {
        id: string;
        nom: string;
        nomClient: string;
        dataea: string;
        dateEx: string;
        status: boolean;
    }
