import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { UserStoreService } from '../../store/user-store.service';
import { hasAuthority } from '../../helpers/authority-helpers';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { LicenceService } from '../../service/licence.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isLoaded: boolean= false;
  user:User= null;
  list:any[];
  dely:number=3;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches)
  );
  
  constructor(private breakpointObserver: BreakpointObserver,private licenceservice:LicenceService, private userStore: UserStoreService ,private userService: UserService, private route: Router) { }

  ngOnInit() {
    this.userService.populate().subscribe(
      (user: User)=>{
        this.userStore.updateUser(user);
        this.user= user;
        this.isLoaded= true;
      },
      (error)=>{
        // console.log(error);
      }
    );
    this.userStore.userMessage.subscribe(
      (user: User) => {
        if (user) {
          this.user = user;
          this.isLoaded = true;
        }
      }
    );
  }

  hasAuth(user : User, authority: any[]): boolean{
    return hasAuthority(user,authority);
  }

  getDate(date:any){  
    return ( moment(date).diff(moment(),'days')>=0  && moment(date).diff(moment(),'days')<=this.dely );
    }
  
    getCount(){
    return this.list.filter(e=>( moment(e.dateEx).diff(moment(),'days')>=0  && moment(e.dateEx).diff(moment(),'days')<=this.dely )).length;
    }
  }
  

