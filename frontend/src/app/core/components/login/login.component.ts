import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(private authService: AuthService, private router: Router, private toastr: ToastrService) { 
    this.authService.destroyToken();
    this.signUpForm = new FormGroup({
      'username': new FormControl('admin@admin.com', [Validators.required, Validators.email, Validators.maxLength(150)]),
      'password': new FormControl('admin1', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
    });
  }

  ngOnInit() {}

  onSubmit() {
    this.authService.login(this.signUpForm.value.username, this.signUpForm.value.password)
      .subscribe(
        (data: any) => { this.router.navigate(['/']) },
        (error) => {
          if (error.error instanceof ErrorEvent || error.status == 0) {
            this.toastr.error('erreur de réseau s\'est produite', null, {
              'timeOut': 0
            });
            console.error('erreur de réseau s\'est produite');
          } else {
            this.toastr.error(error.error);
          }
        }
      );
    ;
  }

}
