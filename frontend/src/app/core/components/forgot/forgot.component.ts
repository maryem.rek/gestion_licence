import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(private authService: AuthService, private toastr: ToastrService) { 
    this.signUpForm = new FormGroup({
      'username': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(150)]),
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.forget(this.signUpForm.value.username)
      .subscribe(
        (data: any) => { this.toastr.success('Merci de consulter votre email') },
        (error) => {
          if (error.error instanceof ErrorEvent || error.status == 0) {
            this.toastr.error('erreur de réseau s\'est produite', null, {
              'timeOut': 0
            });
            console.error('erreur de réseau s\'est produite');
          } else {
            this.toastr.error(error.error.error.message);
          }
        }
      );
    ;
  }

}
