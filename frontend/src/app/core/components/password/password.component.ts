import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
  signUpForm: FormGroup;
  token: string= null;

  constructor(private authService: AuthService, private toastr: ToastrService, private route: ActivatedRoute) { 
    this.signUpForm = new FormGroup({
      'password': new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
    });
  }

  ngOnInit() {
    this.route.params.subscribe((data) => {
      this.token = data.token;
    });
  }

  onSubmit() {
    this.authService.reset(this.signUpForm.value.password,this.token)
      .subscribe(
        (data: any) => { this.toastr.success('Mot de passe changé avec succées') },
        (error) => {
          if (error.error instanceof ErrorEvent || error.status == 0) {
            this.toastr.error('erreur de réseau s\'est produite', null, {
              'timeOut': 0
            });
            console.error('erreur de réseau s\'est produite');
          } else {
            this.toastr.error(error.error.error.message);
          }
        }
      );
    ;
  }

}
