import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-my-modal',
  templateUrl: './my-modal.component.html',
  styleUrls: ['./my-modal.component.css']
})
export class MyModalComponent implements OnInit {
  @ViewChild('mymodal') mymodal: ElementRef;
  @Output() notifyMe: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() closeMe: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input("formEnable") formEnable: boolean= false;
  @Input("title") title: string= '';
  @Input("height") height: string= '350px';
  @Input("width") width: string= '94%';
  @Input("saveText") saveText: string= 'Enregistrer';
  @Input("cancelText") cancelText: string= 'Fermer';
  
  onNotify(){
    this.notifyMe.emit(true);
  }
  
  constructor() { }

  ngOnInit() {
  }

  hideOnClick(event) {
    let target = event.target || event.srcElement || event.currentTarget;
    let idAttr = target.attributes.id;
    if (idAttr) {
      let value = idAttr.nodeValue;
      if(value=="mdl"){
        this.closeModal();
      }
    }
  }
  
  closeModal() {
    this.closeMe.emit(true);
    document.getElementsByTagName("BODY")[0].classList.remove("modal-open");
    this.mymodal.nativeElement.classList.remove("in");
    this.mymodal.nativeElement.classList.remove("show");
  }

  showModal() {
    document.getElementsByTagName("BODY")[0].classList.add("modal-open");
    this.mymodal.nativeElement.classList.add("in");
    this.mymodal.nativeElement.classList.add("show");
  }
}
