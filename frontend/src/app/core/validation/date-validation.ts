import { FormControl, FormGroup } from "@angular/forms";

export function MonthValidation(control: FormControl): { [s: string]: boolean }{
    if(control.value>=1 && control.value<=12){
        return null;
    }
    return { 'monthIsNotValid': true };
}
export function YearValidation(control: FormControl): { [s: string]: boolean }{
    if(control.value>0){
        return null;
    }
    return { 'yearIsNotValid': true };
}
export function DateValidation(control: FormControl): { [s: string]: boolean }{
        return null;
}
export function DateIsGreatThanValidationFromAndTo(control: FormGroup): { [s: string]: boolean }{
    if(control.value.from==null || control.value.to==null){
        return {'DateIsNotGreatThanValid': true};
    }
    if(control.value.from.getTime() > control.value.to.getTime()){
        return {'DateIsNotGreatThanValid': true};
    }
    return null;
}