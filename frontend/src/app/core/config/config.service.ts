import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { UserStoreService } from '../store/user-store.service';
import { ToastrService, ActiveToast } from 'ngx-toastr';
import { ErrorMapping } from '../mapping/ErrorMapping';

@Injectable()
export class ConfigService {
    private refToastr: ActiveToast<any> = null;
    constructor(private route: Router, private userStore: UserStoreService, private toastr: ToastrService) { }

    handleError(error: HttpErrorResponse, show: string = 'all') {
        console.log(error);
        if (show = 'all') {
            if (error.status == 401) {
                this.toastr.clear();
                this.toastr.error('vous n\'êtes pas connecté ', null, {
                    'timeOut': 0
                });
                this.userStore.purgeAuth();
                this.route.navigate(['/auth/login']);
                return new ErrorObservable(error);
            }
        }
        if (error.error instanceof ErrorEvent || error.status == 0) {
            // A client-side or network error occurred. Handle it accordingly.
            //   console.error('An error occurred:', error.error.message);
            if (this.refToastr) {
                this.toastr.clear(this.refToastr.toastId);
            }
            this.refToastr = this.toastr.error('erreur de réseau s\'est produite merci de verifier votre connexion internet', null, {
                'timeOut': 0
            });
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            if (error.status >= 500) {
                if (show == 'all' || show == 'servererror') {
                    this.toastr.error('Internal server error please try again later', null, {
                        'timeOut': 0
                    });
                }
            }
            if (show = 'all') {
                if (error.status == 409) {
                    if (error.error.hasOwnProperty('error') && error.error.error.hasOwnProperty('message')) {
                        this.toastr.info(ErrorMapping(error.error.error.message));
                        return new ErrorObservable(error);
                    } else {
                        this.toastr.error('The data you enter already exist');
                    }
                }
                if (error.status >= 400 && error.status < 500) {
                    if (error.error && error.error.hasOwnProperty('error') && error.error.error.hasOwnProperty('message')) {
                        this.toastr.error(ErrorMapping(error.error.error.message));
                    } else {
                        if (error.status == 404) {
                            this.toastr.error('The resource you looking for was not found');
                        } else if (error.status == 403) {
                            this.toastr.error('You don\'t have permission to execute this action');
                        } else if (error.status == 405) {
                            this.toastr.error('Request method not supported');
                        } else if (error.status == 400) {
                            this.toastr.error('Value already exist, please fill all the data');
                        } else {
                            this.toastr.error('An error occured');
                        }
                    }
                }
            }
            // console.error(
            //     `Backend returned code ${error.status}, ` +
            //     `body was: ${error.error}`);
        }
        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable(error);
    };

}