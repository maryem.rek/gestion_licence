import { Injectable } from '@angular/core';

@Injectable()
export class ParamsLocalService {

  private url: string= '/assets/store-local/';
  constructor() { }
  getUrl(): string{
    return this.url;
  }

}
