import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ParamsService } from '../config/params.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ConfigService } from '../config/config.service';
import { ParamsLocalService } from './params-local.service';
@Injectable()
export class ApiLocalService {
    constructor(private paramsService: ParamsLocalService, private http: HttpClient, private HandleError: ConfigService) { }

    get(url: string, params?: any) {
        return this.http.get<any>(this.paramsService.getUrl() + url)
            .map((data: any) => { return data; })
            .catch(error => {
                return this.HandleError.handleError(error, this.getshowToaster(params));
            })
            ;
    }

    post(url: string, data: any, params?: any) {
        return this.http.post<any>(this.paramsService.getUrl() + url, data)
            .map((data: any) => { return data; })
            .catch(error => {
                return this.HandleError.handleError(error, this.getshowToaster(params));
            })
            ;
    }

    postFile(url: string, formData: FormData, params?: any) {
        let req: HttpHeaders = new HttpHeaders().set('Content-Type', 'multipart/form-data');
        let option = {
            headers: req
        };
        return this.http.post<any>(this.paramsService.getUrl() + url, formData, option)
            .map((data: any) => { return data; })
            .catch(error => {
                return this.HandleError.handleError(error, this.getshowToaster(params));
            })
            ;
    }

    put(url: string, data: any, params?: any) {
        return this.http.put<any>(this.paramsService.getUrl() + url, data)
            .map((data: any) => { return data; })
            .catch(error => {
                return this.HandleError.handleError(error, this.getshowToaster(params));
            })
            ;
    }

    delete(url: string, params?: any) {
        return this.http.delete<any>(this.paramsService.getUrl() + url)
            .map((data: any) => { return data; })
            .catch(error => {
                return this.HandleError.handleError(error, this.getshowToaster(params));
            })
            ;
    }

    private getshowToaster(params?: any) {
        let showToastr: string = 'all';
        if (params != null) {
            if (params.hasOwnProperty('showToastr')) {
                if (params.showToastr) {
                    showToastr = 'servererror';
                } else {
                    showToastr = 'none';
                }
            }
        }
        return showToastr;
    }
}