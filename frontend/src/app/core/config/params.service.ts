import { Injectable } from '@angular/core';

@Injectable()
export class ParamsService {

  private url: string= 'http://localhost:8001/';
  // private url: string= 'https://bdi.peaqock.com:8797/';
  constructor() { }
  getUrl(): string{
    return this.url;
  }

}
