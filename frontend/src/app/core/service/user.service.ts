import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParamsService } from '../config/params.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ConfigService } from '../config/config.service';
import { User } from '../models/user';
import { AuthService } from '../auth/auth.service';
import { UserStoreService } from '../store/user-store.service';
import { Router } from '@angular/router';
import { ApiService } from '../config/api.service';

@Injectable()
export class UserService {

    constructor(private apiService: ApiService, private route: Router, private userStore: UserStoreService, private authService: AuthService, private paramsService: ParamsService, private http: HttpClient, private HandleError: ConfigService) {
    }

    //login purpose
    populate() {
        return this.http.get<Object>(this.paramsService.getUrl() + 'api/user/current')
            .map((data: User) => { this.userStore.updateUser(data); return data; })
            .catch(error => {
                return this.HandleError.handleError(error);
            })
            ;
    }
    logout() {
        this.userStore.purgeAuth();
        this.route.navigate(['/auth/login']);
    }

    //user manipulation service
    getAllUsers() {
        return this.apiService.get('api/admin/user');
    }
    changeAvatar(file: File) {
        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        return this.apiService.postFile('api/user/avatar', formData);
    }
    changeInfo(user: User) {
        return this.apiService.post('api/user/info', user);
    }
    changePassword(password: string) {
        return this.apiService.post('api/user/password', password);
    }
    getOne(id: string) {
        return this.apiService.get('api/admin/user/' + id);
    }
    newUser(user: User) {
        return this.apiService.post('api/admin/user', user);
    }
    editUser(user: User) {
        return this.apiService.put('api/admin/user', user);
    }
    delete(id: string) {
        return this.apiService.delete('api/admin/user/' + id);
    }
    enable(id: string) {
        return this.apiService.get('api/admin/user/' + id + '/enable');
    }
    disable(id: string) {
        return this.apiService.get('api/admin/user/' + id + '/disable');
    }
    getAll(){
        return this.apiService.get('users');
    }
}
