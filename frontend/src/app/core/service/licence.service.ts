import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import { ParamsService } from '../config/params.service';
import { UserStoreService } from '../store/user-store.service';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { Licence } from '../models/licence';
import { ApiService } from '../config/api.service';

const headr={
  headers:new HttpHeaders({
    'Content-Type':'application/json'
  })
}
@Injectable()
export class LicenceService {
  api: 'http://localhost:8001/';
  constructor(private apiService: ApiService, private route: Router,private paramsService: ParamsService, private http: HttpClient, private HandleError: ConfigService) {
  }
  
  
  private data = new BehaviorSubject(<any>[]);
  getData =  this.data.asObservable();

  getAllLicences() {
    return this.apiService.get('licences');
}

 AddLicence(license: any): Observable<any> {
   return this.http.post(this.api+'licence',license ,headr);
 }
 updateLicence(licence: Licence) {
  return this.apiService.put('licence', licence);
}
deleteLicence(id: string) {
  return this.apiService.delete('licence/' + id);
}
newLicence(licence: Licence) {
  return this.apiService.post('licence', licence);
}

getOne(id: string) {
  return this.apiService.get('licences/' + id);

}

notifyData(data){
  this.data.next(data);
}
}
