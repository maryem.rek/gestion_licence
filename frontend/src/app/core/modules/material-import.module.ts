import { NgModule } from '@angular/core';
import {MatMenuModule, MatOptionModule, MatFormFieldModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { MatSortModule } from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatNativeDateModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgSelectModule } from '@ng-select/ng-select';
import {DpDatePickerModule} from 'ng2-date-picker';



@NgModule({
  imports: [MatNativeDateModule, MatDatepickerModule, MatIconModule, MatDialogModule, MatButtonModule, MatSelectModule, MatOptionModule, MatMenuModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCardModule, MatFormFieldModule, NgxMatSelectSearchModule, NgSelectModule, DpDatePickerModule],
  exports: [MatNativeDateModule, MatDatepickerModule, MatIconModule, MatDialogModule, MatButtonModule, MatSelectModule, MatOptionModule, MatMenuModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCardModule, MatFormFieldModule, NgxMatSelectSearchModule, NgSelectModule, DpDatePickerModule],
})
export class      MaterialMenuModule { }
