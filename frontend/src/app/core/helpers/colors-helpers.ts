export function randomColor(number) {
    // let colors = ['#7AB024', '#9A9B9D', '#F2B42B', '#0c9abc', '#FE4A49', '#2AB7CA', '#FED766'];
    // let colors = ['#e75641', '#efc665', '#f88f56', '#f5db9e', '#FE4A49', '#2AB7CA', '#FED766'];
    // let colors = ['#F22901' ,'#D82501' ,'#D85001' ,'#C38901' ,'#C36201' ,'#C3A901' ,'#9B9476'];
    let colors = ['#a20929' ,'#66091f' ,'#666b69' ,'#d3b07e' ,'#c60001' ,'#490303' ,'#95a1a1', '#e2e2e2' ,'#706662' ,'#b6aba2' ,'#ecd7bf' ,'#f0f1f3', '#69110a'];
    for (let i = 0; i < number; i++) {
        let allowed = "ABCDEF0123456789", S = "#";
        while (S.length < 7) {
            S += allowed.charAt(Math.floor((Math.random() * 16) + 1));
        }
        colors.push(S);
    }
    return colors;
}