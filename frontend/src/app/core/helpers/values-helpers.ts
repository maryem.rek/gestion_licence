export function NullToNumer(number) {
    if (number) {
        return number;
    }
    return 0.00;
}

export function NoCommas(val) {
    if (val !== undefined && val !== null) {
        var re = new RegExp(",", 'g');
        let result= val;
        result= result.toString().replace(re, "");
        return result;
    } else {
        return "";
    }
}
export function SpaceCommas(val) {
    if (val !== undefined && val !== null) {
        var re = new RegExp(",", 'g');
        let result= val;
        result= result.toString().replace(re, " ");
        return result;
    } else {
        return "";
    }
}