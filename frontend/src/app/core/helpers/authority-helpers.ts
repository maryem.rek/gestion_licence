import { User } from "../models/user";

export function hasAuthority(user :User, authority: any[]) : boolean{
     if(user.authorities!=null){
        if(user.authorities.length!=0){
            let found= false;
            user.authorities.forEach(element => {
                if(authority.indexOf(element.name)>=0){
                    found= true;
                    return true;
                }
            });
            return found;
        }
    }
    return false;
}