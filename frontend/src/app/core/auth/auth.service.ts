import { Injectable } from '@angular/core';
import { ParamsService } from '../config/params.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

@Injectable()
export class AuthService {
  token: string = "";
  isAuth: boolean = false;

  constructor(private paramsService: ParamsService, private http: HttpClient) { }

  isConnected() {
    if (this.isAuth || localStorage.getItem('token')) {
      this.isAuth = true;
      this.token = localStorage.getItem('token');
      return true;
    }
    return false;
  }

  getJwt() {
    return this.token;
  }

  destroyToken() {
    this.isAuth = false;
    this.token = null;
    localStorage.removeItem('token');
  }

  login(username: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post<Object>(this.paramsService.getUrl() + 'auth', { 'email': username, 'password': password },
      httpOptions
    )
      .map((data: any) => {
        if (data.token) {
          this.isAuth = true;
          this.token = data.token;
          localStorage.setItem('token', this.token);
        }
      });
  }

  forget(username: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: {
        'email': username
      }
    };

    return this.http.post<Object>(this.paramsService.getUrl() + 'auth/forget', {},
      httpOptions
    )
      .map((data: any) => {
        return data;
      })
      .catch(error => {return new ErrorObservable(error)})
  }

  reset(password: string, token: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: { 'password': password, 'token': token }
    };

    return this.http.post<Object>(this.paramsService.getUrl() + 'auth/reset', {},
      httpOptions
    )
      .map((data: any) => {
        return data;
      })
      .catch(error => {return new ErrorObservable(error)})
  }

}
