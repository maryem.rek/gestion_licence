import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParamsService } from '../config/params.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ConfigService } from '../config/config.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from '../models/user';
import { distinctUntilChanged } from 'rxjs/operators/distinctUntilChanged';
import { AuthService } from '../auth/auth.service';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class UserStoreService {
    private user: User;

    private userSubject = new BehaviorSubject<User>(null);
    // private userSubject = new BehaviorSubject<User>({} as User);
    userMessage = this.userSubject.asObservable().pipe(distinctUntilChanged());

    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();

    constructor(private authService: AuthService) {
    }

    updateUser(user: User) {
        this.user = user;
        this.userSubject.next(user);
        this.isAuthenticatedSubject.next(true);
    }

    getCurrentUser(): User {
        return this.userSubject.value;
    }

    purgeAuth() {
        // Remove JWT from localstorage
        this.authService.destroyToken();
        // Set current user to an empty object
        this.userSubject.next({} as User);
        this.user= null;
        // Set auth status to false
        this.isAuthenticatedSubject.next(false);
    }

}
