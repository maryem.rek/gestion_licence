export function SuccessMapping(key: string): string {
    let error = {
        //user module
        USER_ADDED: 'utilisateur ajouté avec succès',
        USER_EDITED: 'utilisateur modifié avec succès',
        USER_DELETED: 'utilisateur supprimé avec succès',
        USER_IMAGE: 'image changé avec succées',
        USER_DATA_CHANGED: 'données changé avec succées',
        USER_PASSWORD_CHANGED: 'mot de passe changé avec succées',

        //BDI module
        ZONE_ADDED: 'zone ajouté avec succès',
        ZONE_EDITED: 'zone modifié avec succès',
        ZONE_DELETED: 'zone supprimé avec succès',
        TOPIC_ADDED: 'topic ajouté avec succès',
        TOPIC_EDITED: 'topic modifié avec succès',
        TOPIC_DELETED: 'topic supprimé avec succès',
        CURRENCY_ADDED: 'devise ajouté avec succès',
        CURRENCY_EDITED: 'devise modifié avec succès',
        CURRENCY_DELETED: 'devise supprimé avec succès',
        COUNTRY_ADDED: 'pays ajouté avec succès',
        COUNTRY_EDITED: 'pays modifié avec succès',
        COUNTRY_DELETED: 'pays supprimé avec succès',
        FAMILY_ADDED: 'famille ajouté avec succès',
        FAMILY_EDITED: 'famille modifié avec succès',
        FAMILY_DELETED: 'famille supprimé avec succès',
        INDICATOR_ADDED: 'indicateur ajouté avec succès',
        INDICATOR_EDITED: 'indicateur modifié avec succès',
        INDICATOR_DELETED: 'indicateur supprimé avec succès',
        FORMULA_ADDED: 'formule ajouté avec succès',
        FORMULA_EDITED: 'formule modifié avec succès',
        FORMULA_DELETED: 'formule supprimé avec succès',
        CURRENCY_VALUE_ADDED: 'valeur ajouté avec succès',
        CURRENCY_VALUE_EDITED: 'valeur modifié avec succès',
        CURRENCY_VALUE_DELETED: 'valeur supprimé avec succès',
        SUBSIDIARY_ADDED: 'filiale ajouté avec succès',
        SUBSIDIARY_EDITED: 'filiale modifié avec succès',
        SUBSIDIARY_DELETED: 'filiale supprimé avec succès',
        QUERY_ENTITY_ADDED: 'query ajouté avec succès',
        QUERY_ENTITY_EDITED: 'requête modifié avec succès',
        QUERY_ENTITY_DELETED: 'query supprimé avec succès',
        MYDASHES_DELETED: 'requête supprimé avec succès',
        QUERYENTITY_EDITED: 'requête modifié avec succès',
        COMMENT_ADDED: 'commentaire ajouté avec succès',
        COMMENT_EDITED: 'commentaire modifié avec succès',
        COMMENT_DELETED: 'commentaire supprimé avec succès',
    };
    if (error.hasOwnProperty(key)) {
        return error[key];
    } else {
        return key;
    }
}