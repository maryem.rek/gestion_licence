export function ErrorMapping(key: string): string {
    let error = {
        //user module
        USER_TOKEN_REQUIRED: 'token requie',
        USER_PASSWORD_REQUIRED: 'mot de passe requie',
        USER_INVALID_TOKEN: 'invalide token',
        USER_INVALID_TOKEN_TIME: 'token expiré',
        USER_INVALID_PASSWORD: 'invalide mot de passe',
        USER_INVALID_CREDENTIALS: 'invalide credentials',
        USER_NOT_FOUND: 'Utilisateur introuvable',
        USER_ALREADY_EXIST: "L'email saisie exite déja",
        USER_DISABLED: 'Utilisateur est désactivé',
        USER_FORBIDDEN_SUPER_ADMIN: "Utilisateur ne peut pas etre modifié car c'est un super admin",
    };
    if (error.hasOwnProperty(key)) {
        return error[key];
    } else {
        return key;
    }
}