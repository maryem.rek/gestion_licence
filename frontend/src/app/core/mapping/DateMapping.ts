export function dateToString(month: any) {
    const monthNames =["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
    return monthNames[month-1];
}