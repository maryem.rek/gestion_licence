import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule, AppRoutingComponent, AppRoutingproviders } from './app-routing.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { AppComponent } from './app.component';
import { HomeComponent } from './core/components/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FileSaverModule } from 'ngx-filesaver';
import { ToastrModule } from 'ngx-toastr';
import {MatListModule} from '@angular/material/list';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HeaderComponent } from './modules/bdi/components/header/header.component';
import { MatNativeDateModule, MatSnackBarModule } from '@angular/material';
import { LicenseDialogComponent } from './modules/bdi/components/license-dialog/license-dialog.component';
import {MatInputModule} from '@angular/material';
import {NgxPaginationModule} from 'ngx-pagination';
import {MatDialogModule} from '@angular/material/dialog';
import { LicenseConfigComponent } from './modules/bdi/components/license-config/license-config.component';



@NgModule({
  declarations: [
    AppComponent,
    AppRoutingComponent,
    HomeComponent,
    HeaderComponent,
    LicenseConfigComponent,
    LicenseDialogComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ToastrModule.forRoot({timeOut: 3000}),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FileSaverModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatInputModule,
    NgxPaginationModule,
    PopoverModule.forRoot()
  ],
  providers: [
  AppRoutingproviders],
  bootstrap: [
    AppComponent
  ],

  entryComponents: [
  LicenseConfigComponent,
  LicenseDialogComponent
  ],
})
export class AppModule { }
