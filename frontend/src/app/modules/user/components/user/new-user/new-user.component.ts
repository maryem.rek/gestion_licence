import { Component, OnInit } from '@angular/core';
import { UserStoreService } from '../../../../../core/store/user-store.service';
import { UserService } from '../../../../../core/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { User, Authorities } from '../../../../../core/models/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthoritiesEnum } from '../../../../../core/enum/authorities-enum.enum';
import { ObjectKeys } from '../../../../../core/helpers/json-helpers';
import { Router } from '@angular/router';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  user: User = null;
  isLoaded: boolean = true;
  userForm: FormGroup;
  authorities: any[];
  constructor(private router: Router, private userStore: UserStoreService, private userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    let authEnum: typeof AuthoritiesEnum = AuthoritiesEnum;
    let arr = [];
    ObjectKeys(AuthoritiesEnum).forEach(key => {
      arr.push({
        'name': authEnum[key],
        'value': {
          'name': key
        }
      })
    });
    this.authorities = arr;
    this.userForm = new FormGroup({
      'firstname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'lastname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'email': new FormControl('', [Validators.required, Validators.email, Validators.maxLength(150)]),
      'password': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      'enabled': new FormControl(true, [Validators.required]),
      'authorities': new FormControl(null),
    });
  }
  onSubmit() {
    this.user = {
      firstname: this.userForm.value.firstname,
      lastname: this.userForm.value.lastname,
      email: this.userForm.value.email,
      password: this.userForm.value.password,
      enabled: this.userForm.value.enabled,
      authorities: null
    };
    if (this.userForm.value.authorities != null) {
      if (this.userForm.value.authorities.length != 0) {
        this.user.authorities = this.userForm.value.authorities;
      }
    }
    this.isLoaded = false;
    this.userService.newUser(this.user).subscribe(
      (data) => {
        this.toastr.success(SuccessMapping('USER_ADDED'));
        this.router.navigate(['/user']);
        this.isLoaded = true;
      },
      (error) => {
        this.isLoaded = true;
      }
    );
    return false;
  }
  onAnnuler() {
    this.router.navigate(['/user']);
    return false;
  }
}
