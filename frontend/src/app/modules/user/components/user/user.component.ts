import { Component, OnInit, ViewChild } from '@angular/core';
import { UserStoreService } from '../../../../core/store/user-store.service';
import { UserService } from '../../../../core/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { User } from '../../../../core/models/user';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { MyModalComponent } from '../../../../core/components/my-modal/my-modal.component';
import { ObjectKeys } from '../../../../core/helpers/json-helpers';
import { AuthoritiesEnum } from '../../../../core/enum/authorities-enum.enum';
import { FormGroup, FormControl } from '@angular/forms';
import { SuccessMapping } from '../../../../core/mapping/SuccessMapping';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  isLoaded = false;
  users: User[] = [];
  private name:string;
  authEnum: typeof AuthoritiesEnum = AuthoritiesEnum;
  displayedColumns = ['email', 'firstname', 'roles', 'action'];
  dataSource = new MatTableDataSource<User>(this.users);
  @ViewChild(MatPaginator) private paginator: MatPaginator;
  @ViewChild(MatSort) private sort: MatSort;
  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }
  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  @ViewChild("modal") private childComponent: MyModalComponent;
  userForm: FormGroup;
  authorities: any[];
 

  constructor(public dialog: MatDialog, private userStore: UserStoreService, private userService: UserService, private toastr: ToastrService) { }


  ngOnInit() {
    let arr = [];
    ObjectKeys(AuthoritiesEnum).forEach(key => {
      arr.push({
        'name': this.authEnum[key],
        'value': {
          'name': key
        }
      })
    });
    this.authorities = arr;
    this.userForm = new FormGroup({
      'name': new FormControl(null),
      'email': new FormControl(null),
      'authorities': new FormControl(''),
    });
    this.userService.getAllUsers().subscribe(
      (users: User[]) => {
        this.isLoaded = true;
        this.users = users;
        this.dataSource = new MatTableDataSource<User>(this.users);
      }
    );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  ObjectKeys(obj) {
    return ObjectKeys(obj);
  }

  disable(id) {
    this.isLoaded = false;
    this.userService.disable(id).subscribe(
      () => {
        let usersCopy = this.users;
        this.users = [];
        usersCopy.map((data: User) => {
          if (data.id == id) {
            data.enabled = false;
          }
          this.users.push(data);
        });
        this.dataSource = new MatTableDataSource<User>(this.users);
        this.isLoaded = true;
        this.toastr.success(SuccessMapping('USER_EDITED'));
      },
      (error) => {
        this.isLoaded = true;
      }
    )
    return false;
  }
  enable(id) {
    this.isLoaded = false;
    this.userService.enable(id).subscribe(
      () => {
        let usersCopy = this.users;
        this.users = [];
        usersCopy.map((data: User) => {
          if (data.id == id) {
            data.enabled = true;
          }
          this.users.push(data);
        });
        this.dataSource = new MatTableDataSource<User>(this.users);
        this.isLoaded = true;
        this.toastr.success(SuccessMapping('USER_EDITED'));
      },
      (error) => {
        this.isLoaded = true;
      }
    )
    return false;
  }
  remove(id) {
    this.isLoaded = false;
    this.userService.delete(id).subscribe(
      () => {
        let usersCopy = this.users;
        this.users = [];
        usersCopy.map((data: User) => {
          if (data.id != id) {
            this.users.push(data);
          }
        });
        this.dataSource = new MatTableDataSource<User>(this.users);
        this.isLoaded = true;
        this.toastr.success(SuccessMapping('USER_DELETED'));
      },
      (error) => {
        this.isLoaded = true;
      }
    )
    return false;
  }

  Search(){
    if(this.name !=""){
    this.users=this.users.filter(res=>{
    return res.firstname.toLocaleLowerCase().match(this.name.toLocaleLowerCase())
    });
    }else if(this.name == ""){
    this.ngOnInit();
    }
    }

}
