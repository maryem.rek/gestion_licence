import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserStoreService } from '../../../../../core/store/user-store.service';
import { UserService } from '../../../../../core/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';

@Component({
  selector: 'app-password-user',
  templateUrl: './password-user.component.html',
  styleUrls: ['./password-user.component.css']
})
export class PasswordUserComponent implements OnInit {
  isLoaded: boolean= true;
  userForm: FormGroup;
  constructor(private userStore: UserStoreService, private userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.userForm = new FormGroup({
      'password': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
    });
  }

  onSubmit(){
    this.isLoaded = false;
    this.userService.changePassword(this.userForm.value.password).subscribe(
      (data) => {
        this.toastr.success(SuccessMapping('USER_PASSWORD_CHANGED'));
        this.userForm.patchValue({'password': ''});
        this.isLoaded = true;
      }
    );
    return false;
  }
  onAnnuler() {
    this.userForm.patchValue({'password': ''});
    return false;
  }

}
