import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../../../../../core/service/user.service';
import { User } from '../../../../../core/models/user';
import { UserStoreService } from '../../../../../core/store/user-store.service';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../../core/mapping/SuccessMapping';

@Component({
  selector: 'app-avatar-user',
  templateUrl: './avatar-user.component.html',
  styleUrls: ['./avatar-user.component.css']
})
export class AvatarUserComponent implements OnInit {
  @ViewChild('file') fileInput: ElementRef;
  myfile: File = null;
  image: string = null;
  isLoaded: boolean = true;
  constructor(private userService: UserService, private userStore: UserStoreService, private toastr: ToastrService) { }

  ngOnInit() {
  }

  changeName(image: string) {
    this.image = image;
  }

  handleFileInput(files) {
    if (files && files.item(0)) {
      this.myfile = files.item(0);
      let fileInput = this.fileInput;
      let reader = new FileReader();
      let parent = this;
      reader.onload = function (e) {
        let target: any = e.target;
        parent.changeName(target.result);
      }
      reader.readAsDataURL(files.item(0));
    } else {
      this.fileInput = null;
      this.image = null;
    }
    this.fileInput.nativeElement.value = null;
  }
  onSauvgarder() {
    if (this.myfile) {
      this.userService.changeAvatar(this.myfile).subscribe(
        (user: User) => {
           this.userStore.updateUser(user); 
           this.toastr.success(SuccessMapping('USER_IMAGE'));
          },
        (error) => { console.log(error); }
      )
    }
    ;
  }

}
