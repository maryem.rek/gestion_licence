import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../core/service/user.service';
import { User } from '../../../../core/models/user';
import { UserStoreService } from '../../../../core/store/user-store.service';
import { AuthoritiesEnum } from '../../../../core/enum/authorities-enum.enum';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  user: User = null;
  isLoaded: boolean = false;
  authEnum: typeof AuthoritiesEnum = AuthoritiesEnum;

  constructor(private userStore: UserStoreService, private userService: UserService) { }

  ngOnInit() {
    this.userService.populate().subscribe(
      (user: User) => {
        this.userStore.updateUser(user);
        this.user = user;
        this.isLoaded = true;
      },
      (error) => {
        this.isLoaded = true;
      }
    );
    this.userStore.userMessage.subscribe(
      (user: User) => {
        if (user) {
          this.user = user;
          this.isLoaded = true;
        }
      }
    );
  }

}
