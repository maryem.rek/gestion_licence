import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialMenuModule } from '../../core/modules/material-import.module';
import { ModalImportModule } from '../../core/modules/modal-import.module';
import { GridComponent } from './components/grid/grid.component';
import { NewLicenceComponent } from './components/new-licence/new-licence.component';
import { EditLicenceComponent } from './components/edit-licence/edit-licence.component';




const routes: Routes = [
  { path: '', component: GridComponent, pathMatch: 'full'},
  { path: 'licence', component: GridComponent },
  { path: 'new', component: NewLicenceComponent, pathMatch: 'full'},
  { path: 'edit', component: EditLicenceComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalImportModule,
    MaterialMenuModule,
    RouterModule.forChild(routes)],

  exports: [
    MaterialMenuModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    GridComponent,
   
  ]
})
export class BdiRoutingModule { }
