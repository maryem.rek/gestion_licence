import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseConfigComponent } from './license-config.component';

describe('LicenseConfigComponent', () => {
  let component: LicenseConfigComponent;
  let fixture: ComponentFixture<LicenseConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
