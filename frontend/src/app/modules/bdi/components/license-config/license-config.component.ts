import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LicenceService } from '../../../../core/service/licence.service';




@Component({
  selector: 'app-license-config',
  templateUrl: './license-config.component.html',
  styleUrls: ['./license-config.component.css']
})
export class LicenseConfigComponent implements OnInit {
 
  constructor(
    private licenceService: LicenceService,
    public dialogRef: MatDialogRef<LicenseConfigComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit() {
  }

  remove(){
    this.licenceService.deleteLicence(this.data.id)
    .subscribe(
      data => {
        this.cancel();
  })
}

  cancel() {
    this.dialogRef.close();
  }
}


    

