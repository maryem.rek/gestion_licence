import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewLicenceComponent } from './new-licence.component';

describe('NewLicenceComponent', () => {
  let component: NewLicenceComponent;
  let fixture: ComponentFixture<NewLicenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewLicenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewLicenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
