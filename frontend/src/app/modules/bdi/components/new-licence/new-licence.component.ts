import { Component, OnInit, Inject } from '@angular/core';
import { Licence } from '../../../../core/models/licence';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LicenceService } from '../../../../core/service/licence.service';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../core/mapping/SuccessMapping';


@Component({
  selector: 'app-new-licence',
  templateUrl: './new-licence.component.html',
  styleUrls: ['./new-licence.component.css']
})
export class NewLicenceComponent implements OnInit {
  licence: Licence = null;
  isLoaded: boolean = true;
  licenceForm: FormGroup;
  constructor(
    private router: Router, 
    private licenceService: LicenceService, 
    private toastr: ToastrService) { }

  ngOnInit() {
    this.licenceForm = new FormGroup({
      id: new FormControl(''),
      nom: new FormControl(''),
      nomClient: new FormControl(''),
      dateA: new FormControl(''),
      dateEx: new FormControl(''),
  });

    this.licenceForm.setValue({
      id: this.licenceForm.value.id,
      nom: this.licenceForm.value.nom,
      nomClient: this.licenceForm.value.nomClient,
      dateA: new Date(this.licenceForm.value.dateA),
      dateEx: new Date(this.licenceForm.value.dateEx)
      
    }
    );
  }

  onSubmit() {  
    this.create(this.licenceForm.value);
  }

  create(licence:Licence){
    this.licenceService.newLicence(licence).subscribe(data=>{
      this.router.navigate(['/bdi']);
      this.toastr.success(SuccessMapping('Licence bien ajouter'));
      })
  }
  onAnnuler() {
    this.router.navigate(['/bdi']);
    return false;
  }
}
