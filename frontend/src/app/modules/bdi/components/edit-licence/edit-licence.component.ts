import { Component, OnInit, Inject, Optional } from '@angular/core';
import { Licence } from '../../../../core/models/licence';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LicenceService } from '../../../../core/service/licence.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { SuccessMapping } from '../../../../core/mapping/SuccessMapping';


@Component({
  selector: 'app-edit-licence',
  templateUrl: './edit-licence.component.html',
  styleUrls: ['./edit-licence.component.css']
})
export class EditLicenceComponent implements OnInit {
  licence: Licence;
  isLoaded: boolean = true;
  licenceForm: FormGroup;
  isUpdate:Boolean=false;
  licenceId: string;
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private licenceService: LicenceService,
    private route: ActivatedRoute,
    ) { }

    ngOnInit() {
      this.licenceForm = new FormGroup({
        id: new FormControl(''),
        nom: new FormControl(''),
        nomClient: new FormControl(''),
        dateA: new FormControl(''),
        dateEx: new FormControl(''),
    });
  
      this.licenceForm.setValue({
        id: this.licenceForm.value.id,
        nom: this.licenceForm.value.nom,
        nomClient: this.licenceForm.value.nomClient,
        dateA: new Date(this.licenceForm.value.dateA),
        dateEx: new Date(this.licenceForm.value.dateEx)
        
      }
      );
    }  

  onSubmit(){
   
    this.licenceService.updateLicence(this.licenceForm.value).subscribe(
      dataUpdate =>{
        this.router.navigate(['/bdi']);
        
      
      }
    )}

  onAnnuler() {
      this.router.navigate(['/bdi']);
      return false;
  }

}
