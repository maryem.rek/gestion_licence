import {Component, OnInit, Output, ViewChild} from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { EventEmitter } from 'events';
import { LicenseDialogComponent } from '../license-dialog/license-dialog.component';
import { LicenceService } from '../../../../core/service/licence.service';
import { Licence } from '../../../../core/models/licence';
import { FormGroup } from '@angular/forms';
import { LicenseConfigComponent } from '../license-config/license-config.component';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

  licences: Licence[]= [];
  private firstname:string;
  licenceForm: FormGroup;
  @Output() refresh= new EventEmitter();

  constructor(private licenceService: LicenceService,public dialog: MatDialog) {
  }

  ngOnInit() {
    this.licenceService.getAllLicences().subscribe((licences: Licence[]) => {
      this.licences= licences;
      this.licenceService.notifyData(licences);
    })
  }

  remove(licence:Licence) {
    const dialogRef = this.dialog.open( LicenseConfigComponent , {
      width: '450px',
      data: {id:licence.id}
    });

    dialogRef.afterClosed().subscribe(result => {
    this.ngOnInit();
    });
  }

  update(licence:Licence){
    const dialogRef = this.dialog.open(LicenseDialogComponent, {
      width: '350px',
      data: {
        data:licence
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
      });
  }

  getData(){
    this.ngOnInit();
  }

  // pour la recherche
  Search(){
    if(this.firstname !=""){
    this.licences=this.licences.filter(res=>{
    return res.nom.toLocaleLowerCase().match(this.firstname.toLocaleLowerCase())
    });
    }else if(this.firstname == ""){
    this.getData();
    }
    }
}
