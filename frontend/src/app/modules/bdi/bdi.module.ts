import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BdiRoutingModule } from './bdi-routing.module';
import { MaterialMenuModule } from '../../core/modules/material-import.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewLicenceComponent } from './components/new-licence/new-licence.component';
import { EditLicenceComponent } from './components/edit-licence/edit-licence.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialMenuModule,
    BdiRoutingModule,
  ],
  declarations: [NewLicenceComponent, EditLicenceComponent],
  providers: []
})
export class BdiModule { }
