import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { LoginComponent } from './core/components/login/login.component';
import { ForgotComponent } from './core/components/forgot/forgot.component';
import { NotFoundComponent } from './core/components/not-found/not-found.component';
import { PasswordComponent } from './core/components/password/password.component';
import { HomeComponent } from './core/components/home/home.component';
import { AuthService } from './core/auth/auth.service';
import { ParamsService } from './core/config/params.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialMenuModule } from './core/modules/material-import.module';
import { UserDropdownComponent } from './core/components/dropdown/user/user-dropdown.component';
import { NotificationDropdownComponent } from './core/components/dropdown/notification/notification-dropdown.component';
import { ConfigService } from './core/config/config.service';
import { AuthGuard } from './core/auth/auth.guard';
import { UserService } from './core/service/user.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './core/auth/auth.interceptor';
import { UserStoreService } from './core/store/user-store.service';
import { AuthoritiesGuard } from './core/auth/authorities.guard';
import { DeniedComponent } from './core/components/denied/denied.component';
import { ApiService } from './core/config/api.service';
import { ParamsLocalService } from './core/config/params-local.service';
import { ApiLocalService } from './core/config/api-local.service';
import { LicenceService } from './core/service/licence.service';




const routes: Routes = [
  {path: 'auth/login', component: LoginComponent, pathMatch:'full'},
  {path: 'auth/forget', component: ForgotComponent, pathMatch:'full'},
  {path: 'auth/forget/:token', component: PasswordComponent},
  {path: '', redirectTo: 'bdi', pathMatch:'full'},
  {path: '', component: HomeComponent, canActivate: [AuthGuard], children: [
    { path: 'bdi', loadChildren:'./modules/bdi/bdi.module#BdiModule'},
    { path: 'user', loadChildren:'./modules/user/user.module#UserModule'}
  ]},
  {path: 'not-found', component: NotFoundComponent, pathMatch:'full'},
  {path: 'denied', component: DeniedComponent, pathMatch:'full'},
  {path: 'login', redirectTo: 'auth/login', pathMatch:'full'},
  {path: '**', redirectTo: 'not-found'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules, useHash: true}),
    BrowserAnimationsModule,
    MaterialMenuModule
  ],
  exports: [
    RouterModule,
    BrowserAnimationsModule,
    MaterialMenuModule],
  declarations: []
})
export class AppRoutingModule { }

export const AppRoutingComponent=[
  LoginComponent,
  ForgotComponent,
  PasswordComponent,
  NotFoundComponent,
  DeniedComponent,
  UserDropdownComponent,
  NotificationDropdownComponent,

  
];

export const AppRoutingproviders=[
  AuthGuard,
  AuthoritiesGuard,
  UserStoreService,
  ParamsService,
  ParamsLocalService,
  ConfigService,
  AuthService,
  ApiService,
  ApiLocalService,
  UserService,
  LicenceService,
  {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
];
